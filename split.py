import os
from minh.experiment import ukwac

if __name__ == '__main__':
    output_dir = 'data/split'
    os.mkdir(output_dir)
    ukwac.random_split(['data/*.xml.gz'], output_dir + '/split-%03d.xml.gz', 
                       100, 0.4, 1, True)
