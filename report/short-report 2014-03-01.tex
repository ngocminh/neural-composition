\documentclass[10pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{float}
\usepackage{color}
\usepackage{array}
\usepackage{supertabular}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage[round]{natbib}
\author{Lê Ngọc Minh}
\title{Compositional Distributional Semantics with Neural Networks: A Short Report}
% Text styles
\newcommand\textstyleStrongEmphasis[1]{\textbf{#1}}
\makeatletter
\newcommand\arraybslash{\let\\\@arraycr}
\makeatother

\newcommand\wordform[1]{#1}

\begin{document}
\maketitle

\section{Introduction}

Being the single central problem of compositional distributional semantics, composition model design has always received much attention. Researchers have proposed various ways to composition but all of them have some fundamental problems. Tensor product produces tensor whose size grows exponentially with the number of words. Addition, element-wise multiplication, dilation, and circular convolution are all too simplistic in the sense that they have too little parameters to model the great variation in language. Linear models are more expressive but still restrictive. More complicated models such as matrix-vector recursive neural network and lexical function don't suffer from this problem but their size is proportional to vocabulary size and much more training data is needed.

My aim is to find a composition model that is expressive enough to account for the complexity of language while having a manageable and tunable size. With recent advances in neural network research, this family of universal function approximators has become more and more appealing.

\section{Methods}

I study the family of composition models that has the form:
\[\hat{v}_{ab} = f([v_a v_b])\]
where $v_a, v_b$ are vectors representing words $a$, $b$ and $\hat{v}_{ab}$ is a vector predicted for the phrase $ab$. $f$ stands for a neural network which is not restricted to be single-layered as in \cite{Socher2011}.

The neural networks are trained so that predicted vectors $\hat{v}_{ab}$ are as similar as possible to observed vectors $v_{ab}$ which can be formulated as maximizing the total cosine similarity
%\[\max \sum_{a,b} sim(\hat{v}_{ab}, v_{ab})\]
or minimizing the total distance of \textit{normalized} vectors.
%\[\min \sum_{a,b} d(\hat{v}_{ab}, v_{ab}).\]
Following a common practice in neural network literature, I chose to minimize squared error which is equivalent to minimizing total \textit{squared} Euclidian distance between predicted and observed vectors:
\[ \min \sum_{a,b} d^2(\hat{v}_{ab}, v_{ab}) \]
To ensure that neural networks produces normalized vectors, I add a normalization layer after the output layer of all networks:
\[
\overline{y}_i =
  \begin{cases}
   \frac{y_i}{\sum_{i=1}^{s}y_i^2} & \text{if } \exists j, y_j \neq 0 \\
   0       & \text{if } \forall j, y_j = 0 
  \end{cases}
\]
where $\overline{y}_i$ is the normalized activation at node $i$ and $y_i$ is the net input at node $i$ which is taken to equal to the unnormalized activation at the same position.

I experimented with sigmoid and rectified linear (also call rectifier) activation function \citep{glorot2011deep}. A rectifier node forward positive input while block negative input, emitting zero, therefore sparsify activation and allow deeper back propagation. A smoothed variation of rectifier is the softplus function used in output layers:
\[ f(x) = \log(1+e^x) \]

Dropout \citep{Srivastava2013} is a supportive technique to train deep networks. It randomly mutes a portion of nodes at training time and scale the weights before testing. Dropout can be seen as a bagging technique which simultaneously train an exponential number of models and average them at testing. Experiments have shown that it raises training error but lowers test error, therefore improves generalization. Srivastava also observed that dropout promotes sparse activation at test time.

\section{Experiments}

Co-occurrence counts are gathered from a mixed corpus of BNC, UkWaC and Wikipedia. All words that appear in a question or have at least 2 characters and appear at least 100 times in the corpus are choosen,resulting in 274K rows. All words co-occurring to a target word in a sentence are candidates for context. A maximum of 10,000 most frequent contexts are used. The co-occurrence matrix is then processed by PPMI, SVD and NNSE. To gather observed phrase vectors, I created a peripheral space from co-occurrences of contexts with all phrases in questions together with those sharing either their heads or modifiers. The peripheral space is then applied exact transformations as the core space.

It should be noted that NNSE is a complex non-linear transformation which involves solving a LASSO regression to represent a vector as a linear combination of given set of vectors (called dictionary) with a sparsity constraint. The problem is also known as basis pursuit denoising. Therefore, it is unlikely that a neural network can simulate a NNSE transformation by its first layers.

So far, I tested a shallow sigmoid-activation net, a shallow rectified linear net, deep rectified linear nets with or without dropout, fat shallow rectifier nets with or withou dropout. The fat nets have 6,000 nodes in its hidden layer while all others have hidden layer(s) of size 3000. Shallow nets have only one hidden layer while deep nets have three. Following \cite{glorot2011deep}, to avoid blocking error propagation through nodes with absolute zero activation, I used a softplus layer instead of a rectifier layer. I also experimented with linear output layer. 

\begin{table}[h]
\begin{center}
\begin{tabular}{l c c}
\hline
& NNSE & SVD \\
\hline
sigmoid & 23.0 & 20.1 \\
rectifier (softplus output) & 49 & 49.9 \\
deep rectifier (softplus output) & 49 & 49.5 \\
deep rectifier + dropout (softplus output) & 49.7 & 49.5 \\
fat rectifier + dropout (softplus output) & 51.4 & 51.3 \\
rectifier (linear output) & 52.0 & - \\
fat rectifier (linear output) & 52.6 & \textbf{53.6} \\
\hline
\end{tabular}
\end{center}
\caption{Precision (in percentage) of neural network models when ran on either NNSE or SVD space. All models have answered more than 99\% of questions.
\label{tab:precision}}
\end{table}

Table \ref{tab:precision} shows available results so far. Achieving the best performance is the neural network with 6,000 rectified linear nodes in the only hidden layer and linear nodes in the output layer. Though argued to be more expressive \citep{Pascanu2013}, deep neural networks have not shown superior results. Dropout results in modest improvement. NNSE has mixed impact on performance. The large of hidden layer turned out to be the most important factor.

\begin{table}[h]
\begin{center}
\begin{tabular}{l c c c c }
\hline
& \multicolumn{2}{ c  }{NNSE} & \multicolumn{2}{ c }{SVD} \\
& Precision & Answered rate & Precision & Answered rate \\
\hline
observed & 66 & 68 & 65.9 & 67.8 \\
multiplicative & 57 & 35 & 56.8 & 34.6 \\
weighted additive & 49 & 92 & 49.3 & 92.1 \\
full additive & 50.0 & 100 & 50.6 & 99.7 \\
\hline
\end{tabular}
\end{center}
\caption{Precision of baseline models. Some questions are omitted either because the target phrase is never observed or assigned a zero vector by the model.
\label{tab:precision-baseline}}
\end{table}

The best neural model outperformed all baseline models in table \ref{tab:precision-baseline}, except the observed space which it tries to predict. However, due to data scarcity, observed space can answer only about 68\% of questions while neural models always have more than 99\% coverage.

In previous experiments, I measured sparseness by \textit{sparsity level} which is defined to be the number of zeros in a matrix. However, because it is hard to achieve absolute zeros by neural networks, this measurement failed to differentiate some models. As shown in table \ref{tab:sparseness}, all shallow rectifier networks have zero or near-zero sparsity levels although they are different in structure and performance.

\cite{Foldiak:2008} suggest the use of $L^1$ norm as a measure of (the opposite of) sparseness. The larger $L^1$ norm a matrix has, the less sparse it is. I found a weak correlation of 0.27 between $L^1$ norm and precision, that means sparseness works against precision but accounts for only a small portion of its variation.

\begin{table}[h]
\begin{center}
\begin{tabular}{l c c c c c c}
\hline
& \multicolumn{3}{ c }{NNSE} & \multicolumn{3}{ c }{SVD} \\
& S & L1 & P & S & L1 & P \\
\hline
observed words & 99.6 & 1.7 & - & 0 & 20.1 & - \\
observed phrases & 99.6 & 1.4 & - & 99.6 & 1.4 & - \\
\hline
sigmoid	&61.6	&17.01	&23	&55.1	&17.57	&20.1\\
rectifier (softplus)	&0.1	&2.88	&49	&0	&2.93	&49.9\\
deep r. (softplus)	&85.3	&1.52	&49	&84.4	&1.52	&49.5\\
deep r. + dropout (softplus)	&97.5	&2.82	&49.7	&97.5	&2.83	&49.5\\
fat r. + dropout (softplus)	&0	&2.92	&51.4	&0	&2.93	&51.3\\
rectifier (linear)	&-	&-	&52	&-	&-	&- \\
fat rectifier (linear)	&0	&7.17	&52.6	&0	&7.17	&53.6\\
\hline
\end{tabular}
\end{center}
\caption{Sparseness and precision of models. Columns: S=Sparsity level (\%), L1=L1 norm (sum of absolute values,  smaller means sparser), P=Precision. Rows: r.=rectifier, words in parentheses are the types of output layer. All spaces are row normalized.
\label{tab:sparseness}}
\end{table}

I observed that SVD phrase space is as sparse as NNSE space which could partially explain the proximity of their performance. Creating the NNSE phrase space is essentially solving a linear system with $L^1$-regularization. If the input space is already very sparse, the $L^1$ term will be small enough to have no effect. But why is the SVD phrase space so sparse? In fact it uses only 3.85$\pm$2.73 dimensions to represent each phrase. A possible reason is that vectors representing phrases are close to the bases discovered by SVD when applied on word space. Put another way, in distributional semantics, a word is the combination of phrases instead of vice versa.

\section{Further research}

At this stage of research, some results are still missing and some need to be redo for consistency (for example, some numbers in table \ref{tab:precision-baseline} need one more digit after decimal point). To confirm the performance of neural models, I need to carry out experiments on other tasks. Another proposal is to experiment with maxout net as it is reported to bring up performance in various tasks \citep{goodfellow2013maxout}.

\bibliographystyle{plainnat}
\bibliography{/home/cumeo/Documents/nlp,/home/cumeo/Documents/deep-learning,/home/cumeo/Documents/cognitive-science,frege-in-space}
\end{document}