\chapter{ANN models of composition}\label{chap:model}
\ifpdf
    \graphicspath{{Chapter3/figs/PNG/}{Chapter-model/figs/PDF/}{Chapter-model/figs/}}
\else
    \graphicspath{{Chapter-model/figs/EPS/}{Chapter-model/figs/}}
\fi

\epigraph{Creativity is just connecting things.}{Steve Jobs}

Compositional VSMs call for expressive power where neural networks shine. I simply put them together.

The first neural network architecture I investigated (denoted by \code{tahn}) is a tanh ANN as described in Section \ref{sec:tanh}. This is equivalent to the encoding component of deep autoencoders in \cite{Socher2011}. However it does not learn by reconstructing constituents but by fitting to observed phrase vectors.

The second architecture (\code{rect}) consists of a rectifier hidden layer and a linear output layer as described in Section \ref{sec:rectifier}. Rectifier units are not suitable for output layers because they cannot emit negative values and the gradient cannot be propagated through inactive nodes. They were attempted with poor results and will not be reported here.

The third architecture (\code{lwta}) has two hidden layers - one linear and one LWTA - and a linear output layer as described in Section \ref{sec:lwta}. The reason for a linear output layer is similar to that of \code{rect}.

To fix the problem of non-monotonicity mentioned at the end of Section \ref{sec:lwta}, I replaced the linear hidden layer by a rectifier layer, resulting in the fourth architecture (\code{rect+lwta}). Note that:
\begin{equation}
\max(g_k(z_1,...z_s), 0) = g_k(\max(z_1, 0),...,\max(z_s, 0))
\end{equation} 
where $g_k(\cdot)$ is defined by \eqref{eq:competition-function}. Therefore reordering does not effect our nets.

While the above mentioned architectures provide the expressive power needed for effective composition, we can actually compose word vectors in at least two ways. 

I call the first approach \textit{direct composition} (\code{dir}) which simply uses the network output as a phrase vector:
\begin{equation}
p = F([u, v]; \theta_R)
\end{equation}
where $F$ is the whole-network function (see Section \ref{sec:ann-nutshell}), $u, v$ are constituent vectors, $[u, v]$ is the concatenation of them, $\theta_R$ is the set of parameters for the syntactic relation $R$, and $p$ is the predicted phrase vector.

Notice that the conventional and effective loss function used in this research, mean squared error, is proportional to the squared distance between actual and expected phrase vectors. But we measure their similarity by the cosine of the angle between them instead. For this loss function to work properly, I added a normalization layer immediately after the output layer of each architecture above. This layer does not perform linear combination and have an special activation function that depends on the net input of all units in the layer:
\begin{equation}
z_i^{(l)} = a_i^{(l-1)}
\end{equation}
\begin{equation}\label{eq:a_normalized}
a_i^{(l)} = \begin{cases}
		0 & \mbox{if } z_i^{(l)} = 0 \\
		\frac{z_i^{(l)}}{\sum_{j=1}^{s_l} \left( z_j^{(l)} \right)^2} & \mbox{otherwise}
	\end{cases}
\end{equation}

With a normalization layer, the output vectors of our models are always unit vectors (except the case of zero vector) and its distance to the expected vector monotonically decreases with the cosine between them. Therefore, minimizing mean squared error is equivalent to maximizing similarity.

The second approach explicitly uses ANNs to model the variation among phrases. The composition in this approach is a two-step process. Firstly, we compute a \textit{base vector} using a simple composition function, addition in this study. This function has limited expressive power therefore cannot work well for every phrases but we assume that it can compute a vector somewhat close to the expected one. We then use an ANN to compute a \textit{blending vector} that will be added to the base vector to create final result. I term this approach \textit{blending composition} (\code{bld}).
One can also think of blending models as unconventional neural networks with fixed-weight connections skipping from the input layer directly to the output layer but I prefer the previous conception to stress that the nets are used to model variation, 
hence\footnote{An alternate is to use normalized addition as a base vector which may be more stable. We can also replace it by piece-wise multiplication for this method achieves good empirical results in many tasks. However piece-wise multiplication does not work well with SVD space (see a discussion in Section \ref{sec:baseline}). I leave the problem of the best base composition for future research.}:

\begin{equation}
p = \frac{u+v}{2} + F([u, v]; \theta_R)
\end{equation}

Because there is not an apparent way to normalize blending vectors, I omit the normalization layer in these models.

Combining 4 architectures and 2 modes of composition, I created 8 novel models. In next chapters, I will denote them as \code{architecture.mode}, for example \code{tanh.dir}, \code{lwta.bld}.

% ------------------------------------------------------------------------


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
