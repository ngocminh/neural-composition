%%% Thesis Introduction --------------------------------------------------
\chapter{Introduction}
\ifpdf
    \graphicspath{{Introduction/figs/PNG/}{Introduction/figs/PDF/}{Introduction/figs/}}
\else
    \graphicspath{{Introduction/figs/EPS/}{Introduction/figs/}}
\fi

\epigraph{I am a HAL 9000 computer. I became operational at the H.A.L. plant in Urbana, Illinois, on the 12th January 1992. My instructor was Mr. Langley, and he taught me to sing a song.}{HAL 9000}

For a computer to have conversation in fluent English as HAL 9000 does in \textit{2001: A Space Odyssey}, it must understand an infinite number of sentences.

In a large part of the history of natural language processing (NLP), this was impossible because researchers framed human languages as a sibling of logics, as \cite{montague1970universal} famously wrote:

\begin{quote}
There is in my opinion no important theoretical difference between natural languages and the artificial languages of logicians; indeed, I consider it possible to comprehend the syntax and semantics of both kinds of languages within a single natural and mathematically precise theory.
\end{quote}

Even though logics is well-understood, powerful, and indeed works under the hood of most modern computers; it falls short in capturing the fuzzy, multi-faceted meaning of natural words and the complex interaction, with lots of irregularities, between them. Some limitations of logics can be demonstrated by a word: \textit{good}. Although everybody understands and uses this word, nobody, to the best of my knowledge, has defined it in an uncontroversial way. A person can be good as a worker while not good as a mother, and good in the eyes of her colleagues while not so to her employer. Moreover, Alice and Bob can be both good but one is better than the other and the ways in which they are good can differ.

An alternative way to the semantics of human language is to represent meaningful units as points or vectors in a vector space.
In contrast to logical structures, vectors can effortlessly express fuzziness by and polysemy. Moreover, they can be learned unsupervisedly from a large body of text, fed as the input of machine learners and efficiently manipulated by parallel computing.

Researchers have acknowledged the advantages of vector spaces. Starting in 1990 with a research modelling the similarity of words by the proximity of vectors \citep{deerwester1990indexing}, vector space models (VSMs) of meaning have greatly improved and shown human-level performance in similarity judgement tasks. Nowadays we are on the verge of a radically different NLP where a significant part of meaning is represented by vectors. Researchers have been attacking many subproblems in NLP with encouraging results such as selectional preference \citep{Erk07asimple}, 
terminology mining \citep{Sahlgren2009}, recommender systems \citep{Musto2010}, word sense discovering \citep{pantel2002discovering} and machine translation \citep{kalchbrenner2013recurrent}, among others.

Another contrast to logics but not advantageous for vectors is meaning composition. While it is native to the syntax and semantics of logics, the composition of two or more vectors is lossy and complex. This problem is of crucial importance. Without composition, we cannot understand a noun phrase, not to mention infinitely many sentences. In response to its special role, researchers have endeavoured to construct compositional VSMs and created a large and growing literature around the issue.

This research is an attempt to contribute to that literature with new approaches. 
It was born from my longtime interest in the human brain and what mimics it, artificial neural networks (ANNs). 
In my perspective, biomimicry is not the only road to artificial intelligence, as \cite{McDermott1997} put it: airplane ``doesn't flap its wings''; and ANNs, however deep they are, are not the silver bullet of machine learning. Nonetheless, the brain and ANNs have many desirable properties that, in an appropriate problem setting, can greatly improve performance. 
I put priority on the sparseness of representation and the expressive power of ANNs. \\
Sparse representation is thought to be able to disentangle factors and choose the most appropriate ones. 
A large group of ANNs was proved to be able to approximate any function to arbitrary accuracy. 
Moreover, recent developments in ANN research have created architectures that can be trained very effectively and achieved impressive empirical performance.

My thesis is organized as follows: The first part places the research in context, puts forward unsolved problems and motivates solutions. It does so by discussing linguistics notion of and observations on compositionality (Chapter \ref{chap:ling}); studying VSMs and current approaches to composition (Chapter \ref{chap:cds}); and exploring ANNs, especially recent developments (Chapter \ref{chap:ann}). The second part actually writes down my answers to problems in the previous part and grade those answers. In this part, Chapter \ref{chap:model} proposes novel ANN-based composition models; Chapter \ref{chap:eval} describes tasks to evaluate them; and Chapter \ref{chap:result} details the results. The last part made of Chapter \ref{chap:conclusions} discusses the achievements and future directions.

%%% ----------------------------------------------------------------------


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
