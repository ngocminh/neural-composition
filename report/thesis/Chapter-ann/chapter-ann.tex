\chapter{Artificial neural networks (ANN)}\label{chap:ann}
\ifpdf
    \graphicspath{{Chapter-ann/figs/PNG/}{Chapter-ann/figs/PDF/}{Chapter-ann/figs/}}
\else
    \graphicspath{{Chapter-ann/figs/EPS/}{Chapter-ann/figs/}}
\fi

Invented in 1943 as a simple model of biological neural networks \citep{mcculloch1943logical}, after 70 years of research, artificial neural networks (ANNs) are now a powerful and popular tool for machine learning and have achieved state-of-the-art in various tasks in computer vision, robotics and natural language processing (see \citealp{bengio2009learning} for a review).

Given the fact that the composition of meaning is effectively performed by billions of biological neural networks in a daily basis, it is very appealing to implement the composition of word vectors by ANNs. May it not only come close to the performance of human language users, but also give insights into the human mind.

This chapter serves as an introduction to the core ideas in ANNs together with specific models and techniques that will come in next chapters to make the thesis self-contained. No background knowledge is required apart from derivative and linear combination. Many topics about ANNs are beyond the scope of this thesis for which interested readers may want to consult \cite{bishop1995neural} (for foundational topics) or \cite{heaton2008introduction} (for practical purposes).

\section{ANN in a nutshell}\label{sec:ann-nutshell}

Neurons are building blocks of the human brain. Three main components of a neuron are its dendrite forest, its soma (cell body) and its axon. Information flows through a neuron in one direction: input signals are picked up by dendrites, aggregated in the soma and ignite a response signal transferred along the axon. This axon comes in contact with another neuron, which gathers signals from not only one but thousands of neurons, and make it fire and so on...

This structure is faithfully modelled by artificial neurons. A neuron aggregates inputs by linear combination with its \textit{weights} as coefficients, plus a \textit{bias}. The result is called \textit{net input}. It then ignites an \textit{activation} which is a number we get from applying an \textit{activation function} on the net input.

Let us consider an ANN which is organized into layers: all neurons lying in the same layer are structurally identical and there is a notion of ordering between layers. Let us further assume that connections come in only one direction from one layer to the next. That net is called a \textit{feed-forward} neural network. Its first layer is called \textit{input layer}. The last layer is called \textit{output layer}. Other layers are call \textit{hidden layers}.

Because of homogeneity within a layer, we can formally define it as a whole using vectors and matrices. Assume that we have $L$ layers, the $l^{\mbox{th}}$ layer has size $s_l$, $l=1,...,L$. The input is a vector $x \in \mathbb{R}^{s_0}$ and output $\hat{y} \in \mathbb{R}^{s_L}$. The weights of all units in layer $l$ are summarized in matrix $W^{(l)} \in \mathbb{R}^{s_l \times s_{l-1}}$ and their bias in vector $b \in \mathbb{R}^{s_l}$. Similarly, the net input is a vector $z \in \mathbb{R}^{s_l}$ and activation $a \in \mathbb{R}^{s_l}$. Each layer is assigned an activation function $f_l(\cdot)$ which can be vectorized: $f_l([z_1,...,z_n]) = [f_l(z_1),...,f_l(z_n)]$. The network can now be specified by the following system of equations:
\begin{equation}\label{eq:z_1}
z^{(1)} = W^{(1)} x + b^{(1)}
\end{equation}
\begin{equation}
a^{(1)} = f_1\left(z^{(1)}\right)
\end{equation}
\begin{equation}\label{eq:z_i}
z^{(l)} = W^{(l)} a^{(l-1)} + b^{(l)}, \forall l=2,...,L
\end{equation}
\begin{equation}\label{eq:a_i}
a^{(l)} = f_l\left(z^{(l)}\right), \forall l=2,...,L
\end{equation}
\begin{equation}\label{eq:y_hat}
\hat{y} = a^{(L)}
\end{equation}

As long as an ANN's structure remains the same, only by changing its weights one may change its behavior. Similar to synaptic plasticity observed in the brain, our artificial neurons can disconnect itself from another neuron by setting the corresponding weight to zero, form an excitatory connection by a positive weight or an inhibitory connection by a negative weight. 

We can think of the whole ANN as a big function: 
\begin{equation}
\hat{y} = F(x; \theta)
\end{equation}
where $\theta \in \mathbb{R}^m$ is the set of its parameters, consisting of weights and biases. Given an \textit{expected output} $y \in \mathbb{R}^{s_L}$, we can modify the parameters $\theta$ in a way that the actual output $\hat{y}$ becomes close to the expected output $y$ where closeness is a number returned by some \textit{loss function}. This process is called \textit{supervised training}. In the current research, the only loss function I used is the \textit{mean squared error}:
\begin{equation}
J(\theta) = \frac{1}{s_L}\sum_{i=1}^{s_L}(\hat{y}_i - y_i)^2
\end{equation}

We can compute the \textit{gradient} which, starting at any point in parameter space, can tell us the direction along which we can maximally reduce error:
\begin{equation}
\nabla J(\theta) = \left[
	\frac{\partial J}{\partial \theta_1}(\theta),...,
	\frac{\partial J}{\partial \theta_m}(\theta) \right]
\end{equation}

The training procedure was \textit{gradient descent}. In essence, it moves parameters along a direction informed by the gradient until it can come no closer to the expected output. Note that although, for the sake of simplicity, we talk about only one pair of input and output here, the training was actually executed on batches of thousands of training examples. In that case, inputs and outputs are represented by matrices instead of vectors without much change in our formulae.

The choice of activation functions is a crucial one. If all activation functions are linear or identical function, the net's function $F$ will degenerate into a linear combination of inputs, which is not very useful. Hence, there must always be some nonlinearity in the network. The shape of the activation function has a great effect on the performance of a neural net, which will be discussed in Section \ref{sec:backprop}. Then, three types of nonlinearity and their representative realization used in this research will be shown in next sections.

\section{Backpropagation and its implications on  activation functions}\label{sec:backprop}

Equations (\ref{eq:z_1}-\ref{eq:y_hat}) are called forward propagation because they calculate the activation of layers from the first to the last. A reverse procedure called backward propagation or backpropagation is used to calculate the gradient used in training.

Firstly, take the derivative of the last layer:
\begin{equation}\label{eq:dJdW-full}
\frac{\partial J}{\partial W^{(L)}_{ij}} =
	\frac{\partial J}{\partial \hat{y}_i} 
	\frac{\partial \hat{y}_i}{\partial z^{(L)}_i}
	\frac{\partial z^{(L)}_i}{\partial W^{(L)}_{ij}} =
	\frac{2}{s_L} (\hat{y}_i - y_i) f_L' \left( z^{(L)}_i \right) a^{(L-1)}_j
\end{equation}
\begin{equation}\label{eq:dJdb-full}
\frac{\partial J}{\partial b^{(L)}_{i}} =
	\frac{\partial J}{\partial \hat{y}_i} 
	\frac{\partial \hat{y}_i}{\partial z^{(L)}_i}
	\frac{\partial z^{(L)}_i}{\partial b^{(L)}_{i}} =
	\frac{2}{s_L} (\hat{y}_i - y_i) f_L' \left( z^{(L)}_i \right)
\end{equation}
The amount:
\begin{equation}\label{eq:delta-L}
\delta^{(L)}_i = \frac{2}{s_L} (\hat{y}_i - y_i) f_L' \left( z^{(L)}_i \right)
\end{equation}
can be thought of as the change that unit $i$ of the last layer need to make to reduce error. Substitute \eqref{eq:delta-L} in (\ref{eq:dJdW-full}-\ref{eq:dJdb-full}), we see that this change is passed down to its weights $W^{(L)}_{ij},\forall j=1,...,s_{L-1}$ and bias $b^{(L)}_{i}$:
\begin{equation}\label{eq:dJdWL}
\frac{\partial J}{\partial W^{(L)}_{ij}} = \delta^{(L)}_i a^{(L-1)}_j
\end{equation}
\begin{equation}\label{eq:dJdbL}
\frac{\partial J}{\partial b^{(L)}_{i}} = \delta^{(L)}_i
\end{equation}

Notice that an unit in hidden layer $l$ can only contribute to the output via units in the next layer, suggesting that the change in this unit consists of a weighted sum of changes in the next layer (a proof can be easily derived by chain rule):
\begin{equation}\label{eq:delta-l}
\delta^{(l)}_i = \left( \sum_{k=1}^{s_{l+1}} W^{(l+1)}_{ki} \delta^{(l+1)}_i \right)
                 f_l' \left( z^{(l)}_i \right)
\end{equation}
The derivatives of weights and bias come immediately:
\begin{equation}\label{eq:dJdWl}
\frac{\partial J}{\partial W^{(l)}_{ij}} = \delta^{(l)}_i a^{(l-1)}_j
\end{equation}
\begin{equation}\label{eq:dJdbl}
\frac{\partial J}{\partial b^{(l)}_{i}} = \delta^{(l)}_i
\end{equation}

We observe that the derivative of activation function appears in both \eqref{eq:delta-L} and \eqref{eq:delta-l}. A non-zero value of $f_l'(z^{(l)}_i)$ means the neuron can adapt to the error by changing its weights and bias. A zero or very small value means the neuron will not make any noticeable change therefore will not help reduce the error. On the other hand, a non-zero activation $f_l(z^{(l)}_i)$ contribute to the final output while a zero or small enough one does not.

Let us define relation $\mathcal{T}$ between a neuron $i$ and an example $(x,y)$:  \begin{equation}
\mathcal{T}(i, (x,y)) \iff |f_l'(z^{(l)}_i)| \geq \epsilon
\end{equation}
If $\mathcal{T}(i, (x,y))$ true, we say $i$ is \textit{trained on} $(x,y)$. 
Relation $\mathcal{E}$ is defined between a neuron $i$ and an input $x$: 
\begin{equation}
\mathcal{E}(i, x) \iff |f_l(z^{(l)}_i)| \geq \xi
\end{equation}
If $\mathcal{E}(i, x)$ holds true, we say $i$ \textit{responds} to $x$. $\epsilon, \xi \in \mathbb{R}^+$ are some small constants.

It is natural to expect these relations to go hand in hand: 
\begin{equation}\label{eq:T-iff-E}
\mathcal{T}(i, (x,y)) \iff \mathcal{E}(i, x)
\end{equation}
However there are cases where a neuron responds to an input but cannot be trained on it, for example on a non-zero plateau or saturation region of the activation function, $|f_l(z^{(l)}_i)| \geq \xi$ while $f_l'(z^{(l)}_i) \approx 0$. Regions like this can be typically found in bounded function with non-zero limits. The other case when a neuron is trained on what it does not respond to happens only at some certain points because the non-zero derivative will drive the activation function away from zero.

For hidden layers, we certainly expect a neuron to be trained on a certain set but not all of the examples: 
\begin{equation}
|\{(x,y)|\mathcal{T}(i, (x,y))\}| \ll |\{(x,y)\}|
\end{equation} 
because examples sharing some characteristics may reinforce each other while all available examples are too noisy. Vice versa, we expect an input to be responded by a certain group of neurons but not all of them: 
\begin{equation}
|\{i|\mathcal{E}(i, x)\}| \ll s_l
\end{equation} 
to disentangle factors contributing to the output. This represents the specialization within our neural population. Because of these desiderata, we prefer activation functions with a large region of zero activation (both value and derivative are zero).

For output layer, we need them to be always trainable: $\mathcal{T}(i, (x,y)), \forall i$. Hence, we prefer activation function with no plateau or saturation region.

\section{Sigmoidal activation functions and tanh}\label{sec:tanh}

\begin{wrapfigure}{R}{0.5\textwidth}
\includegraphics[width=0.5\textwidth]{tanh} 
\caption{$y = \tanh(x)$, an example of sigmoidal functions.\label{fig:tanh}}
\end{wrapfigure}

Sigmoidal functions were once the de facto standard in ANN researches. All sigmoidal functions share a characteristic that each has a (mostly) linear region framed between two saturation regions where the function tends to a constant (see Figure \ref{fig:tanh}).

In the current research, I used a special case of sigmoidal function, \textit{hyperbolic tangent}, which is defined by:
\begin{equation}
\tanh(z) = \frac{1-e^{-2z}}{1+e^{-2z}}
\end{equation}
Different from a more popular sigmoidal function, \textit{logistic function}, which can return positive values only, the output of this function lies between -1 and 1. This property makes it suitable for calculating vector components. Similar to some other sigmoidal functions, its derivative can be computed efficiently, fertilizing efficient training:
\begin{equation}
\tanh'(z) = 1-\tanh^2(z)
\end{equation}

An important property of sigmoidal functions is that they can be used to construct \textit{universal function approximators}. \cite{cybenko1989approximation} proved that any feed-forward neural net with a sigmoidal hidden layer can approximate any continuous function given a large enough number of neurons.

However, two saturation regions mean that this function can be trapped in high activation - low derivative situation. Besides, the absence of a zero region means each neuron is trained on all examples and each example is responded by all neurons.

\section{Piece-wise linear activation functions and rectifier units}\label{sec:rectifier}

\textit{Rectified linear} is an activation function that passes the net input if it is greater than zero and suppresses it otherwise (see Figure \ref{fig:rectified}):
\begin{equation} \label{eq:rectifier-activation-function}
f(x)=\begin{cases} 
			x & \mbox{if } x \geq 0 \\
           	0 & \mbox{otherwise}
       \end{cases}
\end{equation}

It is also written in a more compact way:
\begin{equation}
f(x)=\max(0,x)
\end{equation}

The function is monotonically increasing. Its derivative is given by:
\begin{equation}
f(x)=\begin{cases} 
			1 & \mbox{if } x > 0 \\
           	0 & \mbox{if } x < 0 \\
           	\mbox{undefined} & \mbox{if } x = 0
       \end{cases}
\end{equation}

An artificial neuron equipped with rectified linear as its activation function is called a \textit{rectifier unit}.

As a direct consequence of its definition, rectifier units have \textit{sparsifying} power. For example, a randomly initialized rectifier network has around 50\% of its rectifier units being inactive for an input event. This property helps disentangle different factors as discussed near the end of Section \ref{sec:backprop}. Moreover, this function satisfies \eqref{eq:T-iff-E} (except at the origin) which helps efficient training.

Moreover, \cite{glorot2011deep} has argued that, rectified linear as an activation function is more biologically plausible than hyperbolic tangent.

\begin{wrapfigure}{R}{0.5\textwidth}
\includegraphics[width=0.5\textwidth]{rectified-linear} 
\caption[Rectified linear function]{Rectified linear function, an example of piece-wise linear functions.\label{fig:rectified}}
\end{wrapfigure}

A biological neuron fires by many consecutive \textit{spikes}, short bursts of the electrical membrane potential in its soma that is transferred along its axon. Every spike of a neuron is identical but the firing rate (the number of spikes in an unit of time) depends on total electric current applied on the neuron, among other factors. It is a consensus among neuroscientists that a neuron starts firing when input current is greater than a certain threshold, increases monotonically and tends to a saturation level as the current increases \citep[p. 49-50]{dayan2001theoretical}.

The property that a neuron is active only when input signal exceeds a certain threshold is termed \textit{one-sided}. Just as biological neurons, rectifier units are one-sided. In contrast, hyperbolic tangent neurons are \textit{antisymmetric}, meaning that the output for negative input is negative. 

Rectified linear belongs to the family of \textit{piece-wise linear} activation functions. These functions, especially that of \textit{maxout} \citep{goodfellow2013maxout} and rectifier units, are becoming more and more popular and have achieved improved performance over traditional activation functions \citep{goodfellow2013maxout,glorot2011deep,zeiler2013rectified}. Feed-foward networks with maxout units was shown to be universal function approximators \citep{goodfellow2013maxout}.

%TODO {number of linear regions?}

\section{Competitive neural networks and local winner-take-all (LWTA) blocks}\label{sec:lwta}

\begin{wrapfigure}{R}{0.35\textwidth}
\includegraphics[width=0.35\textwidth]{lwta} 
\caption[Two local-winner-take-all blocks]{Two local-winner-take-all blocks. Within each block, only the unit receiving maximal input is activated.\label{fig:lwta}}
\end{wrapfigure}

So far we have considered interaction between different layers only while neurons in the same layer are assume to be independent. It turns out that same-layer interaction can be useful as lateral excitatory and inhibitory connections in the brain were found to help visual contrast enhancement, decision and memory \citep[p. 95-153]{levine2000introduction}. Motivated by connectivity in the brain, researchers have devised various competitive neural networks \citep[p. 201-203]{yegnanarayana2009artificial}.

In a typical arrangement, a neuron forms excitatory connections with nearby neurons and spreads inhibitory connections to more distant ones. In a neural population with that arrangement, given constant input signal, a neuron or neuronal subpopulation will emerge as winner because the neuron or neuronal subpopulation that is initially activated strongly enough will suppress its neighbours, dampen theirs inhibition upon itself to become more and more activated. This dynamics is called \textit{winner-take-all} (WTA).

Obviously, implementing this dynamics is neither practical nor necessary for ANN. Computer scientists model its final result (when the network has become stable) by a \textit{hard WTA gate} which outputs an 1 for the maximal input and 0's for other inputs. Some variants were also created: \textit{k-WTA gates} which emit $k$ 1's for the most activated inputs and \textit{soft WTA gates} which emit a real value proportional to the rank of each input. Surprisingly, those simple constructs possesses a vast computing power: a feed-forward network with a single $k$-WTA unit can compute any boolean function and with a single soft WTA unit, it can approximate any continuous function arbitrarily well (i.e. an universal function approximator) \citep{maass2000computational,maass1999neural}.

In my research, I made use of \textit{local winner-take-all} (LWTA) blocks inspired by \cite{srivastava2013compete}. A layer is divided into blocks of equal size. Within each block, each neuron receives the activation of only one neuron at its position in the previous layer. The neuron which gets the greatest input will be activated with a strength equal to that input. Other neurons in the same layer is inactive (see Figure \ref{fig:lwta}).

To formally define a LWTA layer, we need to modify equations (\ref{eq:z_i}-\ref{eq:a_i}) into:
\begin{equation}\label{z_i(lwta)}
z^{(i)} = a^{(i-1)}
\end{equation}
\begin{equation}
a^{(i)}_j = g_{\mathit{off}(j)}(z^{(i)}_{b(j)}, z^{(i)}_{b(j)+1},..., z^{(i)}_{b(j)+s-1})
\end{equation}
where $s$ is block size, $b(j)= \lfloor \frac{j}{s} \rfloor$ is the start index of unit $j$'s block, $\mathit{off}(j) = (j \mod{s})$ is its offset in the block and $g_k(\cdot)$ is the \textit{competition function} at offset $k$:
\begin{equation}\label{eq:competition-function}
g_k(z_1,...,z_s) = 
	\begin{cases}
		z_k & \mbox{if } z_k \geq z_l, \forall l=1,...,s \\
		0 & \mbox{otherwise}
	\end{cases}
\end{equation}
In case of multiple winners, the last one is given precedence.

At training time, while a sigmoidal or piecewise linear unit can be considered a feature detector which responds to some activation pattern of the previous layer, a LWTA block can be thought of as where detected features compete to explain the expected output. When two features which possibly explain for a training example exist but only one is chosen, we say that one \textit{explains away} the other. This works as a simple strategy for disentangling.

To draw a comparison between LWTA and rectifier as an activation function, we can rewrite \eqref{eq:competition-function} as:
\begin{equation}\label{eq:lwta-activation-function}
g_k'(z_k) = 
	\begin{cases}
		z_k & \mbox{if } z_k \geq z^*_k \\
		0 & \mbox{otherwise}
	\end{cases}
\end{equation}
where $z^*_k = \max\{z_i|i \neq k\}$. \eqref{eq:lwta-activation-function} is a generalization of \eqref{eq:rectifier-activation-function}. It is easy to point out that LWTA also has sparsifying power and satisfy \eqref{eq:T-iff-E} as rectified linear does.

A potential problem of LWTA as an activation function occurs when $z^*_k < 0$. In this case, the function $g_k(\cdot)$ is not monotonic: it is zero $\forall z_k < z^*_k$, fails under zero at $z^*_k$ and then gradually rises over zero and to infinity. This behavior is undesirable and can be corrected by rectifying $z^*_k$.

Notice that linear combination is absent in \eqref{z_i(lwta)} (which is different from \citealp{srivastava2013compete}). This modification enables LWTA layers to act as purely competitive layer and combine directly with other nonlinearity.


%\ifpdf
%  \pdfbookmark[2]{bookmark text is here}{And this is what I want bookmarked}
%\fi
% ------------------------------------------------------------------------


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
