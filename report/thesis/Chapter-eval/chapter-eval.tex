\chapter{Evaluation methodology}\label{chap:eval}
\ifpdf
    \graphicspath{{Chapter-eval/figs/PNG/}{Chapter-eval/figs/PDF/}{Chapter-eval/figs/}}
\else
    \graphicspath{{Chapter-eval/figs/EPS/}{Chapter-eval/figs/}}
\fi

I compare my models to baseline models that are widely used in the literature. All models operated on a SVD and a NNSE space in three tasks. 

\section{Baseline models}\label{sec:baseline}

I compared my models against four popular models. Element-wise multiplication (Equation \ref{eq:mul}, \code{mul}) is a simple operation model but achieve competitive performance in many tasks. 
Lexical function (Equation \ref{eq:lexfunc}, \code{lexfunc}) represents its own paradigm. 
Weighted additive (Equation \ref{eq:wadd}, \code{wadd}) and full additive (Equation \ref{eq:fadd}, \code{fadd}) represent parametrized models. 
Besides, I also use observed phrase vectors (i.e. no composition, \code{obs}) as a base line.
Kintsch's nearest neighbour model has shown poor results in previous researches therefore not included.

Element-wise multiplication is not suitable to matrices with many negative values. 
When $u_i$ and $v_i$ are both positive, their composition $p_i$ should be highly positive. 
When they are both negative, the composition should be negative but element-wise multiplication results in a positive element instead. When they bear different signs, it is unclear whether $p_i$ should be positive or negative. 
A SVD matrix is typically half positive and half negative. 
Due to limited time budget, I did not implement a solution for this problem (cf. \citealp{Turney2012}). 
Hence, element-wise multiplication performance on SVD space should be disregarded in next chapters.

\section{Tasks}

\subsection{Seven-choice noun-modifier questions}

\cite{Turney2012} created a dataset that asks composition models to point out, among seven words, the synonymous word of a phrase.

There are 2180 questions, each consists of a bigram and seven words. The bigram is a noun phrase. The first word is the correct answer. Two next words are the noun and the modifier. The fourth is a synonym or hypernym of the modifier and the fifth is a synonym or hypernym of the head. The last two choices are randomly selected nouns. Table \ref{tab:question} shows a sample question. 

Turney argued that humans may use the knowledge that \textit{dog house} cannot mean either \textit{dog} or \textit{house} alone to establish a constraint that eliminate choices (2) and (3) in all questions. 
In his experiments, he found that all methods greatly benefited from this constraint.

In my experiment, different from Turney's, precision is defined as the ratio between the number of correct answers and all answers, and an additional measure of coverage is reported that is equal to the ratio between the number of answers to questions. 
A model will not give an answer in cases that the best choice has a zero or negative similarity with the given phrase. A word and a phrase are of zero similarity if their vectors are perpendicular, the word is missing in the space, or the phrase cannot be composed by the composition model. 
Negative similarity is understood as signalling antonymy therefore not considered as a valid answer.

Besides, I used only 743 questions in which words occur frequently enough in my corpus. A question was included if it satisfies: (i) the modifier, head and foils have a frequency in our concatenated corpus of at least 20 occurrences (ii) the modifier must have adjective as its most frequent tag in the corpus. Foils that are absent or occur with frequency less than 20 are removed but the question it belongs to is retained. As a result, 237 questions miss 1 foil, 42 questions miss 2 foils and 4 questions lack of 3 foils.\footnote{I thank my advisor, Professor Marco Baroni, for preparing this dataset.}

\begin{table}[h]
\begin{center}
\begin{tabular}{l c l}
\hline
Stem & & dog house \\
\hline
Choices: & (1) & kennel \\
 & (2) & dog \\
 & (3) & house \\
 & (4) & canine \\
 & (5) & dwelling \\
 & (6) & effect \\
 & (7) & largeness \\
\hline
Solution: & (1) & kennel \\
\hline
\end{tabular}
\end{center}
\caption[An example of a seven-choice noun-modifier composition question]{An example of a seven-choice noun-modifier composition question (adopted Table 13 in \cite{Turney2012}). A model must find the choice that is synonymous with the phrase in the question. The first choice is always the correct answer.
\label{tab:question}}
\end{table}

\subsection{Verb disambiguating}

In \cite{mitchell2008vector}, evaluated models had to provide a similarity score for pairs of phrases. 
A phrase in each pair was made of a noun and one of two intransitive verbs -- one was called target verb and the other was call landmark verb.
Landmark verbs were chosen in distinct Wordnet synsets of a target verb so that they can be divided into sets of low and high similarity.
The same pair of target and landmark verbs can bear high or low similarity depending on the accompanying noun (see Table \ref{tab:verb-disambiguating}). 

The score was assimilated to human judgements (each pair is judged by multiple participants) according to Spearman's $\rho$. Also called Spearman's rank correlation coefficient, this index lies in [-1, 1] where $\rho= 1$ means one variable monotonically increases as the other increases and $\rho= -1$ corresponds to a decreasing monotonic trend between them. The larger $\rho$ is, the better a model.

Moreover, similarity scores are separated according to high or low similarity. Average high similarity score is expected to be greater than average low quality scores and the two populations should be statistically different.

\begin{table}[h]
\begin{center}
\begin{tabular}{lllll}
\hline
 & Noun & Reference & High & Low \\\hline
The & fire & glowed & burned & beamed \\
The & face & glowed & beamed & burned \\\hline
The & child & strayed & roamed & digressed \\
The & discussion & strayed & digressed & roamed \\\hline
The & sales & slumped & declined & slouched \\
The & shoulders & slumped & slouched & declined \\\hline
\end{tabular}
\end{center}
\caption[Examples taken from verb disambiguating dataset]{Examples from \cite{mitchell2008vector}. Depends on the disambiguating noun, a landmark verb can have high or low similarity with the reference (target) verb. Each tuples of a noun, a target verb and a landmark verb is provided with multiple similarity judgements.
\label{tab:verb-disambiguating}}
\end{table}

\subsection{Short phrase similarity}

\cite{mitchell2010} contributed another dataset. They asked participants to rate the similarity of two short phrases of either adjective-noun combinations, verb-object combinations or compound nouns. The rate ranges from 1 for no similarity to 7 for completely synonymous (see Table \ref{tab:short-phrase} for examples). A pair of phrase can be judged by more than one participants. 

A composition model assigns a similarity score for each pair of phrases. The Spearman's $\rho$ between its similarity scores and that of humans is then computed.

\begin{table}[h]
\begin{center}
\begin{tabular}{clllllc}
\hline
P & Type & \multicolumn{2}{l}{First phrase} &  \multicolumn{2}{l}{Second phrase} & Rating \\\hline
1 & verb-object & support & offer & help & provide & 7 \\
2 & adjective-noun & high & price & short & time & 1 \\
4 & compound nouns & assistant & secretary & company & director & 4 \\\hline
\end{tabular}
\end{center}
\caption[Examples of short phrase similarity judgement task]{Three rows in \cite{mitchell2010} dataset show three types of phrase and high, medium or low similarity judgement. P = Participant.
\label{tab:short-phrase}}
\end{table}

\section{Data}

I used a combination of UkWaC \citep{Ferraresi2007}, Wikipedia and BNC as a corpus. This corpus contains about 2.9 billion tokens accompanied by full dependency parsing and takes up a total size of 20.70GB of gzip-compressed files on disk. Obviously, the corpus covers a very wide range of topics and a huge vocabulary.

% Wikipedia: http://wacky.sslmit.unibo.it/doku.php?id=corpora

%[minh.lengoc@masterclic4 data]$ ls -l -H *.xml.gz | cut -f 1
%-rw-r--r-- 1 marco.baroni domain users  690723296 Apr  9  2012 bnc.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 3102909378 Apr  9  2012 ukwac1.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 3104510997 Apr  9  2012 ukwac2.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 3105827213 Apr  9  2012 ukwac3.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 3104925400 Apr  9  2012 ukwac4.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 2690713228 Apr  9  2012 ukwac5.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 1567462218 Apr  9  2012 wikipedia-1.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 1654186771 Apr  9  2012 wikipedia-2.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 1654444461 Apr  9  2012 wikipedia-3.xml.gz
%-rw-r--r-- 1 marco.baroni domain users 1546952290 Apr  9  2012 wikipedia-4.xml.gz

% sizes:
% UkWaC = 2b
% Wikipedia = 800m
% BNC = 100m

\subsection{Word space}

All words that appear in evaluation tasks are included as rows in co-occurrence matrix.
To discover the latent semantics of those words, more data is needed. 
I gathered words that appear at least 100 times in the corpus and have at least 2 characters. I then sampled them so that the rows of co-occurrence matrix would count to 230K. 
This is the threshold for my current implementation to work properly.

As discussed in Section \ref{sec:stats-methods}, I used within-sentence words as contexts. 10K contexts that show the most co-occurrences with target words are chosen to be columns of co-occurrence matrix.

After construction, there were 703,841,659 non-zero cells in the co-occurrence matrix, with a cell density of 30.60\%. 
Applying PPMI resulted in a matrix of 327,667,591 non-zero cells which translates into 14.25\% density. 

For SVD space, the implementation in \cite{Dinu2013} has been used to create a space of 1,089 dimensions (approximately 1,000 dimensions as the exact number is not obtainable with current implementation and matrix size). The matrix was dense (nearly 100\%) with 250,467,713 non-zero cells.

For NNSE space, following \cite{Murphy2012}, I applied SVD to reduce dimensionality to about 2,000 (more precisely 2,003) and then used NNSE dimensionality reduction (sparsity $\lambda=0.05$) by SPAMS toolbox \citep{Mairal2010} to create an exactly 1,000-dimensional space. 31,218,522 cells were non-zero, equivalent to a density of 13.57\%.

\subsection{Observed phrase space}

The observed baseline model works by looking up phrases in a prepared space containing all phrases that appear in evaluation tasks.
Co-occurrences between phrases and context words are gathered in the same way as for word space.
All transformations applied on word space, in order, are also performed on observed phrase space in a way that preserves their meaning.

For PPMI smoothing, assume the notation of Equation (\ref{eq:p_ij}-\ref{eq:x_ij}) and let $f'_{kj}$ is the co-occurrence count of phrase $k$ and context $j$, the transformed matrix is defined by:
\begin{equation}
p'_{kj} = \dfrac{f'_{kj}}{\sum_i\sum_j f_{ij}}
\end{equation}
\begin{equation}
p'_{k*} = \dfrac{\sum_j f'_{kj}}{\sum_i\sum_j f_{ij}}
\end{equation}
\begin{equation}
\mathrm{pmi}'_{kj} = \log \left( \frac{p'_{kj}}{p'_{k*}p_{*j}} \right)
\end{equation}
\begin{equation}
x'_{kj} = \begin{cases}
\mathrm{pmi}'_{kj} & \mbox{if } \mathrm{pmi}'_{kj} > 0 \\
0 & \mbox{otherwise}
\end{cases}
\end{equation}

Given $V_r$ defined in Section \ref{sec:svd} and $X'$ a co-occurrence or transformed matrix, the SVD matrix for phrases is:
\begin{equation}
A' = X'V_r
\end{equation}

Similarly, given dictionary $D$ obtained by solving \eqref{eq:argmin_AD}, the NNSE phrase matrix can be found by solving:
\begin{equation}\label{eq:argmin_AD}
\arg \min_{A'} \sum_{i=1}^{m} \left( \left\|X'_{i,:} - A'_{i,:} D\right\|^2 + \lambda \left\|A'_{i,:}\right\|_1 \right)
\end{equation}

\subsection{Training phrase space}

For the parametrized and lexical function models to work, I trained them on about 20K phrases of relevant syntactic relations from the corpus. None of them appeared in observed phrase space. The co-occurrence counting and matrix transformation are identical to the previous section.

%TODO generalization power?
To assess the generalization power of composition models, I created two different datasets. 

For \textit{unbalanced} dataset, the most frequent phrases are included while no attention is paid to the distribution of training examples for each word. 
This dataset contains 394 distinct head words with $11.93 \pm 17.15$ training examples each (the later figure is standard deviation) 
and 373 distinct dependent words, each one has $13.73 \pm 23.54$ training examples.

For \textit{balanced} dataset, I limited the number of phrases for each head word by 20K divided by the number of required head words. 
The same restriction applied to dependent words. 
Most frequent phrases are then added or random phrases are removed to meet the predefined number of phrases.
This results in a phrase space of 712 distinct head words having $18.01 \pm 6.62$ phrases each and 677 dependent words having $19.95 \pm 13.27$ phrases each.

\section{Implementation and hyperparameters}

The implementation of baseline models were included in DISSECT toolkit \citep{Dinu2013} while neural network models were implemented with the help of Pylearn2 \citep{goodfellow2013pylearn2}. I used default configurations and hyperparameters whenever they are available.

In neural network models, each hidden layer has 6,000 units while the size of input and output layers depends on the dimensionality of the space. For LWTA layers, the block size was 5.

All neural network layers were sparse-initialized, i.e. initializing a random subset of 30 weights selected from thousands of weights of each neuron, by samples from a standard normal distribution. 

Conjugate batch gradient descent was used as training procedure. A random portion of 10\% of a dataset is reserved for validation. The training is terminated when the validate error is not decreased by at least 0.1\% in 5 consecutive epochs or a maximum number of 150 epochs is exceeded. Then the parameter setting that leads to smallest validate error is taken.

% ------------------------------------------------------------------------


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
