\chapter{Compositional distributional semantics}\label{chap:cds}
\ifpdf
    \graphicspath{{Chapter-cds/figs/PNG/}{Chapter-cds/figs/PDF/}{Chapter-cds/figs/}}
\else
    \graphicspath{{Chapter-cds/figs/EPS/}{Chapter-cds/figs/}}
\fi

\epigraph{You shall know a word by the company it keeps.}{J. R. Firth}
What the epigraph essentially means is that the pattern of contexts in which a word occurs is an important source of knowledge to understand the word's meaning. This idea is called \textit{distributional hypothesis}, hence the name distributional semantics.

In natural language processing literature, the distributional hypothesis is exploited extensively. In a line of research, researchers gather statistics of a word in a large corpus into a vector representing it. This general idea has appeared as early as 1990 with latent semantic indexing \citep{deerwester1990indexing}. Since then, researches have proliferated with many different techniques regarding matrix types, matrix transformation, dimensionality reduction, similarity measure and recently modalities and composition methods (see \citealp{turney2010} and \citealp{Mitchell2011} for reviews).

Another line of research makes use of machine learning techniques to induce word representation. 
The employed techniques are diverse, including, but not limited to, 
clustering \citep{Brown92class-basedn-gram}; 
hidden Markov models, both shallow and deep \citep{Huang2014}; 
restricted Boltzman machines (e.g. \citealp{DBLP:conf/icml/DahlAL12}); 
and neural networks, shallow (e.g. \citealp{bengio2003neural}) and deep (e.g. \citealp{Collobert2011}). 
These researches belong to a larger theme of machine learning called \textit{feature learning} or \textit{representation learning} (see \citealp{bengio2013representation} for a review).

%Researchers often refer to the former as \textit{distributional semantics} and the later \textit{distributed representation}. In my opinion, this is a poor choice of terminology because regarding the source of knowledge, both of them are distributional while regarding the form of representation, both are distributed (as opposed to one-hot or relational representation). Without large corpora, machine learning algorithms have nothing to work on and without a vector space, statistics of contexts cannot be stored. 
% they don't write it down so I can't cite any work with this distinction
Recently, these two lines of research started to converge with works that make use of methods from both of them \citep{Sergienya2014} and evaluate them in the same context \citep{Baroni2014a}.

In this research, I was only able to use statistical methods (corresponding to \textit{count models} in \citealp{Baroni2014a}). Models emerged from these methods were shown to be powerful tools to approximate linguistic and cognitive meaning, achieving human level performance on synonymy tests (cf. \citealp{Mitchell2011}). \cite{Baroni2014}, by referring a large body of research, has argued that these models ``\textit{are} models of a significant part of meanings as it is encoded in human linguistic competence''. Apart from theoretical accomplishments, they have been used to tackle some applicative problems such as terminology mining \citep{Sahlgren2009}, recommender systems \citep{Musto2010}, automated thesaurus construction and machine translation \citep{Dumais2003,turney2010}.

Although I used statistical methods to derive vectors for evaluation, my composition models do not depend on any particular space any can certainly work well with, for example, neural representations.

Next sections will touch some important aspects of vectors, vector space construction and composition.

\section{Vectors as meaning carriers}\label{sec:vectors}

In mathematics, a vector space is a structure whose elements can be added or multiplied by a scalar. 
Elements of a vector space are called vectors, without any restriction on their nature, they can be functions for example. 
Scalars are numbers, also not restricted to real numbers but can be complex numbers for example. 

In current context, by \textit{vector space}, I generally mean a special kind of vector spaces, namely \textit{real coordinate space}. 
A real coordinate space of dimensionality $n$, denoted by $\mathbb{R}^n$, is a structure of vectors (yes, \textit{real} vectors) $\vec{u} = [u_1,...,u_n], u_i \in \mathbb{R}, \forall i=1,...,n$
(I will drop the overhead arrow in subsequent formulae)
with two operators, the addition of two vectors and the multiplication of a vector and a real number. 
In this thesis, I will use \textit{space} or \textit{vector space} to mean \textit{real coordinate space}.

A trait of vector spaces which can be easily overlooked is that it can encode almost everything. All assignations of a set of boolean variables can be represented by vectors in a vector space. The same applies for categorical, integer and real variables. Vectors coming from two or more spaces of dimensionality $n_1,...,n_s$ can be encoded by vectors of dimensionality $n=\sum_{i=1}^s n_i$. 
% Pairs or tuples --> concatenate, unrelated vectors --> padding zeros
A matrix $A \in \mathbb{R}^{n \times n}$ can be represented by a vector $u \in \mathbb{R}^{nn}$ and the corresponding linear mapping can be rewritten as:
\begin{equation}
Av = 
\left[ \begin{array}{l}
u_1 v_1 +...+  u_n v_n \\
u_{n+1} v_1 +...+  u_{n+n} v_n \\
...  \\
u_{n(n-1)+1} v_1 +...+  u_{nn} v_n   \end{array} \right]
\end{equation}
Tensors and multi-linear mappings can be expressed in an analogous manner.
Polynomials and in general any function family that can be parametrized by a finite set of parameters can comfortably live in a vector space. 
To foster imagination, the whole brain of any person at any point in time can be encoded by a big vector. 

As a consequence, the argument that nouns and adjectives 
% don't mention sentences, they are not included in our model
live in different spaces is equivalent to the argument that they live in separate regions of a space. Treating nouns as vectors, adjectives as matrices and transitive verbs as tensors of rank 3 is equivalent to representing them in different regions of different sizes in one vector space. Whenever vector spaces can model meaning, we can do so with one vector space. This observation will enable us to directly compare radically different methods in next sections.

%The representational power of vectors makes them very attractive to scientists.

A potential limitation of vector spaces is the fact that they are bounded. A vector space of $n$ dimensions cannot encode more than $n$ real variables without loss of information. It is in sharp contrast with the productivity of language which allows a language user, with adequate idealization (for example, infinite amount of time and energy), produce or understand an infinitely long sentence. This is a part of the attack \citep{fodor1988connectionism} of classicists to connectionists in 1980s. However, if we do not go too far and claim for all language or cognition processes, we can get rid of this debate.

Just as compositionality principle does not require a definition of meaning, a vector space model (VSM) is taken to be a model of meaning in the sense that it captures a part of the information content of meaning. The goodness of a model is understood in operational term, not being how faithful it is to the elusive meaning but being how good it can perform language tasks. 

Traditionally, similarity judgement tasks are central to distributional semantics to the point that in a recent paper, \citep{bernardi2014distributional} has included a similarity measure into her definition of distributional semantics models. To avoid adding components for composition, reasoning, language modeling, production, etc., I will regard a VSM simply as a function that maps lexical units 
% I have a temptation to use "linguistic units" to include compositional phrases, clauses, sentences,... but they should NOT be included, should they?
to vectors. It is necessarily a partial function as new words appear everyday and not all of them that we can guess.

\section{Statistical methods for model construction}\label{sec:stats-methods}

The construction of a model starts with gathering contexts for each words, summarized in a matrix, and then transforms this matrix to improve its quality (see \citealp{turney2010} for a review and \citealp{kiela2014systematic} for a recent comparison of alternatives).
Following recent researches \citep{DBLP:journals/corr/abs-1301-6939,Boleda2013}, I used \textit{within-sentence co-occurrences} as contexts, \textit{Positive Point-wise Mutual Information} (PPMI) for \textit{weighting} and \textit{Singular Value Decomposition} (SVD) for \textit{dimensionality reduction}. I also experimented with a transformation called \textit{non-negative sparse embedding} for reasons explained in Section \ref{sec:nnse}.

Different types of contexts capture different information about a word. In \cite{Sahlgren2006}'s distinction, co-occurrences give us \textit{paradigmatic} relations between words (words that can substitute each other tend to be similar) in contrast to \textit{syntagmatic} relations (words that can syntactically combine tend to be similar). I chose co-occurrence contexts for their suitability to composition.

Co-occurrences can be defined within a moving window (for example, including 2 nearest words in the left, 3 in the right), within a syntactic relation or within a sentence. However when it comes to contexts of phrases (which can be discontinuous, for example the phrase \textit{good days} can be extracted from a larger phrase \textit{good old days}), only within-sentence contexts are well-defined. 

Contexts themselves can take different forms such as stems, lemmas, original words, POS tagged words. No particular choice has been found to outperform all others in all tasks or do significantly better in composition tasks. In this research I used only bare words.

This except is taken from COCA \citep{Davies2008}:
\begin{quote}
Commonly eaten species of Fabaceae, Poaceae, and Amaranthaceae are rarely eaten raw [...]
\end{quote}
The words \textit{commonly}, \textit{rarely}, \textit{raw} tell us little about \textit{Fabaceae}, \textit{Poaceae}, and \textit{Amaranthaceae} while the words \textit{eaten} and \textit{species} suggest that they are some kinds of plants. 
Weighting schemes take into account the fact that different contexts bear different amount of information and try to assign appropriate weights to them.

PPMI is a popular weighting scheme. Mutual information is defined as the probability that two events co-occur compared to the expected probability if they co-occur just by chance. If there is no relation between a word and a context, they will co-occur just as frequent as they do purely by chance and their mutual information is zero. On the other hand, if there is an interesting relation between them, they may co-occur more often, rendering their mutual information positive. Let $f_{ij}$ be the value of frequency matrix at row $i$, column $j$ and assume summation runs through all possible values of $i$ and $j$. PPMI can be defined by the following equations (adopted from \citealp{turney2010}):
\begin{equation}\label{eq:p_ij}
p_{ij} = \dfrac{f_{ij}}{\sum_i\sum_j f_{ij}}
\end{equation}
\begin{equation}
p_{i*} = \dfrac{\sum_j f_{ij}}{\sum_i\sum_j f_{ij}}
\end{equation}
\begin{equation}
p_{*j} = \dfrac{\sum_i f_{ij}}{\sum_i\sum_j f_{ij}}
\end{equation}
\begin{equation}
\mathrm{pmi}_{ij} = \log \left( \frac{p_{ij}}{p_{i*}p_{*j}} \right)
\end{equation}
\begin{equation}\label{eq:x_ij}
x_{ij} = \begin{cases}
\mathrm{pmi}_{ij} & \mbox{if } \mathrm{pmi}_{ij} > 0 \\
0 & \mbox{otherwise}
\end{cases}
\end{equation}

Because a co-occurrence matrix often has tens or even hundreds thousands of columns, it is often needed to transform it into smaller dimensionality for efficient storage and usage. Via this transformation, researchers also hope to uncover latent meaning, reduce noise in data or manipulate sparseness. Dimensionality reduction is an important component in distributional semantics apparatus and will be discussed in details in the next section.

%To build a model for phrases, \cite{Dinu2013} put forward an useful... peripheral space

\section{Dimensionality reduction}

Let $X \in \mathbb{R}^{m \times n}$ be the input representation, which can be either raw cooccurrence counts or the result of some transformation. Each of $m$ rows is a $n$-dimensional vector representing of a word. We would like to transform $X$ into matrix $A \in \mathbb{R}^{m \times k}$ where each row represents a word in the new $k$-dimensional space.

Usually, $k < n$, hence the name dimensionality reduction. However, \textit{overcompleteness}, that is the use of more dimensions than that of original space, has been proved useful in certain cases for reasons discussed in section \ref{sec:nnse}.

\subsection{Singular Value Decomposition (SVD)}\label{sec:svd}

SVD is a matrix factorization technique that identifies dimensions with greatest variance, and has the power of combining similar dimensions into common components while possibly reducing noise in data.

Formally, SVD is defined as the decomposition:
\begin{equation}
X = U \mathit{\Sigma} V^T
\end{equation}
where $q$ is the rank of $X$, $U \in \mathbb{R}^{m \times q}, \mathit{\Sigma} \in \mathbb{R}^{q \times q}, V \in \mathbb{R}^{n \times q}$. $\mathit{\Sigma}$ is a diagonal matrix consists of singular values.

Here, we actually make use of \textit{truncated SVD} where only $r = \min \{q, k\}$ largest singular values are kept in the result.
Let $\mathit{\Sigma}_r$ is the matrix formed by removing all rows and columns of $\mathit{\Sigma}$ except those containing $r$ largest singular values. $U_r$ and $V_r$ are the matrices produced by selecting the corresponding columns from $U$ and $V$. The transformed matrix will be:

\begin{equation}
A = U_r\mathit{\Sigma}_r
\end{equation}

Truncated SVD is thought to have one or all of those effects:
latent meaning discovering \citep{deerwester1990indexing,landauer1997solution}, 
noise reduction \citep{rapp2003word}, 
high-order co-occurrence (first-order co-occurrence is when two words appear in identical contexts, high-order co-occurrence is when they appear in similar contexts) \citep{landauer1997solution,lemaire2006effects}, 
sparsity reduction (as a way to fight data scarcity) \citep{vozalis2003analysis}.
It has also been successfully applied to solve many tasks in a wide range in NLP applications and in the modelling of cognitive tasks and brain activity.

\subsection{Non-Negative Sparse Embedding (NNSE)\label{sec:nnse}}

Singular Value Decomposition (SVD) has proven its effectiveness and become the de facto standard for dimensionality reduction. However, SVD creates a representation which is hard to interpret and not cognitively plausible.

Cognitive scientists have demonstrated that the representation in the brain is sparse, i.e. a relatively small number of neurons is active for each stimulus \citep{field1994goal, Field:87, olshausen2004sparse}. 

Given that the brain has always been the champion in natural language processing, it is natural to mimic the way it works for not only cognitive plausibility per se but also for performance. Following this line of thought,
\cite{Sivaram2010}, \cite{6320674}, and \cite{Vinyals2012} devised sparse representations for state-of-the-art speech recognition and 
\cite{bagnell2008differentiable} demonstrated that sparse representation facilitates handwritten digit classification as well as sentiment regression 
(see \citealp{bengio2013representation} for more information and discussion).

In distributional semantics, \cite{Murphy2012} took the first step in this direction by showing that vector space models created using Non-Negative Sparse Encoding (NNSE) is much more interpretable and also competitive with SVD in various behavioural and neuro-decoding tasks.

According to Murphy et al., NNSE solves the following optimization problem:
\begin{equation}\label{eq:argmin_AD}
\arg \min_{A, D} \sum_{i=1}^{m} \left( \left\|X_{i,:} - A_{i,:} D\right\|^2 + \lambda \left\|A_{i,:}\right\|_1 \right)
\end{equation}
where $D \in \mathbb{R}^{k \times n}, D_{i,:}D_{i,:}^\mathrm{T} \leq 1, \forall 1 \leq i \leq k$\\
and $A \in \mathbb{R}^{m \times k}, A_{i,j} \geq 0, \forall 1 \leq i \leq m, \forall 1 \leq j \leq k$

D is called a \textit{dictionary} with $k$ entries, each encodes a repetitive pattern in data. $\lambda \in \mathbb{R}$ is a hyperparameter and $\left\| A_{i,:} \right\|_1 = \sum_{j=1}^{k} |A_{i,j}|$ is the $\mathrm{L}^1$ regularization of the $i^\mathrm{th}$ row of $A$. The goal is to find a sparse and non-negative matrix $A$ that best explains the data as the combination of entries in dictionary $D$. The norm of rows in $D$ is restricted by 1, otherwise one can easily trade the scaling of $A_{i,j}$ for the scaling of $D_{i,:}$, thereby effectively disable the regression.

In our complex world, it is reasonable to believe that there are many latent factors that explains the meaning of a word. While the dimensionality of SVD cannot exceed the rank of the coocurrence matrix, NNSE can create an overcomplete dictionary that have many more dimensions.

Although an overcomplete dictionary has many redundant bases, the $\mathrm{L}^1$ penalty is able to choose the most appropriate bases and zero the others, even if they have a high degree of correlation with the data. 

This is an example of \textit{explaining away} effect, which happens regularly in everyday life and is also known as discounting principle in psychology \citep{ahn1996causal}. It is the tendency of people, when explaining an event, to reduce the probability of one cause in the presence of a more likely cause. For example, when one find the lawn in front of her house wet, she might think that it rained yesterday. However, if she knows that sprinklers water the lawn every morning, she might think it probably didn't rain. Here, an observation has rendered two otherwise independent events to become mutually exclusive. In other words, one event has explained away the other. Probabilistically speaking, two independent events can become conditionally dependent in the light of new evident.

\section{Composition models for vector spaces}

In a survey covering many composition models, \cite{mitchell2010} have proposed an unifying formula for phrase composition:
\begin{equation}
p = f(u, v, R, K)
\end{equation}
where $u, v$ are vectors representing two constituents, $R$ is the syntactic relation between them, $K$ is the knowledge of a language user, and $p$ is the resulting phrase. They then ignored $K$ for simplicity.

I argue that $K$ is not an argument of this function. When an utterance is presented to a language user, it is the words and their relations that appear to her while her knowledge has always existed and not yet changed. Therefore $K$ should be considered as a \textit{part} of the function instead. Put in other words, the existence of an input argument apart from the words and their relation is a violation of compositionality principle, the assumption that makes our research possible.

I will make explicit the knowledge contained in a composition function by a set of parameters $\theta$. In addition, because of the relatively small number of syntactic relations, we can learn a specialized set of parameters for each relation, hence:
\begin{equation}
p = f(u, v; \theta_R)
\end{equation}

This is the general formula of most, if not all, existing composition models for two words. As argued in Section \ref{sec:vectors}, we can assume a single vector space without loss of generality. However in some equations I will use different dimensionalities, matrices or tensors merely for better readability.

To capture the variation discussed in Chapter \ref{chap:ling}, a composition function must has necessary expressive power, reflected in the form of the function and the number of parameters. Based on this, I would like to divide them into four groups.

\subsection{Simple operation models}

The first group is characterized by a vector or tensor operation without any parameter. They cannot learn from the data or vary between syntactic relation. The simplest model is perhaps vector addition:
\begin{equation}
f(u,v) = u+v
\end{equation}

Element-wise multiplication achieves good results with a simple formula:
\begin{equation}\label{eq:mul}
f(u,v) = u \odot v = [u_1v_1,...,u_nv_n]^{\mathrm{T}}
\end{equation}

Tensor product maintains information at the cost of explosive dimensionality:
\begin{equation}
f(u,v) = u \otimes v = \left[ \begin{array}{cccc}
			u_1v_1 & u_1v_2 & \cdots & u_1v_n \\
			u_2v_1 & u_2v_2 & \cdots & u_2v_n \\
			\vdots & \vdots & \ddots & \vdots \\
			u_nv_1 & u_nv_2 & \cdots & u_nv_n \\
 		 \end{array} \right]
\end{equation}

Circular convolution tries to compress it by summing along transdiagonal elements:
\begin{equation}
f(u,v) = u \circledast v = [c_1,...,c_n]^{\mathrm{T}}
\end{equation}
where
\begin{equation}
c_i = \sum_j u_j \cdot v_{(i-j)}
\end{equation}

\subsection{Nearest neighbour models}

\cite{kintsch2001predication} called for the help of neighbours of the predicate in a predicate-argument structure:

\begin{equation}
f(u,v) = u+v+\sum_i n_i
\end{equation}
where $\{n_i\}$ stands for a predefined number of neighbours of the predicate which are also close to the argument.

The behavior of Kintsch's function is hard to characterized because it is influenced by an unknown part of the vocabulary. However some researches have shown its performance to be comparable to that of additive model \citep{mitchell2010}.

\subsection{Lexical function models}

In lexical function models, some words are considered as functions operating on other words. The composition function is simply function application or, when they are expressed in matrices and tensors, multiplication. In this aspect, lexical function models are similar to basic operation models.

However lexical function models do have the ability to learn from data. They differ from other models in that the parameters they fit fall out of the composition function into the VSM. In their treatment, some words are meant to compose. Surprisingly, some researches \citep{baroni2010nouns} find out that they are still comparable to each other individually.

This line of research starts with the composition of adjectives and nouns in  \citep{baroni2010nouns}:
\begin{equation}\label{eq:lexfunc}
f(A,v) = Av
\end{equation}
where $A \in \mathbb{R}^{d \times d}$ represents an adjectives and $v \in \mathbb{R}^d$ stands for a noun.

Since then researchers have developed models for determiners, intransitive verbs, transitive verbs. The approach is generalized by \cite{DBLP:journals/corr/abs-1301-6939}. Although elegant and effective, it faces practicality problem because of the explosive size of tensors (if nouns have one thousand dimensions, adjectives will have one million of them and for transitive verbs, one billion). \cite{Paperno2014} tries to fix this by describing each word by a tuple of a vector and some matrices where the number of matrices equal to the number of arguments that word can take. However a discussion of this model falls out of the scope of this writing.

So far, the most powerful lexical function model remains to be (multi-)linear.

\subsection{Parametrized models}

The simplest model with a parametrized composition function is \textit{weighted additive}:
\begin{equation}\label{eq:wadd}
f(u, v; \theta_R) = \alpha_R u + \beta_R v
\end{equation}

An improvement by mixing additive and multiplicative models was introduced in \cite{mitchell2008vector}:
\begin{equation}
f(u, v; \theta_R) = \alpha_R u + \beta_R v + \gamma_R (u \odot v)
\end{equation}

\cite{mitchell2010} proposed \textit{dilation} model which stretches the vector $v$ along the direction of $u$:
\begin{equation}
f(u, v; \theta_R) = (uu)v + (\lambda_R-1)(uv)u
\end{equation}

\textit{Full additive} model greatly increases the expressive power by using matrices to combine word vectors:
\begin{equation}\label{eq:fadd}
f(u, v; \theta_R) = A_R u + B_R v
\end{equation}
where $u,v \in \mathbb{R}^n$ are word vectors provided by a VSM and $A_R, B_R \in \mathbb{R}^{n \times n}$ are matrices to be learned.

\cite{Socher2011} used a single layer neural network to compute phrase vector:
\begin{equation}
f(u, v; \theta) = \tanh(W_e[u;v]+b_e)
\end{equation}
The net is placed in a recursive autoencoder and trained by minimizing reconstruction error of all nodes in a set of parse trees. The authors used the same set of parameters for all syntactic relations.

They also experimented with a deeper network, having the most expressive power in existing models:
\begin{equation}
h = \tanh(W_e^{(1)}[u;v]+b_e^{(1)})
\end{equation}
\begin{equation}
f(u,v; \theta) = \tanh(W_e^{(2)}h+b_e^{(2)})
\end{equation}

\cite{Socher2012} incorporated some ideas from lexical functions. They represented each word by a matrix-vector pair. The composition of two words $u=(A,a)$ and $v=(B,b)$ is carried out in two parts:
\begin{equation}
f_V(u, v; \theta) = g \left(W_V \left[ \begin{matrix}
	Ba \\
	Ab
\end{matrix} \right] + b \right)
\end{equation}
where $g$ is a sigmoidal function, for example sigmoid or tanh.
\begin{equation}
f_M(u, v; \theta) = W_M\left[ \begin{matrix}
	A \\
	B
\end{matrix} \right]
\end{equation}
\begin{equation}
f(u,v; \theta) = (f_M(u, v; \theta), f_V(u, v; \theta))
\end{equation}

All matrices and vectors are not predetermined but computed via training on a parsed corpus. Even for low dimensionality (e.g. 100 dimensions for each vector), the number of parameters grow too large. Therefore they restricted  the rank of matrices by setting: 
\begin{equation}
A = UV + diag(a)
\end{equation}
where $U \in \mathbb{R}^{n \times r}, V \in \mathbb{R}^{r \times n}, a \in \mathbb{R}^{n}$. The rank was fixed to be 3 in all their experiments.

% ------------------------------------------------------------------------

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../thesis"
%%% End: 
