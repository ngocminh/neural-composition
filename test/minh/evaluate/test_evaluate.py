'''
Created on Apr 22, 2014

@author: "Minh Le-Ngoc"
'''
import unittest
from minh.evaluate import evaluate
from collections import OrderedDict
from minh.evaluate.evaluate import print_summary
from StringIO import StringIO


class Test(unittest.TestCase):


    def test_print_summary(self):
        evaluate.data = [OrderedDict([('test1', 0.99), ('test2', 0.5)]),
                         OrderedDict([('test1', 0.8), ('test2', 0.7)])]
        s = StringIO()
        print_summary(s)
        self.assertEqual('test1\ttest2\n'
                         '0.99\t0.5\n'
                         '0.8\t0.7', s.getvalue().strip())


    def test_print_summary_conflicting_field_list(self):
        # different orders
        evaluate.data = [OrderedDict([('test1', 0.99), ('test2', 0.5)]),
                         OrderedDict([('test2', 0.8), ('test1', 0.7)])]
        with self.assertRaises(AssertionError):
            print_summary()


    def test_print_summary_conflicting_field_list2(self):
        # different fields
        evaluate.data = [dict([('test1', 0.99), ('test2', 0.5)]),
                         dict([('test2', 0.8), ('', 0.7)])]
        with self.assertRaises(AssertionError):
            print_summary()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_print_summary']
    unittest.main()