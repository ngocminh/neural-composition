# -*- coding: utf-8 -*-
'''
Created on Feb 13, 2013

@author: Lê Ngọc Minh
'''
import unittest
from minh.evaluate.turney import question, from_file

class QuestionTest(unittest.TestCase):

    def test_of(self):
        spec = "voiceless-j\tconsonant-n\tsurd-n\tunvoiced-j\tphone-n\tamorality-n\tsalp-n"
        self.assertEqual(7, len(spec.split("\t")))

        q = question(spec)
        self.assertEqual("voiceless-j", q.modifier)
        self.assertEqual("consonant-n", q.head)
        self.assertEqual("surd-n", q.correct_answer)
        self.assertEqual(4, len(q.foils))


    def test_choices(self):
        spec = "voiceless-j\tconsonant-n\tsurd-n\tunvoiced-j\tphone-n\tamorality-n\tsalp-n"
        q = question(spec)
        self.assertEqual(7, len(q.choices))


    def test_from_file(self):
        from_file("test-data/questions.10.lst", False)
        

if __name__ == '__main__':
    unittest.main()

