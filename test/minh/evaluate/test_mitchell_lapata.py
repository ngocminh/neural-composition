# -*- coding: utf-8 -*-
'''
Created on Mar 11, 2014

@author: Lê Ngọc Minh
'''
import unittest
from minh.evaluate.mitchell_lapata import VerbDisambiguating,\
    ShortPhraseSimilarity
from composes.similarity.cos import CosSimilarity


class Test(unittest.TestCase):


    def test_verb_disambiguating_simple(self):
        vd = VerbDisambiguating('test-data/mitchell-lapata.small.txt', CosSimilarity())
        result = vd.evaluate(self) # use this test case as a fake space, see get_sim()
        print result
        self.assertPropertyEqual(('rho', 0.3162), result.popitem(last=False))
        self.assertPropertyEqual(('high_score', 0.75), result.popitem(last=False))
        self.assertPropertyEqual(('low_score', 0.15), result.popitem(last=False))


    def test_short_phrases_simple(self):
        sp = ShortPhraseSimilarity('test-data/short-phrases.small.txt', CosSimilarity())
        result = sp.evaluate(self) # use this test case as a fake space, see get_sim()
        self.assertPropertyEqual(('rho', 0.3198), result.popitem(last=False))
        self.assertPropertyEqual(('rho-adj-noun', 0), result.popitem(last=False))
        self.assertPropertyEqual(('rho-verb-object', 0.7182), result.popitem(last=False))
        self.assertPropertyEqual(('rho-noun-noun', -1), result.popitem(last=False))
        
    
    def assertPropertyEqual(self, expected, actual):
        self.assertEqual(expected[0], actual[0], 
                         "Score name is not correct. '%s' was expected, '%s' was given."
                         %(expected[0], actual[0]))
        self.assertAlmostEqual(expected[1], actual[1], 4, 
                               "Score valie is not correct. Expected %.4f while actually %.4f" 
                               %(expected[1], actual[1]))
        
    
    def get_sim(self, p1, p2, sim):
        fake_similarity = {
                ('thought-n_stray-v', 'thought-n_roam-v'): 0.2,
                ('discussion-n_stray-v', 'discussion-n_digress-v'): 0.8,
                ('eye-n_stray-v', 'eye-n_roam-v'): 0.7,
                ('child-n_stray-v', 'child-n_digress-v'): 0.1,
                ('knowledge-n_use-v', 'influence-n_exercise-v'): 0.8,
                ('war-n_fight-v', 'battle-n_win-v'): 0.6,
                ('support-n_offer-v', 'help-n_provide-v'): 0.7,
                ('technique-n_develop-v', 'power-n_use-v'): 0.2,
                ('end-n_achieve-v', 'eye-n_close-v'): 0.3,
                ('national-j_government-n', 'cold-j_air-n'): 0.1,
                ('black-j_hair-n', 'dark-j_eye-n'): 0.6,
                ('northern-j_region-n', 'early-j_age-n'): 0.9,
                ('training-n_programme-n', 'research-n_contract-n'): 0.2,
                ('phone-n_call-n', 'committee-n_meeting-n'): 0.4
                }
        return fake_similarity[(p1, p2)]
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
