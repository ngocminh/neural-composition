'''
Created on Jan 8, 2014

@author: 8460p
'''
import unittest
from minh.space.neural import NeuralComposition, BlendingNeuralComposition,\
    _normalize_np
import numpy
from logging.config import fileConfig


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):        
        conf_path = "test-data/config/full.cfg"
        fileConfig(conf_path)
    

    #TODO
    def test_simple(self):
        NeuralComposition('test-data/model/linear.yaml', 100)


    def test_tanh(self):
        NeuralComposition('test-data/model/tanh.yaml', 100)


    def test_train_blending(self):
        mat1 = numpy.random.uniform(size=(5,40))
        mat2 = numpy.random.uniform(size=(5,40))
        phrase = numpy.random.uniform(size=(5,40))
        model = BlendingNeuralComposition('test-data/model/rectifier.blending.yaml', 
                                          dims=40, valid_set_portion=0)
        model._train(mat1, mat2, phrase)


    def test_train_unnormalized(self):
        mat1 = numpy.random.uniform(size=(5,40))
        mat2 = numpy.random.uniform(size=(5,40))
        phrase = numpy.random.uniform(size=(5,40))
        model = NeuralComposition('test-data/model/rectifier-unnormalized.yaml', dims=40,
                                  valid_set_portion=0)
        model._train(mat1, mat2, phrase)


    def test_train_normalized(self):
        mat1 = numpy.random.uniform(size=(10,40))
        mat2 = numpy.random.uniform(size=(10,40))
        mat1 = _normalize_np(mat1)
        mat2 = _normalize_np(mat2)
        phrase = numpy.random.uniform(size=(10,40))
        phrase = _normalize_np(phrase)
        model = NeuralComposition('test-data/model/rectifier.yaml', dims=40,
                                  valid_set_portion=0.5)
        model._train(mat1, mat2, phrase)


    def test_zero_row(self):
        mat1 = numpy.random.uniform(size=(10,40))
        mat2 = numpy.random.uniform(size=(10,40))
        mat1 = _normalize_np(mat1)
        mat2 = _normalize_np(mat2)
        mat1[0,:] = mat2[0,:] = 0
        phrase = numpy.random.uniform(size=(10,40))
        phrase = _normalize_np(phrase)
        model = NeuralComposition('test-data/model/rectifier.yaml', dims=40,
                                  valid_set_portion=0.5)
        model._train(mat1, mat2, phrase)


    def test_param_simple(self):
        NeuralComposition('test-data/model/rectifier.param.yaml', 100,
                          param_map={'hidden_size': 100})


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test_small']
    unittest.main()
