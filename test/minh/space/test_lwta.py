"""
Tests of the LWTA functionality.
"""
from pylearn2.scripts.tests.yaml_testing import limited_epoch_train
from pylearn2.utils import sharedX
import unittest
from minh.space.lwta import lwta, leaky_lwta
import numpy as np


__author__ = "Ian Goodfellow, Minh Ngoc Le"


class Test(unittest.TestCase):

    def test_lwta_simple(self):
    
        """Test simple cases."""
    
        example_input = np.zeros((2, 6))
    
        # begin block
        example_input[0, 0] = -2.5
        example_input[0, 1] = 1.3  # max
        example_input[0, 2] = 0.9
        # begin block
        example_input[0, 3] = -0.1  # tied for max
        example_input[0, 4] = -0.2
        example_input[0, 5] = -0.1  # tied for max
        # begin block
        example_input[1, 0] = 5.0  # max
        example_input[1, 1] = 4.0
        example_input[1, 2] = 3.0
        # begin block
        example_input[1, 3] = 0.0
        example_input[1, 4] = 1.0
        example_input[1, 5] = 2.0  # max
    
        output = lwta(sharedX(example_input), block_size=3).eval()
    
        num_zeros = (output == 0).sum()
        assert num_zeros == 8
    
        self.assertAllClose(output[0, 1], 1.3)
        self.assertAllClose(output[0, 5], -0.1)
        self.assertAllClose(output[1, 0], 5.0)
        self.assertAllClose(output[1, 5], 2.0)
    
    
    def test_leaky_lwta_simple(self):
    
        """Test simple cases."""
    
        example_input = np.zeros((3, 6))
    
        # begin block
        example_input[0, 0] = -2.5
        example_input[0, 1] = 1.3  # max
        example_input[0, 2] = 0.9
        # begin block
        example_input[0, 3] = 0.3  # tied for max
        example_input[0, 4] = 0.2
        example_input[0, 5] = 0.3  # tied for max
        # begin block
        example_input[1, 0] = 5.0  # max
        example_input[1, 1] = 4.0
        example_input[1, 2] = 3.0
        # begin block
        example_input[1, 3] = 0.0
        example_input[1, 4] = 1.0
        example_input[1, 5] = 2.0  # max
        # begin block
        example_input[2, 0] = -0.1  # clipped max = 0
        example_input[2, 1] = -0.2
        example_input[2, 2] = -0.3 
        # begin block
        example_input[2, 3] = -0.1  # clipped max = 0
        example_input[2, 4] = -0.2
        example_input[2, 5] = -0.1  # tied but less than zero 
    
        output = leaky_lwta(sharedX(example_input), 3, 0.01).eval()
    
        self.assertAllClose(output[0, 0:3], np.array([-0.025, 1.3, 0.009]))
        self.assertAllClose(output[0, 3:6], np.array([0.003, 0.002, 0.3]))
        self.assertAllClose(output[1, 0:3], np.array([5.0, 0.04, 0.03]))
        self.assertAllClose(output[1, 3:6], np.array([0.0, 0.01, 2.0]))
        self.assertAllClose(output[2, 0:3], np.array([-0.001, -0.002, -0.003]))
        self.assertAllClose(output[2, 3:6], np.array([-0.001, -0.002, -0.001]))
    
    
    def assertAllClose(self, a, b, rtol=1.e-5, atol=1.e-8):
        self.assertTrue(np.allclose(a, b, rtol, atol), 
                        "%s is different from %s" %(str(a), str(b)))

    
    def test_lwta_yaml(self):
        """Test simple model on random data."""
        limited_epoch_train("test/minh/space/lwta.yaml")


    def test_leaky_lwta_yaml(self):
        """Test simple model on random data."""
        limited_epoch_train("test/minh/space/leaky_lwta.yaml")


if __name__ == "__main__":
#     import sys;sys.argv = ['', 'Test.test_leaky_lwta_simple']
    unittest.main()
