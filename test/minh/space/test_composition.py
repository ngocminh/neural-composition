'''
Created on Apr 24, 2014

@author: "Minh Le-Ngoc"
'''
import unittest
from minh.space.composition import SyntaxSensitiveNeuralComposition


class Test(unittest.TestCase):


    def test_code(self):
        self.assertEqual('n_v', SyntaxSensitiveNeuralComposition.code(
                                ('knowledge-n', 'use-v', '')))
        self.assertEqual('j_n', SyntaxSensitiveNeuralComposition.code(
                                ('black-j', 'hair-n')))
        self.assertEqual('j_n', SyntaxSensitiveNeuralComposition.code(
                                ('slight-black-j', 'hair-n')))


    def test_code_no_pos(self):
        with self.assertRaises(ValueError):
            SyntaxSensitiveNeuralComposition.code(
                                    ('knowledge', 'use', ''))
        

    def test_group_by_code_empty(self):
        groups = SyntaxSensitiveNeuralComposition.grouped_by_code([])
        self.assertEqual(0, len(groups))
        

    def test_group_by_code_one(self):
        data = [('knowledge-n', 'use-v', '')]
        groups = SyntaxSensitiveNeuralComposition.grouped_by_code(data)
        self.assertEqual(1, len(groups))
        self.assertEqual([('knowledge-n', 'use-v', '')], groups['n_v'])
        

    def test_group_by_code_more(self):
        data = [('knowledge-n', 'use-v', ''),
                ('support-n', 'offer-v', ''),
                ('social-j', 'activity-n', ''),
                ('housing-n', 'benefit-n', ''),
                ]
        groups = SyntaxSensitiveNeuralComposition.grouped_by_code(data)
        self.assertEqual(3, len(groups))
        self.assertEqual([('knowledge-n', 'use-v', ''), 
                          ('support-n', 'offer-v', '')], groups['n_v'])
        self.assertEqual(2, len(groups['n_v']))
        self.assertEqual(1, len(groups['j_n']))
        self.assertEqual(1, len(groups['n_n']))

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_group_by_code']
    unittest.main()