# -*- coding: utf-8 -*-
'''
Created on Nov 15, 2013

@author: Lê Ngọc Minh
'''
import unittest
from composes.utils import io_utils
from minh.evaluate.examine import examine_all


class Test(unittest.TestCase):


    def test_simple(self):
        examine_all(io_utils.load('test-data/processed-domain-space.pkl'), 
                ['box-n', 'executive-j'])


    def test_whitespace(self):
        examine_all(io_utils.load('test-data/processed-domain-space.pkl'), 
                ['box-n  ', 'executive-j   '])


    def test_word_not_found(self):
        examine_all(io_utils.load('test-data/processed-domain-space.pkl'), 
                ['no-such-word'])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test']
    unittest.main()