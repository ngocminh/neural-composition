# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
from minh.experiment.experiment import run, defaults, tee, EXPERIMENT_SECT
import os
import shutil
import unittest
from logging.config import fileConfig


class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        shutil.rmtree("test-target")
        os.mkdir("test-target")
        
        conf_path = "test-data/config/full.cfg"
        cls.conf = ConfigParser(defaults)
        cls.conf.read(conf_path)
        fileConfig(conf_path)
        tee(cls.conf.get(EXPERIMENT_SECT, 'output-duplicate-path'))
        
        
    def setUp(self):
        pass
        
    
    def test_run(self):
        run(Test.conf)
        

if __name__ == "__main__":
    #import sys;sys.argv = []
    unittest.main()