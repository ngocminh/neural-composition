# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
from minh.experiment.experiment import run, defaults
import os
import shutil
import unittest
from logging.config import fileConfig


class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        conf_path = "test-data/config/dropout.cfg"
        cls.conf = ConfigParser(defaults)
        cls.conf.read(conf_path)
        fileConfig(conf_path)
        
        
    def setUp(self):
        shutil.rmtree("test-target")
        os.mkdir("test-target")
        
    
    def test_run(self):
        run(Test.conf)
        

if __name__ == "__main__":
    #import sys;sys.argv = []
    unittest.main()