# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
import unittest
from logging.config import fileConfig
from minh.experiment import sparsity_experiment, experiment


class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        conf_path = "test-data/config/sparsity.cfg"
        cls.conf = ConfigParser(experiment.defaults)
        cls.conf.read(conf_path)
        fileConfig(conf_path)
        
        
    def test_run(self):
        sparsity_experiment.run(Test.conf)
        

if __name__ == "__main__":
    #import sys;sys.argv = []
    unittest.main()