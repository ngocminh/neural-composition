'''
Created on May 14, 2014

@author: "Minh Le-Ngoc"
'''
import unittest
from composes.semantic_space.space import Space
from minh.experiment.experiment import prepare_for_composition
from logging.config import fileConfig


class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        conf_path = "test-data/config/full.cfg"
        fileConfig(conf_path)


    def test_prepare_for_composition(self):
        space = Space.build(format="dm",
                            data="test-data/space.has-zero.dm")
        space = prepare_for_composition(space, '')
        self.assertEqual((5, 5), space.cooccurrence_matrix.shape)
        self.assertEqual(5, len(space.id2row))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_prepare_for_composition']
    unittest.main()