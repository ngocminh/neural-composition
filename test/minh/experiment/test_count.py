# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
from minh.experiment.count import find_contexts_external, \
    _read_most_common_contexts, add_candidate_rows,\
    count_cooccurrences_external, count_cooccurrences_internal,\
    find_contexts_internal,\
    _head_phrases, _dependent_phrases, _merge_queue,\
    intersecting_phrases_unbalanced, log_phrase_statistics, END_OF_QUEUE
from minh.experiment.ioutils import read_stripped_lines
from minh.experiment.ukwac import lemma_and_pos
from minh.space.cooccurrence import domain_cooccurrences
import os
import shutil
import unittest
from logging.config import fileConfig
from StringIO import StringIO
import multiprocessing


class Test(unittest.TestCase):
    

    @classmethod
    def setUpClass(cls):
        cls.words = set(read_stripped_lines('test-data/corpus_noun.lst'))
        cls.contexts = set(['executive', 'tables', 'hospitality' , 'portsmouth', 
                            'old', 'takeover', 'league', 'club', 'football', 
                            'premier', 'fans', 'hilaire', 'pompey', 'boxes', 
                            'united', 'passion', 'team', 'leeds', 'cities', 
                            'trafford']);
                            
        cls.small_paths = ["test-data/split-small/ukwac000-00-00.xml.gz",
                           "test-data/split-small/ukwac000-00-01.xml.gz",
                           "test-data/split-small/ukwac000-00-02.xml.gz",
                           "test-data/split-small/ukwac000-00-03.xml.gz"]
        cls.small_contexts = set(read_stripped_lines('test-data/split-small/context.small.lst'));
                            
        cls.big_paths = ["test-data/split/ukwac000-00.xml.gz",
                         "test-data/split/ukwac000-01.xml.gz",
                         "test-data/split/ukwac000-02.xml.gz",
                         "test-data/split/ukwac000-03.xml.gz"]
        cls.big_contexts = set(read_stripped_lines('test-data/split/context.big.lst'));
        
        cls.target_phrases = {("single-j", "day-n", "single-j_day-n"), 
                              ("invisible-j", "people-n", "invisible-j_people-n"),
                              ("sweet-j", "past-n", "sweet-j_past-n")}
          
        conf_path = "test-data/config/full.cfg"
        fileConfig(conf_path)
        shutil.rmtree("test-target")
        os.mkdir("test-target")
        

    def setUp(self):
        pass
    
        
    def test_read_stripped_lines(self):
        lines = list(read_stripped_lines('test-data/corpus_noun.lst'))
        self.assertTrue(len(lines) > 0)
        for line in lines:
            self.assertFalse(line.endswith(' '))
            self.assertFalse(line.endswith('\t'))
            self.assertFalse(line.endswith('\n'))
        
        
    def test_read_most_common_contexts(self):
        contexts = set(_read_most_common_contexts("test-data/counts5", 4))
        self.assertTrue(("man", 1200) in contexts)
        self.assertTrue(("woman", 1200) in contexts)
        self.assertTrue(("car", 200) in contexts)
        self.assertTrue(("building", 100) in contexts)
        self.assertEqual(4, len(contexts))

    
    def test_find_contexts_external(self):
        contexts = find_contexts_external(domain_cooccurrences,
                                            ["test-data/ukwac.1000.xml"]*4, 
                                            Test.words, 10, 
                                           "test-target/context.merge.lst");
        print contexts
        self.assertEqual(10, len(contexts))
        self.assertTrue(os.path.exists("test-target/context.merge.lst"))


    def test_find_contexts_small_with_threashold(self):
        n = 1000
        threshold = 1
        
        contexts2 = find_contexts_external(domain_cooccurrences,
                                           Test.small_paths, None, n, 
                                           "test-target/context.threshold.lst",
                                           threshold);
        self.assertSetPrecision(contexts2, Test.small_contexts, 0.98)
    
        
    def test_find_contexts_exception_in_process(self):
        path = "test-data/ukwac.with-error.xml"
        # test that it terminates without error
        contexts = find_contexts_external(domain_cooccurrences,
                                          path, None, 10, 
                                          "test-target/context.exception.lst");
        # and we still can read well-formed data
        self.assertTrue(len(contexts) > 0, 
                        "No context found due to error in input data!")

    
    def assertSetPrecision(self, measuredSet, referenceSet, expectedPrecision):
        wrongItems = []
        count = 0
        for item in measuredSet:
            if item in referenceSet:
                count += 1
            else:
                wrongItems.append(item)
        precision = count / float(len(measuredSet))
        print "precision: %.2f%%" %(precision*100)
        # Don't print out wrong items!
        # Invalid UTF-8 character in the data will cause the test to hang
        self.assertGreaterEqual(precision, expectedPrecision, 
                                "Precision is %f, lower than expected %f."
                                %(precision, expectedPrecision));


    def test_merge_queue_one_path(self):
        queue = multiprocessing.Queue(10)
        queue.put("test-data/merge/1.txt")
        queue.put(END_OF_QUEUE)
        result_path = _merge_queue(queue, "test-target/merge-one-path.txt", 
                                   cleaning_up=False)
        self.assertEqual(['a\t2','d\t2','f\t1'], 
                         list(read_stripped_lines(result_path)))
    

    def test_merge_queue_one_process(self):
        queue = multiprocessing.Queue(10)
        for i in range(1,5):
            queue.put("test-data/merge/%d.txt" %i)
        queue.put(END_OF_QUEUE)
        result_path = _merge_queue(queue, "test-target/merge-one-process.txt", 
                                   cleaning_up=False)
        self.assertEqual(['a\t5','b\t3','c\t2','d\t2','e\t5','f\t11','g\t9'], 
                         list(read_stripped_lines(result_path)))
    
    
    def test_merge_queue_many_processes(self):
        queue = multiprocessing.Queue(10)
        def get_path(i):
            import time; time.sleep(0.01)
            return "test-data/merge/%d.txt" %i
        pool = multiprocessing.Pool(4)
        for i in range(1,5):
            pool.apply_async(get_path, args=(i,), callback=queue.put)
        pool.close()
        merge_process = multiprocessing.Process(target=_merge_queue, args=(queue, 
                                        "test-target/merge-one-process.txt", 
                                        False))
        merge_process.start()
        pool.join()
        queue.put(END_OF_QUEUE)
        merge_process.join()
        result_path = queue.get_nowait()
        self.assertEqual(['a\t5','b\t3','c\t2','d\t2','e\t5','f\t11','g\t9'], 
                         list(read_stripped_lines(result_path)))
    
    
    def test_count_cooccurrences_external(self):
        count_cooccurrences_external(domain_cooccurrences, 
                                    Test.small_paths, None, Test.small_contexts, 
                                    'test-target/cooccurrences-small.external.lst',
                                    lemma_and_pos)


    def test_count_cooccurrences_internal(self):
        count_cooccurrences_internal(domain_cooccurrences, Test.small_paths, 
                                      None, Test.small_contexts, 
                                      'test-target/cooccurrences-small.merge.lst',
                                      lemma_and_pos)


    def test_count_cooccurrences_internal_exception_in_process(self):
        path = "test-data/ukwac.with-error.xml"
        result = "test-target/count.exception.sm"
        count_cooccurrences_internal(domain_cooccurrences, path, None, 
                                      Test.big_contexts, result, lemma_and_pos, 1)
        with open(result) as f:
            self.assertTrue(len(f.readlines()) > 0, 
                            "No co-occurrence found due to error in input data!")        


    def test_count_cooccurrences_internal_exception_in_process2(self):
        paths = ["no-such-file", "test-data/ukwac.with-error.xml"]
        result = "test-target/count.exception.sm"
        count_cooccurrences_internal(domain_cooccurrences, paths, None, 
                                      Test.big_contexts, result, lemma_and_pos, 1)
        with open(result) as f:
            self.assertTrue(len(f.readlines()) > 0, 
                            "No co-occurrence found due to error in input data!")        


    def test_add_candidate_rows_no_output(self):
        words = set()
        add_candidate_rows(words, "test-data/ukwac.100k.xml.gz", lemma_and_pos, 1000, 2, 10, None)
        self.assertGreater(len(words), 0)


    def test_add_candidate_rows_with_output(self):
        words = set()
        add_candidate_rows(words, "test-data/ukwac.100k.xml.gz", lemma_and_pos, 1000, 2, 10, 
                           "test-target/candiate-row.ukwac-100k.txt")
        self.assertGreater(len(words), 0)

        
    def big_test_find_contexts_external(self):
        '''
        Test finding contexts on big data (nearer to realistic data) in the 
        hope to find some problems. The name has been changed to avoid 
        automatic run of this method.
        '''
        paths = ["test-data/split/ukwac000-00.xml.gz",
                 "test-data/split/ukwac000-01.xml.gz",
                 "test-data/split/ukwac000-02.xml.gz",
                 "test-data/split/ukwac000-03.xml.gz"]
        n = 1000
        
        contexts1 = set(read_stripped_lines('test-data/split/context.big.lst'));
        self.assertEqual(n, len(contexts1)) 
                                                       
        contexts2 = find_contexts_external(domain_cooccurrences,
                                             paths, None, n, None);
        self.assertSetEqual(contexts1, contexts2)
        
        
    def big_test_find_contexts_no_threashold(self):
        '''
        Test finding contexts on big data (nearer to realistic data) in the 
        hope to find some problems. The name has been changed to avoid 
        automatic run of this method.
        '''
        paths = ["test-data/split/ukwac000-00.xml.gz",
                 "test-data/split/ukwac000-01.xml.gz",
                 "test-data/split/ukwac000-02.xml.gz",
                 "test-data/split/ukwac000-03.xml.gz"]
        n = 1000
        
        contexts1 = set(read_stripped_lines('test-data/split/context.big.lst'));
        self.assertEqual(n, len(contexts1)) 
                                                       
        contexts2 = find_contexts_internal(domain_cooccurrences,
                                             paths, None, n, None);
        self.assertSetEqual(contexts1, contexts2)
        
        
    def big_test_find_contexts_with_threashold(self):
        '''
        Test finding contexts on big data (nearer to realistic data) in the 
        hope to find some problems. The name has been changed to avoid 
        automatic run of this method.
        '''
        n = 1000
        threshold = 1
        
        contexts2 = find_contexts_internal(domain_cooccurrences,
                                             Test.big_paths, None, n, 
                                             'test-target/big-context-threshold.txt', 
                                             threshold);
        self.assertSetPrecision(contexts2, Test.big_contexts, 0.99)


    def test_head_phrases_empty(self):
        phrases = _head_phrases(StringIO(""), set((("use-v", "n_v"),)), 3)
        self.assertEqual(0, len(phrases))


    def test_head_phrases_empty2(self):
        data = ("knowledge-n\tuse-v\t1\n"
                "knowledge-n\tuseful-j\t1\n"
                "tool-n\tuse-v\t1\n"
                "effectively-a\tuse-v\t1\n"  
                "influence-n\texercise-v\t2\n")
        phrases = _head_phrases(StringIO(data), set(), 3)
        self.assertEqual(0, len(phrases))


    def test_head_phrases(self):
        data = ("knowledge-n\tuse-v\t1\n"
                "knowledge-n\tuseful-j\t1\n"
                "tool-n\tuse-v\t1\n"
                "effectively-a\tuse-v\t1\n"  
                "influence-n\texercise-v\t2\n")
        phrases = _head_phrases(StringIO(data), set((("use-v", "n_v"),)), 3)
        self.assertEqual([("knowledge-n", "use-v"), 
                          ("tool-n", "use-v")], phrases)


    def test_dependent_phrases_empty(self):
        phrases = _dependent_phrases(StringIO(""), set((("use-v", "n_v"),)), 3)
        self.assertEqual(0, len(phrases))


    def test_dependent_phrases_empty2(self):
        data = ("knowledge-n\tuse-v\t1\n"
                "knowledge-n\tuseful-j\t1\n"  
                "influence-n\texercise-v\t2\n")
        phrases = _dependent_phrases(StringIO(data), set(), 3)
        self.assertEqual(0, len(phrases))


    def test_dependent_phrases(self):
        data = ("knowledge-n\tuse-v\t1\n"
                "knowledge-n\tuseful-j\t1\n"  
                "influence-n\texercise-v\t2\n")
        phrases = _dependent_phrases(StringIO(data), set((("knowledge-n", "n_v"),)), 3)
        self.assertEqual([("knowledge-n", "use-v")], phrases)


    def test_intersecting_phrases_unbalanced(self):
        phrases = intersecting_phrases_unbalanced(Test.small_paths, Test.words,
                                                  Test.target_phrases, 100, 
                                                  "test-target/phrases.txt")
        log_phrase_statistics(phrases, Test.target_phrases)
        self.assertTrue(len(phrases) > 0)

if __name__ == "__main__":
    import sys;sys.argv = ['', 'Test.big_test_find_contexts_with_threashold']
    unittest.main()
