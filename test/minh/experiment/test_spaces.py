# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
import unittest
from collections import Counter
from minh.experiment.ukwac import DependencyTreeNode
from minh.space.cooccurrence import extract_domain_columns,\
    extract_function_columns, function_cooccurrences, domain_cooccurrences
from minh.space import cooccurrence


class Test(unittest.TestCase):


    def nodes(self, sentence):
        nodes = []
        words = sentence.split(" ")
        for i in range(len(words)):
            trunks = words[i].split("/")
            word = trunks[0]
            pos = trunks[1]
            nodes.append(DependencyTreeNode(word, pos, word, i))
        return nodes


    def assertDomainPattern(self, sentence, index, expected_patterns):
        nodes = self.nodes(sentence)
        patterns = extract_domain_columns(nodes, index)
        self.assertSetEqual(set(expected_patterns), set(patterns))
        
        
    def test_extract_domain_columns(self):
        sentence1 = ('would/MD visit/VB Big/NNP Lake/NNP and/CC take/VB '
                    'our/PRP$ boat/NN on/IN this/DT huge/JJ beautiful/JJ '
                    'lake/NN ./. There/EX was/VBD')
        patterns1 = ['lake']
        self.assertDomainPattern(sentence1, 7, patterns1)

        sentence2 = ('the/DT large/JJ paved/JJ parking/NN lot/NN in/IN the/DT '
                     'boat/NN ramp/NN area/NN and/CC walk/VB south/RB '
                     'along/IN the/DT')
        patterns2 = ['lot', 'ramp']
        self.assertDomainPattern(sentence2, 7, patterns2)

        sentence3 = ('building/VBG permit/NN ./. \’/” Anyway/RB ,/, we/PRP '
                     'should/MD have/VB a/DT boat/NN next/JJ summer/NN with/IN '
                     'skiing/NN and/CC tubing/NN paraphernalia/NNS ./.')
        patterns3 = ['permit', 'summer']
        self.assertDomainPattern(sentence3, 10, patterns3)


    def assertFunctionPattern(self, sentence, index, expected_patterns):
        expected_pattern_set = set()
        for pattern in expected_patterns:
            expected_pattern_set.add(pattern.replace(" ", "_"))
        nodes = self.nodes(sentence)
        patterns = extract_function_columns(nodes, index)
        self.assertSetEqual(expected_pattern_set, set(patterns))


    def test_extract_function_columns(self):
        sentence1 = 'the/DT canals/NNS by/IN boat/NN and/CC wandering/VBG the/DT'
        patterns1 = ('X C wandering', 'by X C wandering')
        self.assertFunctionPattern(sentence1, 3, patterns1)
        
        sentence2 = 'a/DT charter/NN fishing/VBG boat/NN captain/NN named/VBN Jim/NNP'
        patterns2 = ('fishing X N named', 'fishing X', 'X N named')
        self.assertFunctionPattern(sentence2, 3, patterns2)
        
        sentence3 = 'used/VBN from/IN a/DT boat/NN and/CC lowered/VBD to/TO'
        patterns3 = ('used I D X C lowered', 
                         'used I D X', 'X C lowered',
                         'used from D X C lowered to',
                         'used from D X', 'X C lowered to')
        self.assertFunctionPattern(sentence3, 3, patterns3)


    def test_function_cooccurrences(self):
        counter = Counter()
        def increase_counter(c): counter[c] += 1
        function_cooccurrences("test-data/ukwac.1000.xml", increase_counter)
        self.assertTrue(len(counter) > 0)


    def test_domain_cooccurrences_small(self):
        cooccurrences = []
        domain_cooccurrences("test-data/ukwac.1.xml", 
                           cooccurrences.append, 
                           lambda row: row.endswith("n"))
        print cooccurrences
        self.assertEqual(4, len(cooccurrences))


    def test_domain_cooccurrences_big(self):
        domain_cooccurrences("test-data/ukwac.1000.xml", lambda x: True)
        
        
    def test_write(self):
        empty_path = "target/test-empty.sm"
        cooccurrence.write({}, empty_path)
        self.assertEqual(0, len(open(empty_path, "r").readlines()))
        self.assertRaises(NotImplementedError, cooccurrence.write, 
                          {}, "target/test-error.txt", "txt");
        self.assertRaises(NotImplementedError, cooccurrence.write, 
                          {}, "target/test-error.txt");
        
        
    def test_write_sparse(self):
        cooccurrences = { ('test', 'method'):3, ('test', 'class'):2  }
        path = "target/test.sm"
        cooccurrence.write_sparse(cooccurrences, path)
        self.assertEqual(2, len(open(path, "r").readlines()))
        

if __name__ == "__main__":
    unittest.main()