# -*- coding: utf-8 -*-
'''
Created on Feb 1, 2014

@author: Lê Ngọc Minh
'''
from scipy.sparse.base import issparse
from composes.matrix.matrix import Matrix
import numpy
from logging import getLogger


_log = getLogger(__name__)


def as_dense_numpy_matrix(mat):
    if isinstance(mat, Matrix):
        mat = mat.mat 
    if issparse(mat):
        mat = mat.todense()
    return mat


def _rows_str(mat):
    mat = numpy.array(mat).reshape((-1,))
    l = numpy.where(mat)[0].tolist()
    return ",".join([str(i) for i in l])


def check_matrix(mat, name):
    ok = True
    zero_rows = (abs(mat).sum(axis=1) == 0)
    if numpy.any(zero_rows):
        ok = False
        _log.warn("%d zero row(s) detected in %s: %s" %(
                        zero_rows.sum(), name, _rows_str(zero_rows)))
    nan_rows = numpy.isnan(mat.sum(axis=1))
    if numpy.any(nan_rows):
        ok = False
        _log.warn("%d NaN rows detected in %s: %s" %(
                        nan_rows.sum(), name, _rows_str(nan_rows)))
    return ok
