# -*- coding: utf-8 -*-
'''
Created on Nov 12, 2013

@author: Lê Ngọc Minh
'''
from composes.utils import io_utils
from composes.similarity.cos import CosSimilarity
from optparse import OptionParser
import fileinput
import logging
import sys
import numpy
import os
import pickle
from composes.transformation.scaling.row_normalization import RowNormalization
from minh.miscutils import as_dense_numpy_matrix


__log = logging.getLogger('examine')


def find_representatives(space, matrix=None, num_repr=15):
    __log.info("Computing representatives...")
    if matrix == None:
        matrix = as_dense_numpy_matrix(space.cooccurrence_matrix.mat)
    (num_row, num_col) = matrix.shape
    sorted_indices = numpy.argsort(matrix, 0)
    __log.info("Finished sorting.")
    representatives = {}
    for col in range(num_col):
        reps = []
        for i in range(max(num_row-num_repr, 0), num_row):
            row = sorted_indices[i, col]
            item = (space.id2row[row], matrix[row, col])
            reps.append(item)
        representatives[col] = reps
    __log.info("Finished finding representatives.")
    return representatives
    

def print_dim_representatives(dims, f=sys.stdout):
    for (index, dim) in enumerate(dims):
        if index > 0: f.write(", ")
        f.write("%s=%.2f" %(dim[0], dim[1]))
        

def print_dimensions(space, representatives, word):
    row = space.get_row(word)
    indices = sorted([col for col in range(row.shape[1])], 
                     key=lambda col: row[0,col], reverse=True)[:5]
    for index in indices:
        weight = row[0,index]
        reps = representatives[index]
        sys.stdout.write('Dimension %d (%f): ' %(index, weight))
        print_dim_representatives(reps)
        print
    

def print_neighbors(neighbors):
    sys.stdout.write('Neighbors: ') 
    for i, n in enumerate(neighbors):
        if i > 0: sys.stdout.write(', ')
        sys.stdout.write('%s (%.3f)' %(n[0], n[1]))
    print


def print_representatives(representatives):
    print '=== Representatives ==='
    for i in representatives:
        sys.stdout.write("Dimension %d: " %i) 
        print_dim_representatives(representatives[i])
        print


def print_statistics(space, matrix=None, words_with_least_dimensions=500):
    __log.info("Computing statistics...")
    if matrix == None:
        matrix = as_dense_numpy_matrix(space.cooccurrence_matrix.mat)
    eps = 1E-8
    matrix_abs = numpy.abs(matrix)
    nonzeros = matrix_abs >= eps
    nonzero_count_cols = numpy.squeeze(numpy.array(nonzeros.sum(axis=0)))
    nonzero_count_rows = numpy.squeeze(numpy.array(nonzeros.sum(axis=1)))
    nonzero_count_matrix = numpy.sum(nonzero_count_rows)
    (num_row, num_col) = matrix.shape
    average_activity_ratio = float(nonzero_count_matrix)/(num_row*num_col)
    print "Non-zero cells: %d" %nonzero_count_matrix
    print "Dimensions: %dx%d" %(num_row, num_col)
    print "Average activity ratio: %f%%" %(average_activity_ratio*100)
    l1 = matrix_abs.sum(axis=1).mean()
    print "L1 : %.2f" %l1
    dpw = nonzero_count_rows.mean()
    print "avg(#dimensions per word): %.2f" %dpw
    if nonzero_count_rows.ndim == 1:
        print "std(#dimensions per word): %.2f" %nonzero_count_rows.std()
    else:
        print "std(#dimensions per word): 0"
    wpd = nonzero_count_cols.mean()  
    print "avg(#words per dimension): %.2f" %wpd
    print "std(#words per dimension): %.2f" %nonzero_count_cols.std()
    if nonzero_count_rows.ndim == 1:
        print "Distribution of #dimensions per word:" 
        print numpy.bincount(nonzero_count_rows)
    print "Distribution of (#words per dimension)/100 (first 1000 counts):" 
    print numpy.bincount(nonzero_count_cols/100)[:1000]
    if words_with_least_dimensions > 0 and nonzero_count_rows.ndim == 1:
        sys.stdout.write("First %d words with least number of dimensions: " %words_with_least_dimensions)
        sorted_rows = numpy.argsort(nonzero_count_rows)
        for i in range(min(words_with_least_dimensions, num_row)):
            if i > 0: sys.stdout.write(", ")
            sys.stdout.write(space.id2row[sorted_rows[i]])
        print
    return (average_activity_ratio, l1, dpw, wpd)
    

def examine_all(space, words, space_path=None):
    matrix = as_dense_numpy_matrix(space.cooccurrence_matrix.mat)
    print_statistics(space, matrix)
    if len(words) > 0:
        if space_path:
            dim_path = space_path + '.dim' 
            if os.path.exists(dim_path):
                with open(dim_path) as f:
                    representatives = pickle.load(f)
            else:
                representatives = find_representatives(space, matrix)
                with open(dim_path,'w') as f:
                    pickle.dump(representatives, f, 0)
        else:
            representatives = find_representatives(space, matrix)
    #    print_representatives(representatives)
        for word in words:
            examine(space, word, representatives)
        

def print_weights(weights):
    sys.stdout.write('Weights: ')
    for col in range(weights.shape[1]):
        if col > 0: sys.stdout.write('\t')
        sys.stdout.write("%5.2f" %weights[0, col])
        if (col+1) % 15 == 0: print
    print
    

def examine(space, word, representatives):
    try:
        word = word.strip()
        print '=== %s ===' %word
#        print_weights(space.get_row(word)) 
        neighbours = space.get_neighbours(word, 10, CosSimilarity())
        print_neighbors(neighbours)
        print_dimensions(space, representatives, word)
    except KeyError:
        print "Word not found: %s" %word


if __name__ == '__main__':
#    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
#    logFormatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    consoleHandler = logging.StreamHandler()
#    consoleHandler.setFormatter(logFormatter)
    __log.addHandler(consoleHandler)
    __log.setLevel(logging.DEBUG)
    
    parser = OptionParser(usage="usage: %prog options [word-listing-files]")
    parser.add_option("-s", "--space", metavar="FILE",
                      help="read the examined space from FILE")
    parser.add_option("-n", "--normalize", action="store_true", dest="normalize")
    (options, args) = parser.parse_args()
    __log.info("Read from space %s" %options.space)
    __log.info("Word lists: %s" %str(args))
    __log.info("Reading space...")
    space = io_utils.load(options.space)
    __log.info("Done.")
    if options.normalize:
        space = space.apply(RowNormalization())
    examine_all(space, fileinput.input(args), options.space)