# -*- coding: utf-8 -*-
'''
Created on Mar 11, 2014

@author: Lê Ngọc Minh
'''
from minh.evaluate.evaluate import Evaluation
from scipy.stats.stats import spearmanr
import numpy
from collections import OrderedDict


class VerbDisambiguatingQuestion:
    
    
    def __init__(self, participant, verb, noun, landmark, inp, hilo):
        self.participant = participant
        self.verb = verb
        self.noun = noun
        self.landmark = landmark
        self.inp = inp
        self.hilo = hilo
        
        
    def noun_(self): return self.noun + "-n"
    def verb_(self): return self.verb + "-v"
    def landmark_(self): return self.landmark + "-v"
    def verb_phrase(self): return self.noun_() + "_" + self.verb_()
    def landmark_phrase(self): return self.noun_() + "_" + self.landmark_()


    @classmethod
    def read(cls, path):
        questions = []
        with open(path) as f:
            f.next() # skip the header
            for line in f:
                line = line.strip()
                fields = line.split(' ')
                assert fields[5] in {'high', 'low'}
                q = VerbDisambiguatingQuestion(fields[0], fields[1], fields[2], 
                                               fields[3], int(fields[4]), 
                                               fields[5]=='high')
                questions.append(q)
        return questions
    

class VerbDisambiguating(Evaluation):
    """
    Represent the dataset described in (people say so, I couldn't be patient 
    enough to locate it): Mitchell, J., & Lapata, M. (2008). Vector-based Models 
    of Semantic Composition. In ACL (pp. 236–244).
    
    Each entry of the dataset provides a noun, a target verb and landmark verb 
    (both intransitive). The noun must be composed with both verbs to produce 
    phrase vectors of which the similarity is measured.
    """

    name = "verb-disambiguating" 
    
    def __init__(self, path, sim):
        self.sim = sim
        self.questions = VerbDisambiguatingQuestion.read(path)
                
    
    def get_target_words(self):
        words = set()
        for q in self.questions:
            words.add(q.verb_())
            words.add(q.noun_())
            words.add(q.landmark_())
        return words

    
    def get_target_phrases(self):
        targets = set()
        for q in self.questions:
            targets.add((q.noun_(), q.verb_(), q.verb_phrase()))
            targets.add((q.noun_(), q.landmark_(), q.landmark_phrase()))
        return targets
    
        
    def evaluate(self, stacked_space):
        gold_standard = [q.inp for q in self.questions]
        sims = [stacked_space.get_sim(q.verb_phrase(), q.landmark_phrase(), 
                                      self.sim) for q in self.questions]
        rho = spearmanr(sims, gold_standard)[0]
        high_sim = numpy.mean([sim for index, sim in enumerate(sims) 
                               if self.questions[index].hilo])
        low_sim = numpy.mean([sim for index, sim in enumerate(sims) 
                              if not self.questions[index].hilo])
        return OrderedDict([('rho', rho), ('high_sim', high_sim), ('low_sim', low_sim)])
        
        
class ShortPhraseSimilarityQuestion:
    
    
    def __init__(self, participant, type_, ver, s1lemma1, s1lemma2, s2lemma1, 
                 s2lemma2, rating):
        '''
        
        :param participant:
        :param type_:
        :param ver:
        :param s1lemma1:
        :param s1lemma2:
        :param s2lemma1:
        :param s2lemma2:
        :param rating:
        '''
        self.participant = participant 
        self.type_ = type_ 
        self.ver = ver
        self.s1lemma1 = s1lemma1 
        self.s1lemma2 = s1lemma2
        self.s2lemma1 = s2lemma1
        self.s2lemma2 = s2lemma2
        self.rating = rating
        
    
    def _pos1(self):
        if self.type_ == 'verbobjects':
            return '-n'
        elif self.type_ == 'adjectivenouns':
            return '-j'
        elif self.type_ == 'compoundnouns':
            return '-n'
        else:
            raise ValueError('Strange type: %s' %type)
        
    
    def _pos2(self):
        if self.type_ == 'verbobjects':
            return '-v'
        elif self.type_ == 'adjectivenouns':
            return '-n'
        elif self.type_ == 'compoundnouns':
            return '-n'
        else:
            raise ValueError('Strange type: %s' %type)
        

    def s1w1(self): return self.s1lemma1 + self._pos1()
    def s1w2(self): return self.s1lemma2 + self._pos2()
    def s2w1(self): return self.s2lemma1 + self._pos1()
    def s2w2(self): return self.s2lemma2 + self._pos2()

    def phrase1(self): return self.s1w1() + "_" + self.s1w2()
    def phrase2(self): return self.s2w1() + "_" + self.s2w2()


    @classmethod
    def read(cls, path):
        questions = []
        with open(path) as f:
            f.next() # skip the header
            for line in f:
                line = line.strip()
                fields = line.strip().split(' ')
                type_ = fields[1]
                assert type_ in ('verbobjects', 'adjectivenouns', 'compoundnouns')
                q = ShortPhraseSimilarityQuestion(fields[0], type_, fields[2], 
                                                  fields[3], fields[4], # 1st phrase 
                                                  fields[5], fields[6], # 2nd phrase
                                                  int(fields[7]))
                questions.append(q)
        return questions
    

class ShortPhraseSimilarity(Evaluation):
    """
    Represent the evaluation scheme described in Mitchell, J., & Lapata, M. (2010). 
    Composition in Distributional Models of Semantics. Cognitive Science, 34(8), 
    1388–1429. doi:10.1111/j.1551-6709.2010.01106.x
    
    Each entry of the dataset provides two short phrases with similarity 
    judgment from some subjects. Evaluated models are asked to provide a 
    similarity score for each pair of phrases. 
    
    Following GEMS 2011, the reported result is the Spearman rank correlation of 
    those scores to the rating of all participants. "For example, when the model 
    predicts a value of 1.5 for one data point, and three of Mitchell and 
    Lapata's participants assigned the scores 1, 2 and 2, respectively, three 
    pairs will enter the final correlation analysis: (1.5,1), (1.5,2) and 
    (1.5,2)."
    """

    name = "short-phrase-similarity" 
    
    def __init__(self, path, sim):
        self.sim = sim
        self.questions = ShortPhraseSimilarityQuestion.read(path)
                
    
    def get_target_words(self):
        words = set()
        for q in self.questions:
            words.add(q.s1w1())
            words.add(q.s1w2())
            words.add(q.s2w1())
            words.add(q.s2w2())
        return words

    
    def get_target_phrases(self):
        targets = set()
        for q in self.questions:
            targets.add((q.s1w1(), q.s1w2(), q.phrase1()))
            targets.add((q.s2w1(), q.s2w2(), q.phrase2()))
        return targets
    
        
    def evaluate(self, stacked_space):
        sims_jn = [stacked_space.get_sim(q.phrase1(), q.phrase2(), self.sim) 
                   for q in self.questions if q.type_ == 'adjectivenouns']
        golds_jn = [q.rating for q in self.questions if q.type_ == 'adjectivenouns']
        rho_jn = spearmanr(sims_jn, golds_jn)[0]
        
        sims_vn = [stacked_space.get_sim(q.phrase1(), q.phrase2(), self.sim) 
                   for q in self.questions if q.type_ == 'verbobjects']
        golds_vn = [q.rating for q in self.questions if q.type_ == 'verbobjects']
        rho_vn = spearmanr(sims_vn, golds_vn)[0]
        
        sims_nn = [stacked_space.get_sim(q.phrase1(), q.phrase2(), self.sim) 
                   for q in self.questions if q.type_ == 'compoundnouns']
        golds_nn = [q.rating for q in self.questions if q.type_ == 'compoundnouns']
        rho_nn = spearmanr(sims_nn, golds_nn)[0]
        
        golds = [q.rating for q in self.questions]
        sims = [stacked_space.get_sim(q.phrase1(), q.phrase2(), self.sim) 
                for q in self.questions]
        rho = spearmanr(sims, golds)[0]
        
        return OrderedDict([('rho', rho), ('rho-adj-noun', rho_jn),
                            ('rho-verb-object', rho_vn), 
                            ('rho-noun-noun', rho_nn)])
        
