# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2014

@author: Lê Ngọc Minh
'''
import logging
from minh.evaluate.examine import examine, find_representatives,\
    print_statistics
import sys
from composes.semantic_space.space import Space
from collections import OrderedDict


_log = logging.getLogger(__name__)


class Evaluation(object):
    """
    Encapsulate the evaluation of a space (created from a composition model).
    This class instruct how to create an appropriate space and then take a space
    for evaluation. 
    """
    
    name = "no-name"
    
    
    def get_target_words(self):
        """
        Return a set of words needed for this evaluation. 
        """
        raise NotImplemented
        
    
    def get_target_phrases(self):
        """
        Return a set of target phrases needed for this evaluation.
        
        Each element of the set is a tuple: (dependent word, head word, phrase).
        """
        raise NotImplemented
        
    
    def evaluate(self, stacked_space):
        """
        Evaluate the specified space. The space should include both word and
        phrase vectors.
        """
        raise NotImplemented


def get_target_words(evaluations):
    """
    Get words needed for all provided evaluations.
    @see:    Evaluation#get_target_words()
    """
    if isinstance(evaluations, Evaluation):
        return evaluations.get_target_words()
    words = set()
    for evaluation in evaluations:
        words.update(evaluation.get_target_words())
    return words


def get_target_phrases(evaluations):
    """
    Get targets needed for all provided evaluations.
    @see:    Evaluation#get_target_phrases()
    """
    if isinstance(evaluations, Evaluation):
        return evaluations.get_target_phrases()
    targets = set()
    for evaluation in evaluations:
        targets.update(evaluation.get_target_phrases())
    return targets


_base_space = None
_representatives = []
_examined_words = ['double-j_star-n', 'premature-j_baby-n', 'monosyllabic-j_word-n', 
                  'crowned-j_head-n', 'presidential-j_term-n', 'electrical-j_power-n',
                  'vitreous-j_silica-n', 'twin-j_bill-n', 'abstract-j_artist-n',
                  'dead-j_hand-n']


def _prepend_evaluation_name_to_results(name, results):
    return OrderedDict([("%s[%s]" %(name, key), results[key]) for key in results])


def evaluate_init(space):
    global _representatives, _base_space
    _base_space = space
    _representatives = find_representatives(_base_space)


def evaluate_space(phrase_space, model_name, evaluations):
    stacked_space = Space.vstack(phrase_space, _base_space)
    sparsity, l1, dpw, wpd = print_statistics(phrase_space, words_with_least_dimensions=100)
    row = OrderedDict([('model', model_name), ('activity_ratio', sparsity), 
                      ('l1', l1), ('dpw', dpw), ('wpd', wpd)])
    for evaluation in evaluations:
        row.update(_prepend_evaluation_name_to_results(
                   evaluation.name, evaluation.evaluate(stacked_space)))
    for word in _examined_words:
        examine(phrase_space, word, _representatives)
    return row
    

def print_summary(data, out=sys.stdout):
    if len(data) <= 0:
        return
    keys = list(data[0].keys())
    out.write('\t'.join(keys) + '\n')
    for row in data:
        assert keys == row.keys(), "All rows must have the same list of fields."
        _print_row(row, out)
        
    
def _print_row(row, out=sys.stdout):
    out.write('\t'.join([str(row[k]) for k in row]) + '\n')
