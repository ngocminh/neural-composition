# -*- coding: utf-8 -*-
'''
Created on Feb 13, 2013

@author: Lê Ngọc Minh
'''
import logging
from composes.similarity.cos import CosSimilarity
from minh.miscutils import as_dense_numpy_matrix
import numpy
from minh.evaluate.evaluate import Evaluation
from collections import OrderedDict
import csv
from StringIO import StringIO


_log = logging.getLogger(__name__)


class Question:
    
    def __init__(self, modifier, head, correct_answer, foils):
        self.modifier = modifier
        self.head = head
        self.correct_answer = correct_answer
        self.foils = foils
        
        
    @property
    def phrase(self):
        return self.modifier + "_" + self.head
    
    
    @property
    def choices(self):
        choices = [self.modifier, self.head, self.correct_answer]
        choices.extend(self.foils)
        return choices
    
    
    def __str__(self):
        return "(%s %s: %s)" % (self.modifier, self.head, self.correct_answer)


def question(s):
    chunks = s.split("\t")
    for index, chunk in enumerate(chunks):
        chunks[index] = chunk.strip()
    if len(chunks) < 3:
        raise ValueError("At least 3 fields are required.")
    question = Question(chunks[0], chunks[1], chunks[2], chunks[3:])
    return question


def from_file(paths, supress_error=False):
    if isinstance(paths, basestring):
        paths = (paths, )
    questions = []
    for path in paths:
        with open(path) as f:
            for line in f:
                try:
                    questions.append(question(line))
                except ValueError as e:
                    if supress_error:
                        _log.error("Invalid question: " + line, exc_info=1)
                    else:
                        raise e
    return questions


def answer(stacked_space, question, constrained, measure=CosSimilarity()):
    # compute similarity only for choices with a != c and b != c
    if constrained:
        choices = list([choice for choice in question.choices 
                        if choice != question.head and choice != question.modifier])
    else:
        choices = question.choices
    sims = [stacked_space.get_sim(question.phrase, choice, measure)
            for choice in choices]
    index = max(range(len(sims)), key=lambda i: sims[i])
    ans = choices[index]
    if sims[index] <= 0:
        index = -1
        ans = None
    return index, ans


def answer_and_score(stacked_space, questions, constrained, 
                     measure=CosSimilarity(), recovery_mode='no'):
    """
    Answer each q by the choice that is the most similar to composed 
    phrase vector. If all choices are equal and force_answer=False, no answer 
    is given. Return a tuple of precision and answered rate.
    
    See section 4.2, "Turney, P. D. (2012). Domain and Function : A Dual-Space 
    Model of Semantic Relations and Compositions. Journal of Artificial 
    Intelligence Research, 44, 533–585.
    """
    correct = 0
    answered = 0
    incorrect = [('Modifier', 'Head', 'Answer', 'AnswerIndex', 'Correct', 'Foil1', 'Foil2', 'Foil3', 'Foil4')]
    for q in questions:
        index, ans = answer(stacked_space, q, constrained, measure)
        if ans is None:
            incorrect.append([q.modifier, q.head, "<N/A>", "<N/A>", 
                              q.correct_answer] + list(q.foils))
            if recovery_mode == 'fraction':
                correct += 1.0/(len(q.choices) - (2 if constrained else 0))
                answered += 1
        else:
            answered += 1
            if ans == q.correct_answer:
                correct += 1
            else:
                incorrect.append([q.modifier, q.head, ans, index, 
                                  q.correct_answer] + list(q.foils))
    if len(incorrect) > 0:
        s = StringIO()
        csv.writer(s, dialect=csv.excel_tab).writerows(incorrect)
        _log.info("Incorrect answers or no-answer:\n" + s.getvalue())
    if answered > 0:
        precision = correct/float(answered)
    else:
        precision = 0
    answered_rate = answered/float(len(questions))
    return (precision, answered_rate)


def rank(stacked_space, questions, measure=CosSimilarity()):
    """
    Measure the quality of a space by the rank of correct answers when sort all
    words in the specified space by descending similarity to phrase vector.
    
    See Dinu, N., & Baroni, M. (2013). General estimation and evaluation of 
    compositional distributional semantic models. ACL Workshop on Continuous 
    Vector Space Models.
    """
    _log.info("Ranking...")
    omitted = []
    ranks = []
    for question in questions:
        if (question.phrase in stacked_space.row2id and
                question.correct_answer in stacked_space.row2id):
            vector = stacked_space.get_row(question.phrase)
            correct_id = stacked_space.row2id[question.correct_answer]
            sim_matrix = as_dense_numpy_matrix(measure.get_sims_to_matrix(vector, 
                                        stacked_space.cooccurrence_matrix))
            smaller = sim_matrix < sim_matrix[correct_id,0]
            ranks.append(smaller.sum().sum()+1)
        else:
            omitted.append(question)
    ranks = numpy.array(ranks)
    _log.info("Ranking finished.")
    return (ranks.mean(), ranks.std(), omitted)


class TurneyBase(Evaluation):
    
    def __init__(self, questions):
        if isinstance(questions, basestring):
            questions = from_file(questions)
        self.questions = questions
    
    
    def get_target_words(self):
        words = set()
        for question in self.questions:
            words.add(question.modifier)
            words.add(question.head)
            for foil in question.foils:
                words.add(foil)
        return words

    
    def get_target_phrases(self):
        return set([(question.modifier, question.head, question.phrase)
                for question in self.questions])


class TurneyRanking(TurneyBase):
    
    name = "turney-ranking" 
    
    def __init__(self, questions):
        super(TurneyRanking, self).__init__(questions)
    

    def evaluate(self, stacked_space):    
        mean, std, omitted = rank(stacked_space, self.questions)
        if len(omitted) > 0:
            _log.warn("Omitted %d questions.", len(omitted))
            _log.warn("First 20 omitted question: %s", str(omitted))
        # Don't init it with a map just because it is shorter
        # C'mon! How could you expect a map to be ordered while initializing it 
        # with an *unordered* map?
        return OrderedDict([('mean', mean), ('std', std), ('omitted', len(omitted))])
        

class Turney(TurneyBase):

    name = "turney" 
    
    def __init__(self, questions, constrained, recovery_mode):
        super(Turney, self).__init__(questions)
        self.constrained = constrained
        self.recovery_mode = recovery_mode
            
            
    def evaluate(self, stacked_space):    
        score = answer_and_score(stacked_space, self.questions, self.constrained, 
                                 recovery_mode=self.recovery_mode)
        # Don't init it with a map just because it is shorter
        # C'mon! How could you expect a map to be ordered while initializing it 
        # with an *unordered* map?
        return OrderedDict([('precision', score[0]), ('answered_rate', score[1])])
