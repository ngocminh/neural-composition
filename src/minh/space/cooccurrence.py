# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from logging import getLogger
from string import letters, uppercase
import locale
import os
from minh.experiment import ukwac
from minh.experiment.ukwac import sentences, dependency_nodes, lemma_and_pos
from minh.experiment.ioutils import read_stripped_lines


__log = getLogger(__name__)


def __extract_domain_column_of_direction(nodes, index, direction, columns):
    i = index + direction
    count = 0;
    while count < 7 and i in range(len(nodes)):
        node = nodes[i]
        # an error in UkWaC: most % is marked as NN
        if ukwac.is_noun(node):
            # word form is used in the original paper, see table 1&2
            columns.append(node.word.lower()) 
            return 
        if node.pos[0] in letters:
            count += 1
        i += direction 


def extract_domain_columns(nodes, index):
    columns = []
    __extract_domain_column_of_direction(nodes, index, 1, columns)
    __extract_domain_column_of_direction(nodes, index, -1, columns)
    if len(columns) == 2 and columns[0] == columns[1]:
        return columns[1:] # remove one context if they are the same
    return columns


def __strip_pos(pattern):
    while len(pattern) > 0 and pattern[-1][0] in uppercase:
        del pattern[-1]


def __replace_T_by_to(pattern):
    for index, item in enumerate(pattern):
        if item == "T":
            pattern[index] = "to"


def __reduce_noun_sequence(pattern):
    i = 0
    while i < len(pattern):
        if pattern[i] == "N":
            while i+1 < len(pattern) and pattern[i+1] == "N":
                del pattern[i+1]
        i += 1


def __extract_function_words_of_direction(nodes, index, direction):
    i = index + direction
    count = 0;
    has_verb = False
    general_pattern = []
    specific_pattern = []
    while count < 3 and i in range(len(nodes)):
        node = nodes[i]
        if node.pos[0] not in letters:
            break
        pos = node.pos[0]
        # general patterns
        if pos == "V":
            general_pattern.append(node.word.lower())
        else:
            general_pattern.append(pos)
        # specific patterns
        if pos == "V" or pos == "M" or pos == "I" or pos == "T":
            specific_pattern.append(node.word.lower())
        else:
            specific_pattern.append(pos)
        # has verb?
        if pos == "V":
            has_verb = True
        # next word
        count += 1
        i += direction
    __strip_pos(general_pattern)
    __strip_pos(specific_pattern)
    __replace_T_by_to(general_pattern)
    __replace_T_by_to(specific_pattern)
    __reduce_noun_sequence(general_pattern)
    __reduce_noun_sequence(specific_pattern)
    if direction < 0:
        general_pattern.reverse()
        specific_pattern.reverse()
    return (general_pattern, specific_pattern, has_verb)
        

def __combine_function_words(left, left_has_verb, right, right_has_verb, columns):
    if left and right:
        column = []
        column.extend(left)
        column.append('X')
        column.extend(right)
        columns.append("_".join(column))
    if left_has_verb:
        columns.append("_".join(left) + "_X")
    if right_has_verb:
        columns.append("X_" + "_".join(right))


def extract_function_columns(nodes, index):
    columns = []
    right = __extract_function_words_of_direction(nodes, index, 1)
    left = __extract_function_words_of_direction(nodes, index, -1)
    __combine_function_words(left[0], left[2], right[0], right[2], columns)
    __combine_function_words(left[1], left[2], right[1], right[2], columns)
    return set(columns) # prevent duplication


def words_in_sentence(nodes, target_index):
    words = []
    for index, node in enumerate(nodes):
        if index == target_index:
            continue
        words.append(node.word.lower())
    return words


def _general_cooccurrences(files, callback,
                        row_filter, extract_row,
                        column_filter, extract_columns,
                        suppress_exceptions=0):
#    I filter by row (converted from node(s)) not by node because I need to
#    filter words and phrases alike.
    count = 0
    for sentence in sentences(files):
        try:
            nodes = dependency_nodes(sentence, relation_parsed=False)
            for i in range(len(nodes)):
                node = nodes[i]
                row = extract_row(node)
                if not row_filter(row):
#                    print "Filtered row: %s" %row
                    continue
                columns = extract_columns(nodes, i)
                for column in columns:
                    if not column_filter(column):
#                        print "Filtered column: %s" %column
                        continue
                    try:
                        callback((row, column))
                    except StopIteration:
                        return
            # print progress
            count += 1
            if count % 100000 == 0:
                __log.debug("Number of sentences of process %d so far: %s" 
                            %(os.getpid(), 
                            locale.format("%d", count, grouping=True)))
        except Exception as e:
            if suppress_exceptions > 0:
                __log.error("Suppressed an exception. Problematic sentence comes below.")
                __log.error(sentence)
                __log.exception(e)
                suppress_exceptions -= 1
            else:
                raise
    __log.debug("Processed sentences: %d" %count)


def word_cooccurrences(files, callback,
                      row_filter=lambda row: True, 
                      extract_row=lemma_and_pos,
                      filter_column=lambda column: True,
                      suppress_exceptions=0):
    _general_cooccurrences(files, callback, row_filter, extract_row,
                          filter_column, words_in_sentence, suppress_exceptions)
    

class WordCooccurrences:
    

    def __init__(self, left_window, right_window):
        self.left_window = left_window
        self.right_window = right_window
        
    
    def extract_column(self, nodes, target_index):
        words = []
        for index, node in enumerate(nodes):
            if (index != target_index and
                (self.left_window < 0 or index >= target_index-self.left_window) and
                (self.right_window < 0 or index <= target_index+self.right_window)):
                words.append(node.word.lower())
        return words
        
        
    def __call__(self, files, callback, 
                 row_filter=lambda row: True, extract_row=lemma_and_pos, 
                 filter_column=lambda column: True,
                 suppress_exceptions=0):
        _general_cooccurrences(files, callback, row_filter, extract_row, 
                               filter_column, self.extract_column, 
                               suppress_exceptions)


def domain_cooccurrences(files, callback,
                        row_filter=lambda row: True, 
                        extract_row=lemma_and_pos,
                        column_filter=lambda column: True,
                        suppress_exceptions=0):
    _general_cooccurrences(files, callback, 
                           row_filter, extract_row,
                           column_filter, extract_domain_columns,
                           suppress_exceptions)
                
                
def function_cooccurrences(files, callback,
                        filter_row_node=lambda node: True, 
                        extract_row=lemma_and_pos,
                        column_filter=lambda column: True,
                        suppress_exceptions=0):
    _general_cooccurrences(files, callback, 
                           filter_row_node, extract_row,
                           column_filter, extract_function_columns,
                           suppress_exceptions)


def evaluate_contexts(contexts, ref_contexts):
    if isinstance(contexts, basestring):
        contexts_path = contexts
        contexts = set()
        with open(contexts_path, "r") as f:
            for line in f:
                context = line.strip().split("-")[0]
                contexts.add(context)
    if isinstance(ref_contexts, basestring):
        ref_contexts = set(read_stripped_lines(ref_contexts))
    print len(contexts)
    print len(ref_contexts)
    correct = 0
    for context in contexts:
        if context in ref_contexts:
            correct += 1
    precision = correct / float(len(contexts))
    recall = correct / float(len(ref_contexts))
    f1 = precision * recall / (2*(precision + recall))
    return (precision, recall, f1)


def write_sparse(cooccurrences, path, gzip=False):
    f = None
    if path.endswith(".gz"):
        f = gzip.open(path, "w")
    else:
        f = open(path, "w")
    with f:
        print len(cooccurrences)
        for key in cooccurrences:
            f.write("%s\t%s\t%d\n" %(key[0], key[1], cooccurrences[key]))


def write(cooccurrences, path, format_=None, gzip=False):
    if format_ is None:
        dot = path.rfind(".")
        format_ = path[dot+1:]
        if format_.endswith(".gz"):
            format_ = format[:-3]
    if format_ == 'sm':
        write_sparse(cooccurrences, path, gzip)
    else:
        raise NotImplementedError
