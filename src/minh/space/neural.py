# -*- coding: utf-8 -*-
'''
Created on Jan 7, 2014

@author: Lê Ngọc Minh
'''
from composes.composition.composition_model import CompositionModel
from logging import getLogger
import numpy as np
from composes.matrix.dense_matrix import DenseMatrix
import theano
from pylearn2.datasets.dense_design_matrix import DenseDesignMatrix
from pylearn2.config import yaml_parse
from minh.miscutils import as_dense_numpy_matrix, check_matrix
from pylearn2.costs.cost import Cost, DefaultDataSpecsMixin
from theano import tensor as T
from pylearn2.models.mlp import Linear, Layer
from pylearn2.utils import wraps, serial
from pylearn2.space import Space, VectorSpace
from string import Template
from collections import OrderedDict
import pickle
import os
import tempfile


_log = getLogger(__name__)


def _get_prediction_function(model):
    Xb = model.get_input_space().make_batch_theano()
    ybf = model.fprop(Xb)
    return theano.function([Xb], ybf, allow_input_downcast=True)


def _divide_datasets(x_all, y_all, valid_set_portion):
    rows = range(x_all.shape[0])
    if valid_set_portion > 0:
        np.random.shuffle(rows)
        division = int((1-valid_set_portion)*len(rows))
        ds_train = DenseDesignMatrix(X=x_all[rows[:division], :], 
                                     y=y_all[rows[:division],:]) 
        ds_valid = DenseDesignMatrix(X=x_all[rows[division:],:], 
                                     y=y_all[rows[division:],:])
        _log.info("Validating set size: %d" %ds_valid.X.shape[0])
    else:
        ds_train = DenseDesignMatrix(X=x_all, y=y_all)
        ds_valid = None
        _log.info("Validating set not used")
    _log.info("Training set size: %d" %ds_train.X.shape[0])
    return (ds_train, ds_valid)


def _train_with_error_reporting(pylearn_train):
    try:
        pylearn_train.main_loop()
    except AssertionError:
        path = 'training-data-error.pkl'
        with open(path, 'wb') as f:
            pickle.dump(pylearn_train.dataset, f)
        _log.error("Training failed. The data is saved to %s" %path)
        raise
    

class NeuralComposition(CompositionModel):
    """
    A class of composition models which are based on Pylearn2 models. A typical
    model uses an artificial neural network to perform composition but one can 
    also use any model that can be express in Pylearn2's language.
    """

    _name = "neural"

    def __init__(self, config_file, dims, valid_set_portion=0.1, param_map=dict()):
        self.best_model_path = tempfile.mkdtemp(prefix=self.name + "-") + "/best.pkl"
        with open(config_file) as f:
            param_map = dict(param_map) # avoid changing the map
            param_map.update({'input_dims': dims*2, 'output_dims': dims,
                              'best_path': '"%s"' %self.best_model_path})
            yaml = Template(f.read()).substitute(param_map)
            _log.info("Loading model from YAML string:\n" + yaml)
            self.pylearn_train = yaml_parse.load(yaml)
        self._predict = _get_prediction_function(self.pylearn_train.model)
        self.valid_set_portion = valid_set_portion


    def _input_matrices(self, arg1_mat, arg2_mat):
        arg1_mat = as_dense_numpy_matrix(arg1_mat)
        arg2_mat = as_dense_numpy_matrix(arg2_mat)
        return np.concatenate((arg1_mat, arg2_mat), axis=1)

    
    def _compose(self, arg1_mat, arg2_mat):
        x = self._input_matrices(arg1_mat, arg2_mat)
        # allow_input_downcast: allow casting float 64 --> float 32
        return DenseMatrix(self._predict(x))
    
    
    def _create_datasets(self, arg1_mat, arg2_mat, phrase_mat):
        x_all = self._input_matrices(arg1_mat, arg2_mat)
        check_matrix(x_all, "input")
        y_all = as_dense_numpy_matrix(phrase_mat)
        check_matrix(y_all, "output")
        return _divide_datasets(x_all, y_all, self.valid_set_portion)


    def _train(self, arg1_mat, arg2_mat, phrase_mat):
        ds_train, ds_valid = self._create_datasets(arg1_mat, arg2_mat, phrase_mat)
        self.pylearn_train.dataset = ds_train
        self.pylearn_train.algorithm._set_monitoring_dataset(
                    {'train': ds_train, 'valid': ds_valid} if ds_valid
                    else {'train': ds_train})
        _train_with_error_reporting(self.pylearn_train)
        if os.path.exists(self.best_model_path):
            self._predict = _get_prediction_function(serial.load(self.best_model_path))
            _log.info("Best model loaded from %s" %self.best_model_path)
        else:
            self._predict = _get_prediction_function(self.pylearn_train.model)


class BlendingNeuralComposition(CompositionModel):
    """
    A class of composition models which are based on Pylearn2 models. A typical
    model uses an artificial neural network to perform composition but one can 
    also use any model that can be express in Pylearn2's language.
    
    A blending model thinks the meaning of a phrase can be achieved by blending
    a base meaning to a direction depending on the two component words. This 
    specific model uses the average of two vectors as the base meaning. The 
    direction is learned from the data. 
    """

    _name = "blending-neural"
    
    def __init__(self, config_file, dims, valid_set_portion=0.1, param_map=dict()):
        self.best_model_path = tempfile.mkdtemp(prefix=self.name + "-") + "/best.pkl"
        with open(config_file) as f:
            param_map = dict(param_map) # avoid changing the map
            param_map.update({'input_dims': dims*2, 'output_dims': dims,
                              'best_path': '"%s"' %self.best_model_path})
            yaml = Template(f.read()).substitute(param_map)
            _log.info("Loading model from YAML string:\n" + yaml)
            self.pylearn_train = yaml_parse.load(yaml)
        self._predict = _get_prediction_function(self.pylearn_train.model)
        self.valid_set_portion = valid_set_portion

    
    def _compose(self, arg1_mat, arg2_mat):
        base, x = self._input_matrices(arg1_mat, arg2_mat)
        # allow_input_downcast: allow casting float 64 --> float 32
        return DenseMatrix(base + self._predict(x))
     

    def _train(self, arg1_mat, arg2_mat, phrase_mat):
        ds_train, ds_valid = self._create_datasets(arg1_mat, arg2_mat, phrase_mat)
        self.pylearn_train.dataset = ds_train
        self.pylearn_train.algorithm._set_monitoring_dataset(
                    {'train': ds_train, 'valid': ds_valid} if ds_valid
                    else {'train': ds_train})
        _train_with_error_reporting(self.pylearn_train)
        if os.path.exists(self.best_model_path):
            self._predict = _get_prediction_function(serial.load(self.best_model_path))
            _log.info("Best model loaded from %s" %self.best_model_path)
        else:
            self._predict = _get_prediction_function(self.pylearn_train.model)
        _log.info("Mean blending norms on training set: %f, std: %f"
                  %self._get_blending_norms(ds_train.X))
        

    def _input_matrices(self, arg1_mat, arg2_mat):
        arg1_mat = as_dense_numpy_matrix(arg1_mat)
        arg2_mat = as_dense_numpy_matrix(arg2_mat)
#        base = _normalize_np(arg1_mat + arg2_mat)
        base = (arg1_mat + arg2_mat)/2.
        concat = np.concatenate((arg1_mat, arg2_mat), axis=1)
        return (base, concat) 
   
    
    def _create_datasets(self, arg1_mat, arg2_mat, phrase_mat):
        base, x_all = self._input_matrices(arg1_mat, arg2_mat)
        check_matrix(base, "base")
        check_matrix(x_all, "input")
        y_all = as_dense_numpy_matrix(phrase_mat) - base
        check_matrix(y_all, "output (may be zero where phrase-vector==base)")
        return _divide_datasets(x_all, y_all, self.valid_set_portion)


    def _get_blending_norms(self, dataset):
        bld = self._predict(dataset)
        norms = _norm_np(bld)
        return norms.mean(), norms.std()
        

class MeanSquaredErrorCost(DefaultDataSpecsMixin, Cost):
    
    supervised = True

    def expr(self, model, data, *args, **kwargs):
        """
        J = 1/m sum_{i=1}^m (y-y_hat)^T (y-y_hat)
        """
        self.get_data_specs(model)[0].validate(data)
        X, Y = data
        Y_hat = model.fprop(X)
        return self.cost(Y, Y_hat)


    def cost(self, Y, Y_hat):
        return self.cost_from_cost_matrix(self.cost_matrix(Y, Y_hat))

    def cost_from_cost_matrix(self, cost_matrix):
        return cost_matrix.sum(axis=1).mean()

    def cost_matrix(self, Y, Y_hat):
        return T.sqr(Y - Y_hat)


class L1OutputPenalty(DefaultDataSpecsMixin, Cost):
    """
    TODO: penalty on activation of hidden layers?
    L1 regulation promote sparseness by penalizing activation patterns having
    many active neurons.  
    """
    
    supervised = True


    def expr(self, model, data, *args, **kwargs):
        self.get_data_specs(model)[0].validate(data)
        X, _ = data
        Y_hat = model.fprop(X)
        val = abs(Y_hat).mean().mean()
        val.name = "l1_penalty"
        return val

    
def average_activity_ratio_expr(matrix):
    """
    The average fraction of active neurons. 
    http://www.scholarpedia.org/article/Sparse_coding#Average_activity_ratio
    """
    nonzeros = abs(matrix) >= 1e-8
    nonzero_count = nonzeros.sum().sum()
    total_count = matrix.shape[0]*matrix.shape[1]
    return T.cast(T.cast(nonzero_count,'float32')/total_count,'float32') 


def _add_channels(state, target, rval):
    rval['average_activity_ratio'] = average_activity_ratio_expr(state)
    if target is not None:
        cos = T.mul(state, target).sum(axis=1)
        rval['cos_similarity_mean'] = cos.mean()
        rval['cos_similarity_max'] = cos.max()
        rval['cos_similarity_min'] = cos.min()
        rval['cos_similarity_std'] = cos.std()
        
        
def _normalize_theano(p):
    clip = T.constant(1e-16)
    clipped_norms = T.sqrt(T.maximum(T.sqr(p).sum(
                           axis=1, keepdims=True), clip))
    return p/clipped_norms


def _normalize_np(p):
    clip = 1e-16
    clipped_norms = np.sqrt(np.maximum(np.sum(np.square(p), 
                            axis=1, keepdims=True), clip))
    return p/clipped_norms


def _norm_np(p):
    return np.sqrt(np.sum(np.square(p), axis=1, keepdims=False))


class NormalizedLinear(Linear):
    """
    A layer that performs an affine transformation of its (vectorial)
    input followed by a vector normalization.
    """

    def __init__(self, **kwargs):
        super(NormalizedLinear, self).__init__(**kwargs)


    @wraps(Layer.fprop)
    def fprop(self, state_below):
        p = self._linear_part(state_below)
        return _normalize_theano(p)
    

    @wraps(Layer.get_monitoring_channels_from_state)
    def get_monitoring_channels_from_state(self, state, target=None):
        rval = super(NormalizedLinear, self).get_monitoring_channels_from_state(state, target)
        _add_channels(state, target, rval)
        return rval
    
    
class Normalization(Layer):
    """
    A layer that performs only normalization.
    """

    def __init__(self, layer_name):
        self.layer_name = layer_name
        self._params = []


    @wraps(Layer.fprop)
    def fprop(self, state_below):
        return _normalize_theano(state_below)
    
    
    @wraps(Layer.get_monitoring_channels)
    def get_monitoring_channels(self):
        return OrderedDict()
    

    @wraps(Layer.get_monitoring_channels_from_state)
    def get_monitoring_channels_from_state(self, state, target=None):
        rval = super(Normalization, self).get_monitoring_channels_from_state(state, target)
        _add_channels(state, target, rval)
        return rval
    

    @wraps(Layer.set_input_space)
    def set_input_space(self, space):
        self.input_space = space
        self.output_space = VectorSpace(space.get_total_dimension())
        if not isinstance(space, Space):
            raise TypeError("Expected Space, got " +
                            str(space)+" of type "+str(type(space)))


    @wraps(Layer.cost)
    def cost(self, Y, Y_hat):
        """
        The default cost of this layer is mean squared error.
        """
        return self.cost_from_cost_matrix(self.cost_matrix(Y, Y_hat))

    @wraps(Layer.cost_from_cost_matrix)
    def cost_from_cost_matrix(self, cost_matrix):
        return cost_matrix.sum(axis=1).mean()

    @wraps(Layer.cost_matrix)
    def cost_matrix(self, Y, Y_hat):
        return T.sqr(Y - Y_hat)

    
