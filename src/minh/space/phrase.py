# -*- coding: utf-8 -*-
'''
Created on Jan 11, 2014

@author: Lê Ngọc Minh
'''
from minh.experiment.ukwac import sentences, dependency_nodes, lemma_and_pos
import locale
import os
from logging import getLogger


__log = getLogger(__name__)


def __general_cooccurrences(files, callback,
                        row_filter, extract_row,
                        column_filter, extract_columns,
                        suppress_exceptions=0):
    count = 0
    for sentence in sentences(files):
        try:
            nodes = dependency_nodes(sentence, relation_parsed=False)
            for i in range(len(nodes)):
                node = nodes[i]
                if node.head_id <= 0:
                    continue
                head_index = int(node.head_id) - 1
                head = nodes[head_index]
                row = extract_row(node, head)
                if not row_filter(row):
                    continue
                columns = extract_columns(nodes, i, head_index)
                for column in columns:
                    if not column_filter(column):
                        continue
                    try:
                        callback((row, column))
                    except StopIteration:
                        return
            # print progress
            count += 1
            if count % 100000 == 0:
                __log.debug("Number of sentences of process %d so far: %s" 
                            % (os.getpid(),
                            locale.format("%d", count, grouping=True)))
        except Exception as e:
            if suppress_exceptions > 0:
                __log.error("Suppressed an exception while processing %s. "
                            "The problematic sentence is dropped." % str(files))
                __log.exception(e)
                suppress_exceptions -= 1
            else:
                raise
    __log.debug("Processed sentences: %d" % count)


def words_in_sentence(nodes, target_index, target_index2):
    words = []
    for index, node in enumerate(nodes):
        if index == target_index or index == target_index2:
            continue
        words.append(node.word.lower())
    return words


def phrase_lemma_and_pos(dependent, head):
    return "%s_%s" % (lemma_and_pos(dependent), lemma_and_pos(head))



def word_cooccurrences(files, callback,
                       filter_row_nodes=lambda node: True,
                       extract_row=phrase_lemma_and_pos,
                       filter_column=lambda node: True,
                       suppress_exceptions=0):
    __general_cooccurrences(files, callback,
                           filter_row_nodes, extract_row,
                           filter_column, words_in_sentence,
                           suppress_exceptions)
