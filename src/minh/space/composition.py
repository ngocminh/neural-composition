# -*- coding: utf-8 -*-
'''
Created on Dec 23, 2013

@author: Lê Ngọc Minh
'''

from composes.exception.illegal_state_error import IllegalOperationError
from composes.composition.composition_model import CompositionModel
import numpy
from composes.matrix.dense_matrix import DenseMatrix
from scipy.sparse.base import issparse
from composes.semantic_space.space import Space


class NonNegativeMultiplicative(CompositionModel):
    """
    Implements the component-wise multiplication compositional model:
    
        :math:`\\vec{p} = \\vec{u} \\cdot \\vec{v}`
    
    where :math:`\\vec{p}` is the vector of the composed phrase and
    :math:`\\vec{u}, \\vec{v}` are the vectors of the components.
    
    :math:`\\vec{u} \\cdot \\vec{v} = (u_1v_1,...,u_nv_n)`  
    """
    
    _name = "multiplicative"

    def __init__(self):
        """
        Constructor
        """
        
        
    def train(self, train_data, arg_space, phrase_space):
        """
        Current multiplicative model cannot be trained, it has no parameters.
        """    
        raise IllegalOperationError("Cannot train multiplicative model!")
    
    
    def __as_dense(self, mat):
        if issparse(mat):
            mat = mat.todense()
        return mat
    
    
    def _compose(self, arg1_mat, arg2_mat):
        mat1 = self.__as_dense(arg1_mat.mat).clip(0)
        mat2 = self.__as_dense(arg2_mat.mat).clip(0)
        return DenseMatrix(numpy.multiply(mat1, mat2))
    
    
    def export(self, filename):
        """
        Current multiplicative model cannot be exported, it has no parameters.
        """   
        raise IllegalOperationError("cannot export a Multiplicative model.")
            
                
class SyntaxSensitiveComposition(CompositionModel):

    def __init__(self, factory_method, name):
        self.factory_method = factory_method
        self.models = dict()
        self._name = "%s(syn)" %name


    def _get_model(self, code):
        if code not in self.models:
            self.models[code] = self.factory_method()
        return self.models[code]

            
    def compose(self, data, arg_space):
        groups = self.grouped_by_code(data)
        result = None
        for code in groups:
            space = self._get_model(code).compose(groups[code], arg_space)
            if result is None:
                result = space
            else:
                result = Space.vstack(result, space)
        return result
        
        
    def train(self, train_data, arg_space, phrase_space):
        groups = self.grouped_by_code(train_data)
        for code in groups:
            self._get_model(code).train(groups[code], arg_space, phrase_space)


    @classmethod
    def code(cls, phrase):
        """
        Return the syntax code of a phrase.
        
        @param phrase: a tuple (dependent, head, phrase(optional)) 
        """
        w1 = phrase[0].split('-')
        if len(w1) < 2: raise ValueError("No POS tag found in: %s %s" 
                                         %(phrase[0], str(phrase)))
        w2 = phrase[1].split('-')
        if len(w2) < 2: raise ValueError("No POS tag found in: %s %s" 
                                         %(phrase[1], str(phrase)))
        
        pos1 = w1[-1] # last part after dash is assumed to be POS tag
        pos2 = w2[-1] # last part after dash is assumed to be POS tag
        code = pos1 + '_' + pos2
        return code


    @classmethod
    def grouped_by_code(cls, data):
        """
        Receive a list of tuples (word1, word2, phrase) and return a dictionary
        of groups. All tuples in a group share the same code.
        """
        data = sorted(data, key=lambda phrase: cls.code(phrase))
        curr_code = None
        start_index = -1
        groups = dict()
        for (index, phrase) in enumerate(data):
            new_code = cls.code(phrase)
            if new_code != curr_code:
                if curr_code is not None:
                    groups[curr_code] = data[start_index:index]
                curr_code = new_code
                start_index = index 
        if curr_code is not None:
            groups[curr_code] = data[start_index:]
        return groups