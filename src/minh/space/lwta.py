# Adopted from: https://github.com/goodfeli/forgetting/blob/master/lwta.py
__author__ = "Xia Da, Ian Goodfellow, Minh Ngoc Le"

import numpy
from theano import tensor as T
from pylearn2.models.mlp import Linear

def lwta(p, block_size):
    """
    Apply hard local winner-take-all on every rows of a theano matrix.

    Parameters
    ----------
    p: theano matrix
        Matrix on whose rows LWTA will be applied.
    block_size: int
        Number of units in each block.
    """
    batch_size = p.shape[0]
    num_filters = p.shape[1]
    num_blocks = num_filters // block_size
    w = p.reshape((batch_size, num_blocks, block_size))
    block_max = w.max(axis=2).dimshuffle(0, 1, 'x') * T.ones_like(w)
    max_mask = T.cast(w >= block_max, 'float32')
    indices = numpy.array(range(1, block_size+1))
    max_mask2 = max_mask * indices
    block_max2 = max_mask2.max(axis=2).dimshuffle(0, 1, 'x') * T.ones_like(w)
    max_mask3 = T.cast(max_mask2 >= block_max2, 'float32')
    w2 = w * max_mask3
    w3 = w2.reshape((p.shape[0], p.shape[1]))
    return w3


def leaky_lwta(p, block_size, leak_rate):
    """
    Apply hard local winner-take-all on every rows of a theano matrix.

    Parameters
    ----------
    p: theano matrix
        Matrix on whose rows LWTA will be applied.
    block_size: int
        Number of units in each block.
    """
    batch_size = p.shape[0]
    num_filters = p.shape[1]
    num_blocks = num_filters // block_size
    w = p.reshape((batch_size, num_blocks, block_size))
    block_max = w.max(axis=2).clip(0, float("inf")).dimshuffle(0, 1, 'x') * T.ones_like(w)
    max_mask = T.cast(w >= block_max, 'float32')
    indices = numpy.array(range(1, block_size+1))
    max_mask2 = max_mask * indices
    block_max2 = max_mask2.max(axis=2).clip(1, batch_size).dimshuffle(0, 1, 'x') * T.ones_like(w)
    w2 = T.switch(max_mask2 >= block_max2, w, leak_rate*w)
    w3 = w2.reshape((p.shape[0], p.shape[1]))
    return w3


class LWTA(Linear):
    """
    An MLP Layer using the LWTA non-linearity.
    """
    def __init__(self, block_size=5, rectified=False, **kwargs):
        super(LWTA, self).__init__(**kwargs)
        self.block_size = block_size
        self.rectified = rectified

    def fprop(self, state_below):
        p = super(LWTA, self).fprop(state_below)
        if self.rectified:
            p = T.switch(p > 0., p, 0)
        p = lwta(p, self.block_size)
        p.name = self.layer_name + '_out'
        return p


class LeakyLWTA(Linear):
    """
    An MLP Layer using the leaky LWTA non-linearity.
    """
    def __init__(self, block_size=5, leak_rate=0.01, **kwargs):
        super(LeakyLWTA, self).__init__(**kwargs)
        self.block_size = block_size
        self.leak_rate = leak_rate


    def fprop(self, state_below):
        p = super(LeakyLWTA, self).fprop(state_below)
        p = leaky_lwta(p, self.block_size, self.leak_rate)
        p.name = self.layer_name + '_out'
        return p


class CorruptedInputLWTA(LWTA):
    
    def __init__(self, corruptor, **kwargs):
        super(CorruptedInputLWTA, self).__init__(**kwargs)
        self.corruptor = corruptor

    def fprop(self, state_below):
        corrupted = self.corruptor(state_below)
        return super(CorruptedInputLWTA, self).fprop(corrupted)