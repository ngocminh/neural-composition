# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
import locale
import logging
from logging.config import fileConfig
import numpy
import os
from random import sample
import random
import subprocess
import sys

from composes.composition.full_additive import FullAdditive
from composes.composition.lexical_function import LexicalFunction
from composes.composition.weighted_additive import WeightedAdditive
from composes.exception.illegal_state_error import IllegalOperationError
from composes.semantic_space.peripheral_space import PeripheralSpace
from composes.semantic_space.space import Space
from composes.similarity.cos import CosSimilarity
from composes.transformation.dim_reduction.svd import Svd
from composes.transformation.scaling.ppmi_weighting import PpmiWeighting
from composes.transformation.scaling.row_normalization import RowNormalization
from composes.utils import io_utils
from composes.utils.regression_learner import RidgeRegressionLearner, \
    LstsqRegressionLearner

from minh.evaluate.evaluate import get_target_words, \
    get_target_phrases, evaluate_space, evaluate_init, print_summary
from minh.evaluate.mitchell_lapata import VerbDisambiguating, \
    ShortPhraseSimilarity
from minh.evaluate.turney import Turney, TurneyRanking
from minh.experiment.count import add_candidate_rows, \
    count_cooccurrences_external, intersecting_phrases_balanced, \
    find_contexts_internal, log_phrase_statistics, \
    intersecting_phrases_unbalanced
from minh.experiment.ioutils import convert_paths, read_stripped_lines, \
    safe_write, sort_by_size, write_space
from minh.experiment.ukwac import lemma_and_pos
from minh.miscutils import check_matrix
from minh.space import phrase
from minh.space.composition import NonNegativeMultiplicative, \
    SyntaxSensitiveComposition
from minh.space.cooccurrence import word_cooccurrences
from minh.space.phrase import phrase_lemma_and_pos


__log = logging.getLogger("minh.experiment.experiment")
try:
    locale.setlocale(locale.LC_ALL, 'en_US') # enable pretty number printing
except:
    try:
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8') # enable pretty number printing
    except:
        pass


EXPERIMENT_SECT = "experiment"
EVALUATION_SECT = "evaluate"
    
    
def read_raw_space(space_path, col_path, target_words):
    __log.info("Started reading: " + space_path)
    space = Space.build(format="sm", data=space_path, cols=col_path)
    __log.info("Finished reading.")
    # The biggest row number that I has successfully run on
    # This check is here because some of my raw spaces were too large while 
    # recreating them takes too long. You can safely remove it if you have 
    # restricted the size of raw spaces before hand or want to experiment 
    # with larger spaces.
    n = len(space.id2row)
    max_n = 230000
    if n > max_n:
        target_ids = set(i for i in xrange(n) if space.id2row[i] in target_words)
        added_ids = set(i for i in xrange(n) if space.id2row[i] not in target_words)
        # Caution: len(target_ids) may differ from len(target_words)
        new_ids = target_ids.union(random.sample(added_ids, 
                                                 max(max_n-len(target_ids),0)))
        new_id2row = [space.id2row[i] for i in xrange(n) if i in new_ids]
        new_matrix = space.cooccurrence_matrix[sorted(new_ids),:]
        new_space = Space(new_matrix, new_id2row, space.id2column)
        space = new_space
        __log.warn("Space is so large that PPMI and SVD can't be applied on, " 
                   "randomly removed %d rows. This may lower performance." 
                   %(n-len(new_ids)))
    return space
    
    
def process_ppmi_space(space_path, col_path, target_words, result_path):
    ppmi_path = result_path + '.ppmi'
    if result_path and os.path.exists(ppmi_path + '.sm'):
        __log.info("Reading PPMI space from %s..." %ppmi_path) 
        space = Space.build(format="sm", data=ppmi_path + '.sm', 
                            cols=ppmi_path + '.cols', rows=ppmi_path + '.rows')
        __log.info("PPMI space read.")
    else:
        space = read_raw_space(space_path, col_path, target_words)
        __log.info("Start PPMI")
        space = space.apply(PpmiWeighting())
        space.export(ppmi_path, format='sm')
        __log.info("Finished PPMI")
        
    __log.info("Number of non-zero elements in PPMI space: %d" 
                %len(space.get_cooccurrence_matrix().mat.data))
    __log.info("PPMI space dimensions: " + str(space.cooccurrence_matrix.shape))
    check_matrix(space.cooccurrence_matrix.mat, "word-space-ppmi")
    return space
            
    
def process_svd_space(space_path, col_path, target_words, svd_dimensionality, 
                      result_path):
    svd_path = result_path + '.svd'
    if result_path and os.path.exists(svd_path):
        space = io_utils.load(svd_path)
        __log.info("SVD space is read from: " + svd_path)
    else:
        space = process_ppmi_space(space_path, col_path, target_words, 
                                   result_path)  
                
        __log.info("Start SVD (%d dimensions)" %svd_dimensionality)
        space = space.apply(Svd(svd_dimensionality))
        # It keeps causing error for large spaces
        if space.cooccurrence_matrix.shape[1] < 1500:
            safe_write(svd_path, write_space, space)
        __log.info("Finished SVD")

    __log.info("Number of non-zero elements in SVD space: %d" 
                %numpy.count_nonzero(space.get_cooccurrence_matrix().mat))
    __log.info("SVD space dimensions: " + str(space.cooccurrence_matrix.shape))
    check_matrix(space.cooccurrence_matrix.mat, "word-space-svd")
    return space


def process_space(space_path, col_path, target_words, result_path, 
                  nnse_dimensionality, nnse_sparsity, svd_dimensionality, 
                  nnse_enabled):
    __log.info("Start building semantic space")
    if result_path and os.path.exists(result_path):
        space = io_utils.load(result_path)
        __log.info("Space is read from: " + result_path)
    else:
        space = process_svd_space(space_path, col_path, target_words, 
                                  svd_dimensionality, result_path)

        if nnse_enabled:
            __log.info("Start NNSE")
            # this may be heavy
            from minh.space.nnse import NNSE
            space = space.apply(NNSE(nnse_dimensionality, nnse_sparsity))
            __log.info("Finished NNSE")

            #    Saving NNSE space in pickle format is a big waste.
            #    However, because the NNSE-ed matrix doesn't have column information, it
            #    cannot be saved in sparse format.
            #    One solution is to create temporary column index. It should be marked 
            #    with a special character such as _ and should be as short as possible.
            #    I didn't implement this solution yet because it requires modifying 
            #    DISSECT at various places while the extra disk space consumed is still
            #    acceptable.
            #    Minh, 13 Jan 2014
            safe_write(result_path, write_space, space)

    __log.info("Finished building semantic space")
    # TODO: can it work for both dense and sparse matrices? 
#    __log.info("Number of non-zero elements in final space: %d" 
#                %numpy.count_nonzero(space.get_cooccurrence_matrix().mat))
    __log.info("Space dimensions: " + str(space.cooccurrence_matrix.shape))
    check_matrix(space.cooccurrence_matrix.mat, "word-space-nnse")
    return space


def get_evaluations(conf):
    evaluations = []
    
    if conf.has_option(EVALUATION_SECT, "turney.path"):
        turney_question_path = conf.get(EVALUATION_SECT, "turney.path")
        turney = Turney(turney_question_path, 
                        conf.getboolean(EVALUATION_SECT, "turney.constrained"),
                        conf.get(EVALUATION_SECT, "turney.recovery-mode"))
        __log.info("Number of questions: %d" %len(turney.questions))
        if not turney.questions:
            raise ValueError("Empty question set, please check configuration.")
        evaluations.append(turney)
        
        if conf.getboolean(EVALUATION_SECT, "turney.ranking"):
            turney_ranking = TurneyRanking(turney_question_path)
            evaluations.append(turney_ranking)
    
    if conf.has_option(EVALUATION_SECT, "verb-disambiguating.path"):
        verb_disambiguating_path = conf.get(EVALUATION_SECT, "verb-disambiguating.path")
        evaluations.append(VerbDisambiguating(verb_disambiguating_path, CosSimilarity()))
    
    if conf.has_option(EVALUATION_SECT, "short-phrase.path"):
        short_phrase_path = conf.get(EVALUATION_SECT, "short-phrase.path")
        evaluations.append(ShortPhraseSimilarity(short_phrase_path, CosSimilarity()))
    
    return evaluations


class ModelFactory():
    
    def __init__(self, conf, model_name, dims, valid_set_portion):
        self.name = model_name
        self.dims = dims
        self.valid_set_portion = valid_set_portion
        section = "model_" + model_name
        self.yaml_path = conf.get(section, "config")
        self.blending = (conf.has_option(section, "blending") and 
                         conf.getboolean(section, "blending"))
        if conf.has_option(section, "save_path"):
            self.save_path = conf.get(section, "save_path")
            self.save_freq = conf.get(section, "save_freq")
        else: 
            self.save_path = None
        
        
    def __call__(self):
        # this module initialize Pylearn2 which may be heavy
        from minh.space import neural
        if self.blending:
            nn = neural.BlendingNeuralComposition(self.yaml_path, self.dims, 
                                                  self.valid_set_portion)
        else:
            nn = neural.NeuralComposition(self.yaml_path, self.dims, 
                                          self.valid_set_portion)
        if self.save_path:
            nn.save_path = self.save_path
            nn.save_freq = self.save_freq
        if self.name:
            nn._name = self.name
        return nn


def model_factories(conf, dims):
    """
    Return a list of factory function [f_i]. Calling f_i() will create a new 
    instance of the model i.
    """
    valid_set_portion = conf.getfloat(EXPERIMENT_SECT, "valid-set-portion")
    factories = []
    model_names = conf.get(EXPERIMENT_SECT, "neural-models").split(",")
    model_names = [model_name.strip() for model_name in model_names]
    for model_name in model_names:
        factory = ModelFactory(conf, model_name, dims, valid_set_portion)
        factories.append(factory)
    return factories


def get_compositions(conf, dims):
    compositions = []
    
    # Multiplicative
    if conf.getboolean(EXPERIMENT_SECT, "multiplicative.enabled"):
        mul = NonNegativeMultiplicative()
        compositions.append(mul)
    
    # WeightedAdditive
    if conf.getboolean(EXPERIMENT_SECT, "weighted-additive.enabled"):
        wa = SyntaxSensitiveComposition(WeightedAdditive, "weighted-additive")
        compositions.append(wa)
    
    # Full additive
    if conf.getboolean(EXPERIMENT_SECT, "full-additive.enabled"):
        def create_fadd():
            # Somehow LstsqRegressionLearner gets stuck (more than 1 day) 
            # whenever I run my SVD.unbalanced experiment.
            return FullAdditive(RidgeRegressionLearner())
        fa = SyntaxSensitiveComposition(create_fadd, "full-additive")
        compositions.append(fa)
    
    if conf.getboolean(EXPERIMENT_SECT, "lexical-function.enabled"):
        lf1 = LexicalFunction(LstsqRegressionLearner())
        lf1._name ="lexfunc(lstsq)"
        compositions.append(lf1)
        
#        lf2 = LexicalFunction(RidgeRegressionLearner())
#        lf2._name ="lexfunc(rr)"
#        compositions.append(lf2)
    
    # Neural network
    factories = model_factories(conf, dims)
    neural_models = [SyntaxSensitiveComposition(f, f.name) for f in factories]
    compositions.extend(neural_models)
    
    return compositions


def process_phrase_space(raw_phrase_space_path, word_context_path, space,
                         result_path):
    if result_path and os.path.exists(result_path):
        phrase_space = io_utils.load(result_path)
        __log.info("Read processed phrase space from %s" %result_path)
    else:
        __log.info("Processing peripheral space at %s..." %raw_phrase_space_path)
        phrase_space = PeripheralSpace.build(space, format="sm", 
                                             data=raw_phrase_space_path, 
                                             cols=word_context_path)
        __log.info("Finished processing peripheral space.")
        io_utils.save(phrase_space, result_path)
    __log.info("Phrase space dimensions: " + str(phrase_space.cooccurrence_matrix.shape))
    check_matrix(phrase_space.cooccurrence_matrix.mat, "phrase-space")
    return phrase_space


def get_training_data(phrases):
    training_data = []
    for p in phrases:
        training_data.append((p[0], p[1], "_".join(p)))
    return training_data


def build_composed_space(space, composition_model, training_data, phrase_space, 
                         targets, composed_space_path=None):
    if composed_space_path and os.path.exists(composed_space_path):
        __log.info("Reading composed space...")
        composed_space = io_utils.load(composed_space_path)
        __log.info("Finished reading processed peripheral space.")
        return composed_space
    
    # try to train if possible 
    try:
        __log.info("Training...")
        if isinstance(composition_model, LexicalFunction):
            # filter dependent words that won't be tested to avoid memory error
            dependents = set(dependent for dependent, _, _ in targets)
            lexfunc_training_data = [row for row in training_data
                                     if row[0] in dependents]
            composition_model.train(lexfunc_training_data, space, phrase_space)
        else:
            composition_model.train(training_data, space, phrase_space)
        __log.info("Finished training.")
    except IllegalOperationError:
        __log.info("Not trainable.")

    __log.info("Start building compositional space.")
    composed_space = composition_model.compose(list(targets), space)
    if composed_space_path:
        io_utils.save(composed_space, composed_space_path)
    __log.info("Finished building compositional space.")
    return composed_space


def get_phrases(corpus_paths, words, targets, max_phrases, result_path, 
                balanced, max_processes):
    if balanced:
        phrases = intersecting_phrases_balanced(corpus_paths, words, targets, 
                                                max_phrases, result_path, 
                                                max_processes)
    else:
        phrases = intersecting_phrases_unbalanced(corpus_paths, words, targets,
                                                  max_phrases, result_path, 
                                                  max_processes)
    log_phrase_statistics(phrases, targets)
    return phrases


def prepare_for_composition(space, name):
    #    prepare for composition
    norms = space.cooccurrence_matrix.norm(axis=1)
    norms = numpy.array(norms).reshape((-1,))
    is_safe = numpy.logical_and(norms > 0, numpy.logical_not(numpy.isnan(norms)))
    deletes = len(space.id2row) - is_safe.sum()
    if deletes > 0:
        rows = numpy.where(is_safe)[0].tolist()
        id2row = numpy.array(space.id2row)[rows].tolist()
        mat = space.cooccurrence_matrix[rows,:]
        space = Space(mat, id2row, space.id2column, None, space.column2id)
        __log.info("Deleted %d (zero or NaN) rows in %s." %(deletes, name))
    #    normalized space is transient i.e. not saved into file
    #    because we need to examine the space with untouched weights later
    space = space.apply(RowNormalization())
    check_matrix(space.cooccurrence_matrix.mat, "%s(NO-WAY!!!)" %name)
    return space


def run(conf):
    corpus_paths = convert_paths(conf.get(EXPERIMENT_SECT, "corpora"))
    if len(corpus_paths) <= 0:
        print "No corpus path is provided and found."
        return -1
    sort_by_size(corpus_paths, -1)
    __log.info("Corpus paths: " + str(corpus_paths))
    __log.info("File sizes: " + 
               str([os.stat(path).st_size for path in corpus_paths]))
    __log.info("Start experiment")

    evaluations = get_evaluations(conf)
    target_phrases = get_target_phrases(evaluations)
    target_words = get_target_words(evaluations)
    __log.info("Number of target words: %d" %len(target_words))
    
    words = set(target_words) # avoid modifying target_words
    add_candidate_rows(words, corpus_paths, lemma_and_pos,  
                       conf.getint(EXPERIMENT_SECT, "candidate-row-max-num"),
                       conf.getint(EXPERIMENT_SECT, "candidate-row-min-length"), 
                       conf.getint(EXPERIMENT_SECT, "candidate-row-min-count"),
                       conf.get(EXPERIMENT_SECT, "candidate-row-path"))
    __log.info("Number of candidate rows: %d" %len(words))
    
    context_threshold = conf.getint(EXPERIMENT_SECT, "context-threshold")
    if context_threshold > 0:
        __log.info("Context frequency threshold used: %d "
                    "(may lead to incorrect result)." %context_threshold)
    
    word_context_path = conf.get(EXPERIMENT_SECT, "word-contexts")
    word_contexts = find_contexts_internal(word_cooccurrences,
                                           corpus_paths, words, 
                                           conf.getint(EXPERIMENT_SECT, "max-word-contexts"), 
                                           word_context_path,
                                           context_threshold, 
                                           conf.getint(EXPERIMENT_SECT, "context-counting-max-processes"))
    if conf.has_option(EXPERIMENT_SECT, "stop-word-list"):
        stop_words = set(read_stripped_lines(conf.get(EXPERIMENT_SECT, "stop-word-list")))
        word_contexts.difference_update(stop_words)

    __log.info("Counting word co-occurrences...")
    raw_word_space_path = conf.get(EXPERIMENT_SECT, "raw-word-space")
    count_cooccurrences_external(word_cooccurrences, corpus_paths, words, 
                                 word_contexts, raw_word_space_path,
                                 lemma_and_pos, 
                                 conf.getint(EXPERIMENT_SECT, "cooccurrence-counting-max-processes"))
    __log.info("Finished counting word co-occurrences.")
        
    # We should count phrase co-occurrences before processing word space 
    # because raw phrase space can be reused in other experiments and they 
    # won't have to wait 
    phrases = get_phrases(corpus_paths, words, target_phrases,
                          conf.getint(EXPERIMENT_SECT, "max-phrases"),
                          conf.get(EXPERIMENT_SECT, "phrases"), 
                          conf.getboolean(EXPERIMENT_SECT, "phrases-balanced"), 
                          conf.getint(EXPERIMENT_SECT, "phrase-extracting-max-processes"))
    # We need all phrases so that observed baseline won't miss too much. Don't
    # worry, target phrases will be removed before training.
    # Notice that we need to remove the third element from every tuples
    # otherwise 'phrases' set will fail to detect duplication. 
    phrases.update((dependent, head) for dependent, head, _ in target_phrases)

    __log.info("Counting phrase co-occurrences...")
    raw_phrase_space_path = conf.get(EXPERIMENT_SECT, "raw-phrase-word-space")
    # use concatenation this instead of join() because it won't depend on the
    # number of parts in the tuple representing the phrase 
    joined_phrases = [p[0]+"_"+p[1] for p in phrases]
    count_cooccurrences_external(phrase.word_cooccurrences,
                                 corpus_paths, joined_phrases, word_contexts, 
                                 raw_phrase_space_path, phrase_lemma_and_pos, 
                                 conf.getint(EXPERIMENT_SECT, "cooccurrence-counting-max-processes"))
    __log.info("Finished counting phrase co-occurrences.")

    # Now we can start processing spaces
    space = process_space(raw_word_space_path, word_context_path, target_words, 
                          conf.get(EXPERIMENT_SECT, "processed-word-space"), 
                          conf.getint(EXPERIMENT_SECT, "dimensionality"), 
                          conf.getfloat(EXPERIMENT_SECT, "sparsity"), 
                          conf.getint(EXPERIMENT_SECT, "svd-dimensionality"),
                          conf.getboolean(EXPERIMENT_SECT, "nnse.enabled"))
    #TODO: separate observed phrase space and training phrase space
    phrase_space = process_phrase_space(raw_phrase_space_path, word_context_path, space,
                                        conf.get(EXPERIMENT_SECT, "processed-phrase-word-space"))

    evaluate_init(space)
    space = prepare_for_composition(space, "word-space")
    phrase_space = prepare_for_composition(phrase_space, "phrase-space")
    path_pattern = conf.get(EXPERIMENT_SECT, "composed-word-space")
    compositions = get_compositions(conf, space.cooccurrence_matrix.shape[1])
    
    results = []
    if conf.getboolean(EXPERIMENT_SECT, 'observed.enabled'):
        print "\n*** space:%s, composition:no (observed) ***"
        result = evaluate_space(phrase_space, "observed", evaluations)
        results.append(result)
        print "\n*** results ***\n"
        print_summary(results)
    training_phrases = phrases.difference(set(
            (dependent, head) for dependent, head, _ in target_phrases))
    __log.info("%d phrases were removed from training set because they "
               "are in the test set." %(len(phrases)-len(training_phrases)))
    sample_size = conf.getint(EXPERIMENT_SECT, "training-sample-size")
    if sample_size > 0:
        training_phrases = sample(training_phrases, sample_size)
        __log.info("%d phrases were sampled." %len(training_phrases))
    training_data = get_training_data(training_phrases)
    while len(compositions) > 0:
        composition = compositions[0]
        print "\n*** space:%s, composition:%s ***\n" %("word", composition.name)
        
        path = path_pattern %composition.name
        composed_space = build_composed_space(space, composition, training_data, 
                                              phrase_space, target_phrases, path)
        result = evaluate_space(composed_space, composition.name, evaluations)
        results.append(result)
        print "\n*** results ***\n"
        print_summary(results)
        
        # delete composition and its associated parameters to save memory space
        del compositions[0]
    __log.info("Finished all.")
    

def print_usage():
    print "Usage: python experiment.py config-paths"


#TODO change dimensionality, sparsity into nnse.*
# it should be available to test class
defaults = {'output-duplicate-path': '',
            'dimensionality': '1000',
            'phrases-balanced': 'False',
            'training-sample-size': '-1',
            'sparsity': '1',
            'svd-dimensionality': '1500',
            'weighted-additive.alpha': '0.5',
            'weighted-additive.beta': '0.5',
            'context-threshold': '0',
            'candidate-row-max-num': '20000',
            'candidate-row-min-length': '2',
            'max-word-contexts': '10000',
            'candidate-row-min-count': '0',
            'max-phrases': '10000',
            'nnse.enabled': 'true',
            'valid-set-portion': '0',
            'multiplicative.enabled': 'True',
            'weighted-additive.enabled': 'True',
            'full-additive.enabled': 'True',
            'observed.enabled': 'True',
            'lexical-function.enabled': 'True',
            'turney.ranking': 'False',
            'phrase-extracting-max-processes': '15',
            'turney.recovery-mode': 'no',
            'neural-models': ''}


def tee(path):
    if os.path.exists(path):
        print >> sys.stderr, "Output path already exists: %s" %path
        sys.exit(2)
    # ensure that parent dir exists
    parent_dir = os.path.dirname(os.path.realpath(path))
    if not os.path.exists(parent_dir):
        os.makedirs(parent_dir)
    tee = subprocess.Popen(["tee", path], stdin=subprocess.PIPE)
    os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
    os.dup2(tee.stdin.fileno(), sys.stderr.fileno())    


if __name__ == '__main__':
    
    if len(sys.argv) < 2:
        print_usage()
        sys.exit(1)

    conf = ConfigParser(defaults)
    conf_paths = sys.argv[1:]
    conf.read(conf_paths)
    
    tee(conf.get(EXPERIMENT_SECT, 'output-duplicate-path'))

    print "Config path(s): %s." %str(conf_paths)
    print "Configurations:"
    conf.write(sys.stdout)
    
    # be carefull: calling "logging.config.fileConfig" will cause 
    # AttributeError: 'module' object has no attribute 'config'
    fileConfig("logging.cfg") 
    # override by specified configs??
    #for conf_path in conf_paths: 
    #    fileConfig(conf_path) # config logging
    
    run(conf)
