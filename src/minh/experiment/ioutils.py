# -*- coding: utf-8 -*-
'''
Created on Mar 12, 2013

@author: Lê Ngọc Minh
'''

from glob import glob
import codecs
import logging
import os
import subprocess
import threading
from composes.utils import io_utils


_log = logging.getLogger(__name__)


utf8_enabled = False


def wrapis(inp):
    """
    Wrap input stream by a reader if utf8_enabled is on. Otherwise return the
    input stream as it is.
    """
    if utf8_enabled:
        return codecs.getreader('utf_8')(inp)
    else:
        return inp


def wrapos(out):
    """
    Wrap output stream by a writer if utf8_enabled is on. Otherwise return the
    output stream as it is.
    """
    if utf8_enabled:
        return codecs.getwriter('utf_8')(out)
    else:
        return out


def sort_by_size(paths, order=1):
    paths.sort(key=lambda path: order * os.stat(path).st_size)


def write_space(path, space):
    """
    Same as io_utils.save (in composes.utils package) except that it take path
    as the first parameter and space as the second one. To be used with 
    safe_write.
    """
    io_utils.save(space, path)


def safe_write(path, func, *args, **kwargs):
    """
    Write to a temporary file first. Iff successful, rename it to the desired
    file name. This is to avoid corrupted files or overwriting good results with
    bad ones.
    """
    if not path:
        return
#        TODO: should we better raise an exception?
#        raise ValueError("Path cannot be empty")
    temp_path = path + ".tmp"
    func(temp_path, *args, **kwargs)
    os.rename(temp_path, path)


def read_stripped_lines(path):
    with wrapis(open(path, "r")) as f:
        for line in f:
            yield line.strip()
   

def convert_paths(s):
    paths = []
    for pattern in s.split(","):
        paths.extend(glob(pattern))
    return paths
    

def read_counts(path):
    with wrapis(open(path)) as f:
        #TODO: return iterable instead of list?
        return [parse_count(line) for line in f]


def parse_count(line):
    item = line.split("\t")
    if len(item) > 2: 
        key = item[:-1]
    else:
        key = item[0]
    count = int(item[-1])
    return (key, count)


def write_counter_key_value(f, key, value):
    if isinstance(key, basestring):
        f.write(key)
    else:
        try:
            f.write("\t".join(key))
        except:
            print "Error caused by: " + repr("\t".join(key))
            raise
    f.write("\t%d\n" %value)


def write_counter(counter, path):
    """
    Designed to pair with merge function.
    """
    _log.debug("Started sorting %s (process %d, thread %s)." 
               %(path, os.getpid(), threading.current_thread().name))
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        args = ["sort", "-o", path];
        p = subprocess.Popen(args, stdin=subprocess.PIPE)
        with wrapos(p.stdin) as writer:
            for key in counter:
                write_counter_key_value(writer, key, counter[key])
        p.wait()
    finally:
        os.environ["LC_ALL"] = original_locale
        _log.debug("Finished sorting %s (process %d, thread %s)." 
                   %(path, os.getpid(), threading.current_thread().name))


def sort_and_write_all(counter, path):
    keys = sorted(counter, key=lambda(a): a[0])
    with wrapos(open(path, "wt")) as f:
        for key in keys:
            write_counter_key_value(f, key, counter[key])


def sort_and_write_most_commons(counter, n, path):
    items = sorted(counter.most_common(n), key=lambda(a): a[0])
    with wrapos(open(path, "wt")) as f:
        for item in items:
            write_counter_key_value(f, item[0], item[1])


def merge(dest, paths, clean_up=True):
    """
    Merge different count files to a single count file using sort command
    in merge mode. Input files must be created with write_counter function.
    """
    if isinstance(paths, basestring):
        paths = [paths]
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        args = ["sort", "-m"];
        args.extend(paths)
        p = subprocess.Popen(args, stdout=subprocess.PIPE)
        previous_key = None
        previous_count = 0
        with wrapos(open(dest, "w")) as f:
            for line in wrapis(p.stdout):
                key, count = parse_count(line)
                if key == previous_key:
                    previous_count += int(count)
                else:
                    if previous_key is not None:
                        write_counter_key_value(f, previous_key, previous_count)
                    previous_key = key
                    previous_count = int(count)
            if previous_key is not None: # write the last one
                write_counter_key_value(f, previous_key, previous_count)
        if clean_up:
            for path in paths:
                os.remove(path)
                _log.info("Cleaned up 1 file: %s" %path)
    finally:
        os.environ["LC_ALL"] = original_locale