# -*- coding: utf-8 -*-
'''
Created on Feb 8, 2014

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
from composes.transformation.scaling.row_normalization import RowNormalization
from composes.utils import io_utils
from logging.config import fileConfig
from minh.evaluate.evaluate import evaluate_init, \
    evaluate_space, get_targets, print_summary
from minh.experiment import experiment
from minh.experiment.experiment import get_training_data, get_evaluations,\
    build_composed_space
from minh.experiment.ioutils import read_stripped_lines
from minh.space import neural
from minh.space.neural import L1OutputPenalty
from pylearn2.costs.cost import SumOfCosts
from pylearn2.costs.mlp import L1WeightDecay, WeightDecay
import locale
import logging
import os
import subprocess
import sys
import argparse
from tempfile import NamedTemporaryFile
from string import Template


__log = logging.getLogger("minh.experiment.experiment")
try:
    locale.setlocale(locale.LC_ALL, 'en_US') # enable pretty number printing
except:
    try:
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8') # enable pretty number printing
    except:
        pass


EXPERIMENT_SECT = "experiment"
    
    
def get_compositions(conf, phrases):
    valid_set_portion = conf.getfloat(EXPERIMENT_SECT, "neural-net.valid-set-portion")
    neural_models = neural.models(conf, EXPERIMENT_SECT, "neural-net", valid_set_portion)
    return neural_models


def set_lambda(cost, lambda_):
    success = False
    if isinstance(cost, SumOfCosts):
        for index in range(len(cost.costs)):
            component_cost = cost.costs[index]
            if isinstance(component_cost, L1OutputPenalty):
                cost.coeffs[index] = lambda_ 
                success = True
            else:
                success = success or set_lambda(component_cost, lambda_)
        success = True
    if isinstance(cost, L1WeightDecay) or isinstance(cost, WeightDecay):
        cost.coeffs = [lambda_] * len(cost.coeffs)
    return success


def run(conf, param_map):
    space_path = conf.get(EXPERIMENT_SECT, "processed-word-space")
    space = io_utils.load(space_path)
    evaluate_init(space)
    
    phrases = set(read_stripped_lines(conf.get(EXPERIMENT_SECT, "phrases")))
    __log.info("Number of phrases: %d" %len(phrases))
    
    phrase_space_path = conf.get(EXPERIMENT_SECT, "processed-phrase-word-space")
    phrase_space = io_utils.load(phrase_space_path)
    
#    prepare for composition
#    normalized space is transient i.e. not saved into file
#    because we need to examine the space with untouched weights later
    space = space.apply(RowNormalization())
    phrase_space = phrase_space.apply(RowNormalization())    
    
    prefix = "neural-net"
    valid_set_portion = conf.getfloat(EXPERIMENT_SECT, prefix + ".valid-set-portion")
    dims = space.cooccurrence_matrix.shape[1]
    neural_models = neural.models(conf, EXPERIMENT_SECT, "neural-net", dims, 
                                  valid_set_portion, param_map)
    
    evaluations = get_evaluations(conf)
    try:
        training_data = get_training_data(phrases)
        targets = get_target_phrases(evaluations)
        for composition in neural_models:
            print "\n*** space:%s, composition:%s ***\n" %("word", composition.name)
            composed_space = build_composed_space(space, composition, training_data, 
                                                  phrase_space, targets, None)
            evaluate_space(composed_space, composition.name, evaluations)
    finally:
        print "\n*** summary ***\n"
        print_summary() 
    

def print_usage():
    print "Usage: python param_experiment.py config-path hidden-size l1-coeff"


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("config_path",
                        help="path to a Python config file storing config details")
    parser.add_argument("hidden_size", type=int,
                        help="number of units in each hidden layer")
    parser.add_argument("l1_coeff", type=float,
                        help="an coefficient amplify L1 regularization")
    args = parser.parse_args()

    with open(args.config_path) as f:
        conf_str = Template(f.read()).substitute(args.__dict__)
        f2 = NamedTemporaryFile(delete=False)
        with f2:
            f2.write(conf_str) 
    
    conf = ConfigParser(experiment.defaults)
    # be carefull: calling "logging.config.fileConfig" will cause 
    # AttributeError: 'module' object has no attribute 'config'
    fileConfig(f2.name) # config logging
    conf.read(f2.name)
    
    odpath = conf.get(EXPERIMENT_SECT, 'output-duplicate-path')
    if odpath:
        if os.path.exists(odpath):
            print "Output path already exists."
            sys.exit(2)
        tee = subprocess.Popen(["tee", odpath], 
                               stdin=subprocess.PIPE)
        os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
        os.dup2(tee.stdin.fileno(), sys.stderr.fileno())
    
    run(conf, args.__dict__)
    