# -*- coding: utf-8 -*-
'''
Created on Jan 7, 2014

Counting can be difficult if we deal with gigabytes of data. This module take 
optimization seriously to ensure that each run of the experiment finishes in
a reasonable time.  

@author: Lê Ngọc Minh
'''
from collections import Counter
from heapq import nlargest
from logging import DEBUG
from minh.experiment import ukwac
from minh.experiment.ioutils import write_counter, read_counts, merge, \
    read_stripped_lines, wrapos
from multiprocessing import Process
import multiprocessing
import logging
import os
import threading
from subprocess import Popen, PIPE
from minh.experiment.ukwac import lemma_and_pos
from random import sample
from minh.space.composition import SyntaxSensitiveComposition as syn
import locale
import time


__log = logging.getLogger(__name__)


def __write_contexts(path, count_tuples):
    with wrapos(open(path, "w")) as f:
        for t in count_tuples:
            f.write(t[0])
            f.write("\n")


def add_candidate_rows(words, corpus_paths, extract_row, max_num, 
                       min_length, min_count, result_path=None):
    '''
    Add all content words (nouns, verbs, ajectives) that exceed thresholds to 
    a set of words.
    '''
    if result_path and os.path.exists(result_path):
        __log.info("Read candidate rows from %s" %result_path)
        words.update(read_stripped_lines(result_path))
    else:
        if 0 < max_num <= len(words): return
        __log.debug("Finding candidate rows...")
        counter = Counter()
        sentence_count = 0
        for sentence in ukwac.sentences(corpus_paths):
            try:
                for node in ukwac.dependency_nodes(sentence, False):
                    # Add this to use content words only:
                    # node.pos[0].lower() in ["n", "v", "j"]
                    if (len(node.word) >= min_length):
                        counter[extract_row(node)] += 1
            except:
                __log.exception("Exception while finding candidate rows.")
            sentence_count += 1
            if sentence_count % 100000 == 0:
                __log.debug("#sentences=%d, #words=%d" %(sentence_count, len(counter)))
        counts = dict((key, counter[key]) for key in counter 
                      if counter[key] >= min_count and key not in words)
        if max_num > 0:
            candidates = nlargest(max_num - len(words), counts, 
                                  lambda word: counts[word])
        else:
            candidates = counts.keys()
        words.update(candidates)
        if result_path:
            __log.debug("Writing candiate rows into %s" %result_path)
            with wrapos(open(result_path, "wt")) as f:
                for candidate in candidates:
                    f.write(candidate)
                    f.write("\n")


def _count_contexts_internal(cooccurrences_func, corpus_path, words, 
                             threshold=0, suppress_exceptions=1000):
    counter = Counter()
    def increase_counter(c): counter[c[1]] += 1
    cooccurrences_func(corpus_path, increase_counter, 
                       lambda row : not words or row in words,
                       suppress_exceptions=suppress_exceptions)
    if threshold > 0:
        original_count = len(counter)
        deleted_count = 0
        for key in counter.keys():
            if counter[key] <= threshold:
                del counter[key]
                deleted_count += 1
        deleted_percent = deleted_count/float(original_count)*100
        __log.debug("Deleted %d (%.2f%%) low-frequency contexts from %s" 
                    %(deleted_count, deleted_percent, corpus_path))
    __log.debug("Number of contexts from %s (process %s, thread %s): %d" 
                %(corpus_path, os.getpid(), threading.current_thread().name, len(counter)))
    return counter


def _count_and_write_contexts_internal(dest, cooccurrences_func, corpus_path, 
                                       words, threshold=0, 
                                       suppress_exceptions=1000):
    """
    @param threshold: see find_context_internal
    """
    counter = _count_contexts_internal(cooccurrences_func, corpus_path, words, 
                              threshold, suppress_exceptions)
    write_counter(counter, dest)
    __log.info("Written %d keys to %s (process %d, thread %s)." 
                %(len(counter), dest, os.getpid(), threading.current_thread().name))
    return dest


def _count_and_write_contexts_external(dest, cooccurrences_func, corpus_paths, 
                                       words, threshold=0, 
                                       suppress_exceptions=1000):
    """
    @param threshold: see find_context_external
    """
    __log.info("Started processing %s (process %d, thread %s)." 
               %(str(corpus_paths), os.getpid(), threading.current_thread().name))
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        dest_dir = os.path.dirname(dest)
        with wrapos(open(dest, "w")) as f:
            p1 = Popen(["sort", "-T", dest_dir, "-S", "1G"], bufsize=-1, stdin=PIPE, stdout=PIPE)
            p2 = Popen(["uniq", "-c"], bufsize=-1, stdin=p1.stdout, stdout=PIPE)
            p3 = Popen(["awk", "{print $2 \"\t\" $1}"], bufsize=-1, stdin=p2.stdout, stdout=f)
            p1_inp = wrapos(p1.stdin)
            def callback(c): 
                p1_inp.write(c[1])
                p1_inp.write("\n")
#                print c[1]
            cooccurrences_func(corpus_paths, callback,
                          lambda row : not words or row in words, 
                          suppress_exceptions=suppress_exceptions)
            p1_inp.close()
            p3.wait()
        __log.info("Wrote to %s (process %d, thread %s)." 
                    %(dest, os.getpid(), threading.current_thread().name))
        return dest
    finally:
        os.environ["LC_ALL"] = original_locale    


def _count_and_write_contexts_multiprocessing(count_and_write_func, dest, 
                                              iterate_cooccurrences, corpus_path, 
                                              words, threshold=0, suppress_exceptions=1000):
    try:
        return count_and_write_func(dest, iterate_cooccurrences, 
                                    corpus_path, words, threshold, suppress_exceptions)
    except Exception as e:
        __log.error("Uncautch exception in process %d. The processing of %s "
                    "is terminated but the process is alive. Exception details:" 
                    %(os.getpid(), str(corpus_path)))
        __log.exception(e)
        raise
    

def _read_most_common_contexts(path, n):
    return nlargest(n, read_counts(path), key=lambda c: c[1])


END_OF_QUEUE = "__END_OF_QUEUE__" 

    
def _add_more_paths_if_possible(queue, paths):
    # Merging too many parts maybe troublesome???
    # One potential problem is lack of disk space. During merging, the data in
    # merged parts will be cloned. Merging too much data, we may run short of 
    # disk space. 
    while not queue.empty() and len(paths) <= 20:
        part_path = queue.get_nowait()
        if part_path and part_path != END_OF_QUEUE:
            if __log.isEnabledFor(DEBUG):
                __log.debug("Additional file for better performance: %s" %part_path)
            paths.append(part_path)
        else:
            if part_path == END_OF_QUEUE:
                __log.debug("End of queue flag is put back")
                queue.put(part_path)
            break


def _merge_queue(queue, result_path, cleaning_up=True):
    merge_count = 0
    queue_ended = False
    merge_paths = []
    paths = []
    while (not queue_ended) or len(merge_paths) > 0:
        # Add more parts to the list
        if not queue_ended:
            while len(paths) < 4 and not queue_ended:
                part_path = queue.get() # wait and get
                if part_path == END_OF_QUEUE:
                    queue_ended = True
                else:
                    paths.append(part_path)
                    __log.debug("Part path: %s." %str(part_path))
            _add_more_paths_if_possible(queue, paths)
        else:
            paths.extend(merge_paths[:20])
            merge_paths[:20] = []
        # If we have enough paths, merge. Otherwise run another round.
        if len(paths) >= 2:
            merge_path = result_path + ".merge-" + str(merge_count)
            __log.info("Merging %s into %s" %(str(paths), merge_path))
            merge(merge_path, paths, cleaning_up)
            merge_paths.append(merge_path)
            merge_count += 1
            paths = []
            
    __log.debug("Paths before returning: " + str(paths))
    if len(paths) == 1:
        queue.put(paths[0])
        return paths[0]
    else:
        __log.warn("A bug may be detected: paths are empty.")


def _find_contexts_by_merging(count_and_write_func, cooccurrence_func, 
                              corpus_paths, words, n, result_path=None, 
                              threshold=0, max_processes=None):
    """
    Find contexts concurrently on multiple CPUs. To ensure scalability, this
    method use Linux sort command to merge partial results.

    @param words: the set of accepted words to find contexts for. Set to None to 
    disable filtering words.
    @param result_path: the path to store results to or load results from if it 
    already exists (hopefully from previous run of the same configuration).
    """
    if result_path and os.path.exists(result_path):
        return set(read_stripped_lines(result_path))

    __log.info("Max processes being used: " + str(max_processes))
    __log.info("Start counting contexts")
    
    if len(corpus_paths) > 1:
        pool = multiprocessing.Pool(max_processes)
        if isinstance(corpus_paths, basestring):
            corpus_paths = (corpus_paths,)
        
        queue = multiprocessing.Queue(len(corpus_paths))
        for index, path in enumerate(corpus_paths):
            part_path = "%s.part-%03d" %(result_path, index) 
            pool.apply_async(_count_and_write_contexts_multiprocessing, 
                             args=(count_and_write_func, part_path, 
                                   cooccurrence_func, path, words, threshold),
                             kwds={"suppress_exceptions":1000},
                             callback=queue.put)
        pool.close()
    
        merge_process = Process(target=_merge_queue, args=(queue, result_path))
        merge_process.start()
        
        pool.join()
        queue.put(END_OF_QUEUE)
        __log.debug("find_contexts_internal - Finished processing input files.")
    
        merge_process.join()
        __log.debug("find_contexts_internal - Finished merging.")
    
        last_path = queue.get_nowait()
    else:
        last_path = count_and_write_func(result_path + ".count", cooccurrence_func, 
                                         corpus_paths[0], words, threshold)
    most_commons = _read_most_common_contexts(last_path, n)
    __write_contexts(result_path, most_commons)
    __log.info("First 20 of most common contexts with counts: %s", str(most_commons[:20]))
    __log.info("Last 20 of most common contexts with counts: %s", str(most_commons[-20:]))
    __log.info("Finished counting contexts.")
    return set(item[0] for item in most_commons)


def find_contexts_internal(iterate_cooccurrences, corpus_paths, words, n, 
                           result_path=None, threshold=0, max_processes=None):
    """
    Find contexts in memory by multiple processes. 
    
    @param threshold: how many occurrences a context should reach to be retained
    in the result. The count is for EACH FILE of the corpus, not all of them. 
    This is to prevent memory overflow but a very dangerous practice. If you are 
    doing a serious research you should consider not use this feature.
    @see: count_cooccurrence_internal 
    """
    return _find_contexts_by_merging(_count_and_write_contexts_internal,
                              iterate_cooccurrences, corpus_paths, words, n, 
                              result_path, threshold, max_processes)


def find_contexts_external(iterate_cooccurrences, corpus_paths, words, n, 
                           result_path=None, threshold=0, max_processes=None):
    """
    Find contexts on disk by multiple processes. 
    
    It turned put that this method is very inefficient. It ran at a speed of 
    5h for each 100,000 sentences. One should consider carefully before using.
    
    @param threshold: unused
    @see: count_cooccurrence_external 
    """
    return _find_contexts_by_merging(_count_and_write_contexts_external,
                              iterate_cooccurrences, corpus_paths, words, n, 
                              result_path, threshold, max_processes)


def _count_cooccurrences_internal(cooccurrences_func, corpus_paths, words, contexts, 
                        extract_row, suppress_exceptions=1000):
    counter = Counter()
    def increase_counter(c): counter[c] += 1
    cooccurrences_func(corpus_paths, increase_counter,
                          lambda row : not words or row in words, 
                          extract_row, 
                          lambda column: not contexts or column in contexts,
                          suppress_exceptions)
    __log.info("Number of cooccurrences from %s (process %s, thread %s): %d" 
                    %(corpus_paths, os.getpid(), threading.current_thread().name, len(counter)))
    return counter


def _count_and_write_cooccurrences_internal(dest, cooccurrences_func, corpus_paths, 
                                  words, contexts, extract_row, suppress_exceptions=1000):
    __log.info("Started processing %s (process %d, thread %s)." 
               %(str(corpus_paths), os.getpid(), threading.current_thread().name))
    counter = _count_cooccurrences_internal(cooccurrences_func, corpus_paths, words, 
                                  contexts, extract_row, suppress_exceptions)
    write_counter(counter, dest)
    __log.info("Wrote %d keys to %s (process %d, thread %s)." 
                %(len(counter), dest, os.getpid(), threading.current_thread().name))
    return dest


def _count_and_write_cooccurrences_external(dest, cooccurrences_func, corpus_paths, 
                                            words, contexts, extract_row, suppress_exceptions=1000):
    __log.info("Started processing %s (process %d, thread %s)." 
               %(str(corpus_paths), os.getpid(), threading.current_thread().name))
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        dest_dir = os.path.dirname(dest)
        with wrapos(open(dest, "w")) as f:
            p1 = Popen(["sort", "-T", dest_dir, "-S", "1G"], bufsize=-1, 
                       stdin=PIPE, stdout=PIPE)
            p2 = Popen(["uniq", "-c"], bufsize=-1, stdin=p1.stdout, stdout=PIPE)
            p3 = Popen(["awk", "{print $2 \"\t\" $3 \"\t\" $1}"], bufsize=-1,  
                       stdin=p2.stdout, stdout=f)
            p1_inp = wrapos(p1.stdin)
            def callback(c): 
                p1_inp.write("%s\t%s\n" %(c[0], c[1]))
            cooccurrences_func(corpus_paths, callback,
                          lambda row : not words or row in words, 
                          extract_row, 
                          lambda column: not contexts or column in contexts,
                          True)
            p1_inp.close()
            p3.wait()
        __log.info("Wrote to %s (corpus paths %s, process %d, thread %s)." 
                    %(dest, str(corpus_paths), os.getpid(), 
                      threading.current_thread().name))
        return dest
    finally:
        os.environ["LC_ALL"] = original_locale    


def _count_and_write_cooccurrences_multiprocessing(count_and_write_func, dest, cooccurrences_func, 
                                                  corpus_paths, words, contexts, 
                                                  extract_row, suppress_exceptions=1000):
    """
    Wrap count_and_write_func and catch any unhandled exception.
    """
    for attempt in range(5):
        try:
            return count_and_write_func(dest, cooccurrences_func, corpus_paths, 
                                        words, contexts, extract_row, suppress_exceptions)
        except Exception as e:
            __log.error("Uncautch exception in process %d (attempt %d). "
                        "Exception details below." 
                        %(os.getpid(), attempt+1, str(corpus_paths)))
            __log.exception(e)
            __log.info("Sleep for 10 seconds before retry...")
            time.sleep(10)
    __log.error("Too many errors in process %d. The processing of %s is " 
                "terminated but the process is alive." %os.getpid())
    return None


def _count_cooccurrences_by_merging(count_and_write_func, cooccurrences_func, corpus_paths, 
                                    words, contexts, result_path, extract_row, max_processes=None):
    '''
    Find co-occurrences concurrently on multiple CPUs.
    '''
    if os.path.exists(result_path):
        __log.info("Co-occurrences found at %s." %result_path) 
        return 
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)

    __log.info("Max processes being used: " + str(max_processes))
    __log.debug("Start counting co-occurrences...")
    
    if len(corpus_paths) > 1:
        queue = multiprocessing.Queue(len(corpus_paths))
        pool = multiprocessing.Pool(max_processes)
        for index, path in enumerate(corpus_paths):
            part_path = "%s.part-%03d" %(result_path, index)
            pool.apply_async(_count_and_write_cooccurrences_multiprocessing, 
                             args=(count_and_write_func, part_path, cooccurrences_func, 
                                   path, words, contexts, extract_row),
                             callback=queue.put)
        pool.close()
        
        merge_process = Process(target=_merge_queue, args=(queue, result_path))
        merge_process.start()
        
        pool.join()
        __log.debug("Finished counting input files.")
        # Close the queue as soon as no more path can be added. It may be the case 
        # that some processes fails and their split is simply discarded. 
        queue.put(END_OF_QUEUE)
        merge_process.join()
        final_path = queue.get()
        __log.debug("Final merged path: " + final_path)
        os.rename(final_path, result_path)
        __log.debug("Finished merging.")
    else:
        count_and_write_func(result_path, cooccurrences_func, 
                             corpus_paths[0], words, contexts, extract_row)
    
    __log.info("Finished counting co-occurrences.")


def count_cooccurrences_internal(cooccurrences_func, corpus_paths, 
                                  words, contexts, result_path, extract_row, 
                                  max_processes=None):
    """
    Count co-occurrences in memory by multiple processes. Given a large enough 
    corpus, this method may cause thrashing. If you find swap usage high and 
    CPU usage close to zero you should consider using the external function 
    instead. 
    """
    _count_cooccurrences_by_merging(_count_and_write_cooccurrences_internal, 
                                    cooccurrences_func, corpus_paths, words, 
                                    contexts, result_path, extract_row, max_processes)


def count_cooccurrences_external(cooccurrences_func, corpus_paths, 
                                  words, contexts, result_path, extract_row, 
                                  max_processes=None):
    """
    Count co-occurrences on disk using multiple processes. Make use of sort 
    command in Linux to work with large corpora.
    
    The temporary directory is made the same with the output directory. Make 
    sure that you have enough disk space for both intermediate and final data. 
    """
    _count_cooccurrences_by_merging(_count_and_write_cooccurrences_external, 
                                    cooccurrences_func, corpus_paths, words, 
                                    contexts, result_path, extract_row, max_processes)


def _count_and_write_intersecting_phrases(corpus_path, words, dependents, 
                                          heads, codes, dest):
    __log.info("Started processing %s (process %d, thread %s)." 
               %(str(corpus_path), os.getpid(), threading.current_thread().name))
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        dest_dir = os.path.dirname(dest)
        scount = 0
        lcount = 0
        with wrapos(open(dest, "w")) as f:
            p1 = Popen(["sort", "-T", dest_dir, "-S", "1G"], bufsize=-1, stdin=PIPE, stdout=PIPE)
            p2 = Popen(["uniq", "-c"], bufsize=-1, stdin=p1.stdout, stdout=PIPE)
            p3 = Popen(["awk", "{print $2 \"\t\" $3 \"\t\" $1}"], bufsize=-1,  
                       stdin=p2.stdout, stdout=f)
            p1_inp = wrapos(p1.stdin)
            
            for sentence in ukwac.sentences(corpus_path):
                try:
                    nodes = ukwac.dependency_nodes(sentence, False)
                    for node in nodes:
                        node.lemma_and_pos = lemma_and_pos(node)
                    for node in nodes:
                        if node.head_id == 0: continue
                        head = nodes[int(node.head_id)-1].lemma_and_pos
                        dependent = node.lemma_and_pos
                        code = syn.code((dependent, head, ''))
                        # it is really important to filter phrases ASAP because
                        # we can remove a great deal of phrases to count. 
                        if (head in words and dependent in words and
                                ((heads is None) or (dependents is None) or
                                 (head, code) in heads or 
                                 (dependent, code) in dependents) and
                                ((codes is None) or (code in codes))):
                            try:
                                p1_inp.write("%s\t%s\n" %(dependent, head))
                                lcount += 1
                            except IOError:
                                __log.error("I/O error. Perhaps one of consuming processes was terminated")
                                break
                            
                    # print progress
                    scount += 1
                    if scount % 100000 == 0:
                        __log.debug("Processed %s sentences, wrote %s lines (%s)." 
                                    %(locale.format("%d", scount, grouping=True),
                                    locale.format("%d", lcount, grouping=True),
                                    os.getpid()))
                except:
                    __log.exception("Error while processing sentence: %s" %sentence)
        
            p1_inp.close()
            p3.wait()
        __log.info("Wrote %s lines to %s (process %d, thread %s)." 
                    %(locale.format("%d", lcount, grouping=True), 
                      dest, os.getpid(), threading.current_thread().name))
        return dest
    finally:
        os.environ["LC_ALL"] = original_locale    


def _count_intersecting_phrases_by_merging(corpus_paths, words, dependents, 
                                           heads, codes, result_path, 
                                           max_processes=None):
    '''
    Find co-occurrences concurrently on multiple CPUs.
    '''
    if os.path.exists(result_path): 
        __log.info("Phrase counting file found at %s." %result_path)
        return 
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)

    __log.info("Max processes being used: " + str(max_processes))
    __log.debug("Start counting phrases...")
    
    if len(corpus_paths) > 1:
        queue = multiprocessing.Queue(len(corpus_paths))
        pool = multiprocessing.Pool(max_processes)
        for index, path in enumerate(corpus_paths):
            part_path = result_path + ".part-" + str(index)
            pool.apply_async(_count_and_write_intersecting_phrases, 
                             args=(path, words, dependents, heads, codes, 
                                   part_path),
                             callback=queue.put)
        pool.close()
        
        merge_process = Process(target=_merge_queue, args=(queue, result_path))
        merge_process.start()
        
        pool.join()
        __log.debug("Finished counting input files.")
        # Close the queue as soon as no more path can be added. It may be the case 
        # that some processes fails and their split is simply discarded. 
        queue.put(END_OF_QUEUE)
        merge_process.join()
        os.rename(queue.get(), result_path)
        __log.debug("Finished merging.")
    else:
        _count_and_write_intersecting_phrases(corpus_paths[0], words, 
                                              dependents, heads, codes,
                                              result_path)

    __log.info("Finished counting phrases.")
    
    
def _head_phrases(count_file, heads, max_phrases):
    if len(heads) <= 0: return []
    phrases_per_head = max_phrases / len(heads)
    phrases_for_head = dict([(head, []) for head in heads])
    remaining_slots = max_phrases
    for line in count_file:
        # I don't use the "more advanced" csv.reader because it will fail if
        # the file contain unclosed quotes
        phrase = line.split('\t') 
        if len(phrase) != 3:
            __log.warn("Invalid phrase: %s" %str(phrase))
            continue
        (dependent, head, _) = phrase
        key = (head, syn.code(phrase))
        if (key in phrases_for_head and 
                len(phrases_for_head[key]) < phrases_per_head):
            phrases_for_head[key].append((dependent, head))
            remaining_slots -= 1
        if remaining_slots <= 0:
            break
    return sum(phrases_for_head.values(), [])

 
def _dependent_phrases(count_file, dependents, max_phrases):
    if len(dependents) <= 0: return []
    phrases_per_dependent = max_phrases / len(dependents)
    phrases_for_dependent = dict([(dependent, []) for dependent in dependents])
    remaining_slots = max_phrases
    for line in count_file:
        # I don't use the "more advanced" csv.reader because it will fail if
        # the file contain unclosed quotes
        phrase = line.split('\t') 
        if len(phrase) != 3:
            __log.warn("Invalid phrase: %s" %str(phrase))
            continue
        (dependent, head, _) = phrase
        key = (dependent, syn.code(phrase))
        if (key in phrases_for_dependent and 
                len(phrases_for_dependent[key]) < phrases_per_dependent):
            phrases_for_dependent[key].append((dependent, head))
            remaining_slots -= 1
        if remaining_slots <= 0:
            break
    return sum(phrases_for_dependent.values(), [])


def _add_first_phrases_until_reach(count_file, phrases, max_phrases):
    with open(count_file) as tsv:
        for line in tsv:
            # I don't use the "more advanced" csv.reader because it will fail if
            # the file contain unclosed quotes
            phrase = line.split('\t') 
            dependent, head, _ = phrase
            phrases.add((dependent, head))
            if len(phrases) >= max_phrases:
                break


def sort_by_count_descending(path, sorted_path):
    if os.path.exists(sorted_path):
        __log.info("Sorted phrases file exists: %s" %sorted_path)
        return 
    __log.info("Started sorting phrases in %s." %path)
    with open(sorted_path, "w") as f:
        p1 = Popen(["awk", '{print $3 "\t" $1 "\t" $2}', path], bufsize=-1, stdout=PIPE)
        p2 = Popen(["sort", "-n", "-r", "-S", "1G"], bufsize=-1, 
                   stdin=p1.stdout, stdout=PIPE)
        p3 = Popen(["awk", '{print $2 "\t" $3 "\t" $1}'], bufsize=-1, 
                   stdin=p2.stdout, stdout=f)
        p3.wait()
    __log.info("Finished sorting phrases into %s." %sorted_path)


def log_phrase_statistics(phrases, targets):
    dependents = set([(target[0], syn.code(target)) for target in targets])
    heads = set([(target[1], syn.code(target)) for target in targets])
    head_counter = Counter()
    dependent_counter = Counter()
    for p in phrases:
        code = syn.code(p)
        head = (p[1], code)
        dependent = (p[0], code)
        if head in heads: head_counter[head] += 1
        if dependent in dependents: dependent_counter[dependent] += 1
    import numpy
    head_counts = numpy.array(head_counter.values())
    dependent_counts = numpy.array(dependent_counter.values())
    __log.info("Number of distinct target heads: %d." %len(head_counter))
    __log.info("Number of phrases for a target head: %.2f (std: %.2f)." %(
                head_counts.mean(), head_counts.std()))
    __log.info("Number of distinct target dependents: %d." %len(dependent_counter))
    __log.info("Number of phrases for a target dependent: %.2f (std: %.2f)" %(
                dependent_counts.mean(), dependent_counts.std()))
    __log.info("Total number of phrases: %d" %len(phrases))


def intersecting_phrases_unbalanced(corpus_paths, words, targets, max_phrases, 
                                    result_path, max_processes=None):
    if result_path and os.path.exists(result_path):
        __log.info("Phrase list found at %s" %result_path)
        return set(tuple(line.split('\t')) 
                   for line in read_stripped_lines(result_path))

    count_file = result_path + ".count"
    codes = set(syn.code(phrase) for phrase in targets)
    _count_intersecting_phrases_by_merging(corpus_paths, words, None, None, 
                                           codes, count_file, max_processes)
    sorted_file = count_file + ".sorted"
    sort_by_count_descending(count_file, sorted_file)
    phrases = set()
    _add_first_phrases_until_reach(sorted_file, phrases, max_phrases)
    with open(result_path, "w") as f:
        for phrase in phrases:
            f.write("\t".join(phrase) + "\n") 
    return phrases
    

def intersecting_phrases_balanced(corpus_paths, words, targets, max_phrases, 
                                  result_path, max_processes=None):
    """
    Extract up to a specified number of intersecting phrases from a corpus.
    An intersecting phrase is a phrase that share either it head or dependent
    word with one of the target phrases. It is required that a phrase is of
    a syntactic relation that appears in one of target phrases to be counted 
    as an intersecting phrase. 
    
    This method try to balance the number of training examples for each word in
    a specific position. That is the number of training examples for every head
    word is approximately equal and the number of training examples for every 
    dependent word is approximately equal. It is different from balancing for 
    phrases which is inherently harder because the sets of training examples for 
    different phrases are not disjoined.
    
    The directory to store intermediate results is set to the directory that
    store final results. This can become a problem if the hard drive cannot 
    provide enough space to store intermediate results.
    
    @param corpus_paths: a tuple of file paths making up a corpus
    @param targets: a set of target phrases, each element is a tuple: (dependent 
    word, head word, phrase).
    @param max_phrases: the max number of phrases
    @param result_path:  
    @return: a set of tuples (dependent, head)
    """
    if result_path and os.path.exists(result_path):
        __log.info("Phrase list found at %s" %result_path)
        return set(tuple(line.split('\t')) 
                   for line in read_stripped_lines(result_path))

    dependents = set([(target[0], syn.code(target)) for target in targets])
    heads = set([(target[1], syn.code(target)) for target in targets])
    count_file = result_path + ".count"
    _count_intersecting_phrases_by_merging(corpus_paths, words, dependents, 
                                           heads, None, count_file, max_processes)
    sorted_count_file = count_file + ".sorted"
    sort_by_count_descending(count_file, sorted_count_file)
    phrases = set()
    with open(sorted_count_file) as f:
        phrases.update(_head_phrases(f, heads, max_phrases))
    with open(sorted_count_file) as f:
        phrases.update(_dependent_phrases(f, dependents, max_phrases))
    if len(phrases) < max_phrases:
        _add_first_phrases_until_reach(sorted_count_file, phrases, max_phrases)
    elif len(phrases) > max_phrases:
        removed = sample(phrases, len(phrases) - max_phrases)
        phrases = phrases.difference(removed)
    with open(result_path, "w") as f:
        for phrase in phrases:
            f.write("\t".join(phrase) + "\n") 
    return phrases
