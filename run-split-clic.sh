~/python2.7 src/minh/experiment/experiment.py config/split/linear.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lexical-function.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/tanh.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/rectifier-fat.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-corrupted.cfg

~/python2.7 src/minh/experiment/experiment.py config/split/tanh-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/rectifier-fat.blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-corrupted-blending.cfg

~/python2.7 src/minh/experiment/experiment.py config/split.svd/linear.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lexical-function.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/tanh.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/rectifier-fat.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-corrupted.cfg

~/python2.7 src/minh/experiment/experiment.py config/split.svd/tanh-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/rectifier-fat-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-blending.cfg
~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-corrupted-blending.cfg

#~/python2.7 src/minh/experiment/experiment.py config/split/linear.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lexical-function.cfg ~/python2.7 src/minh/experiment/experiment.py config/split/tanh.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/rectifier-fat.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-corrupted.cfg

#~/python2.7 src/minh/experiment/experiment.py config/split/tanh-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/rectifier-fat.blending.cfg ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split/lwta-fat-rectified-corrupted-blending.cfg

#~/python2.7 src/minh/experiment/experiment.py config/split.svd/linear.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lexical-function.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/tanh.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/rectifier-fat.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-corrupted.cfg

#~/python2.7 src/minh/experiment/experiment.py config/split.svd/tanh-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/rectifier-fat-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-blending.cfg; ~/python2.7 src/minh/experiment/experiment.py config/split.svd/lwta-fat-rectified-corrupted-blending.cfg

